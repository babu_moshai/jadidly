import React, { Component,useEffect} from 'react';
import { Provider } from 'react-redux';
import MainRoute from './app/routes/MainRoute'
import { store } from './app/store/store';

const App =  () =>{ 
     return (
     <Provider store={store} >
        <MainRoute />
    </Provider>
)}
export default App;