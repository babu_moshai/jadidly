import Snackbar from 'react-native-snackbar'
import { colorPrimary } from '../styles/Color'

export const showSnackBarOne = async (message) =>  {
    await new Promise((resolve) => setTimeout(resolve, 200))
    Snackbar.show({
      textColor: 'white',
      backgroundColor: colorPrimary,
      text: message,
      duration: Snackbar.LENGTH_SHORT,
    })
  }