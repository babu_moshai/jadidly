
const API_KEY = 'AIzaSyDhriij70sX5W7GvqILhjW-r7Mq2klQvKA'
const BASE_URL = 'http://ec2-13-126-156-77.ap-south-1.compute.amazonaws.com/api/'

export const SHARE_CAR_URL = 'https://auctionbuy.in/rim/car-detail/'

export const FACEBOOK_LOGIN = 'https://graph.facebook.com/v2.5/me?fields=email,name,friends&access_token='
// 7da7b027697c43c4bd29a0c76e56306f
export const CURRENCY_CONVERSION = 'https://api.currencyfreaks.com/latest?apikey=7da7b027697c43c4bd29a0c76e56306f'



export const REGISTER = BASE_URL + 'register'
export const VERIFY_OTP = BASE_URL + 'verify-otp'
export const LOGIN_EMAIL_USER = BASE_URL + 'login'
export const FORGOT_PASSWORD = BASE_URL + 'forgotPassword'

export const FRIEND_LIST = BASE_URL + 'players/friend-list'
export const ALL_PLAYERS = BASE_URL + 'all-players'
export const EDIT_PROFILE = BASE_URL + 'update-profile'




export const LOGIN_MOBILE_USER = BASE_URL + 'login'
export const UPDATE_PROFILE = BASE_URL + 'editProfile'
export const ADMIN_SETTING = BASE_URL + 'adminSettings'
export const HOME_SCREEN = BASE_URL + 'homescreen'

export const RESTAURANT_LIST = BASE_URL + 'restaurantList'
export const RESTAURANT_DETAILS = BASE_URL + 'restaurantDetail'
export const ADD_TO_CART = BASE_URL + 'addToCart'
export const MY_CART = BASE_URL + 'myCart'
export const ORDER_LIST = BASE_URL + 'orderList'
export const ORDER_DETAIL = BASE_URL + 'orderDetail'
export const PLACE_ORDER = BASE_URL + 'placeOrder'


export const NOTIFICATION = BASE_URL + 'notification'
export const HELP_SUPPORT = BASE_URL + 'helpSupport'

export const SAVE_ADDRESS = BASE_URL + 'saveAddress'
export const SAVE_ADDRESS_LIST = BASE_URL + 'savedAddressList'
export const REMOVE_ADDRESS = BASE_URL + 'removeAddress'


export const VOICE_TOKEN = BASE_URL + 'voiceToken'

export const PAYPAL_ACCESS_TOKEN = BASE_URL + 'paypalAccessToken'
export const PAYPAL_CREATE_TRANSACTION = BASE_URL + 'paypalCreateTransaction'

export const BOOK_RIDE = BASE_URL + 'bookRide'
export const RIDE_LIST = BASE_URL + 'rideList'
export const RIDE_DETAIL = BASE_URL + 'rideDetail'
export const CANCEL_REQUEST = BASE_URL + 'cancelRequest'
export const CANCEL_RIDE = BASE_URL + 'cancelRide'
export const RATE_DRIVER = BASE_URL + 'rateDriver'
export const RATE_VENDOR = BASE_URL + 'rateVendor'

export const RESET_PASSWORD = BASE_URL + 'reset-password'
export const RESEND_OTP = BASE_URL + 'resend-otp'

export const SOCIAL_REGISTER = BASE_URL + 'socialRegister'

export const UPDATE_RIDE_PAYMENT_STATUS = BASE_URL + 'updateRidePaymentStatus'
export const UPDATE_ORDER_PAYMENT_STATUS = BASE_URL + 'updateOrderPaymentStatus'


export const CHANGE_PASSWORD = BASE_URL + 'changePassword'
export const CHECK_PASSWORD = BASE_URL + 'checkPassword'
export const CHECK_MY_ACCOUNT = BASE_URL + 'myAccount'
export const EDIT_BANK_ACCOUNT = BASE_URL + 'editAccount'
export const CHANGE_AVALIBILTY = BASE_URL + 'changeAvalibility'
export const LOGOUT = BASE_URL + 'logout'
export const SUPPORT_TICKET = BASE_URL + 'supportTicket'

export const UPLOAD_IMG_FOR_URL = BASE_URL + 'uploadImage'

export const UPLOAD_COVER_PHOTO = BASE_URL + 'editProfileImage'
export const UPDATE_USER_PROFILE = BASE_URL + 'updateUserProfile'



export const ADD_CAR = BASE_URL + 'addCar'

export const GET_HOME_PAGE_DATA = BASE_URL + 'getHomePageData'
export const GET_FEATURED_CARS = BASE_URL + 'getFeaturedCars'
export const GET_MY_CAR_LIST = BASE_URL + 'myCarsList'
export const GET_FAV_CAR_LIST = BASE_URL + 'favCarsList'
export const MARK_AS_FAV_UNFAV = BASE_URL + 'markAsFav'

export const GET_FILTER_CAR = BASE_URL + 'filterCars'
export const GET_SEARCH_CARS = BASE_URL + 'searchCars'
export const GET_REPORT_CAR = BASE_URL + 'reportCar'

export const MARK_SOLD_HIDE_CAR = BASE_URL + 'soldAndHideCar'
export const GET_NOTIFICATION = BASE_URL + 'notificationList'
export const GET_CAR_DETAIL = BASE_URL + 'getCarDetails'
export const GET_BRAND_CAR_LIST = BASE_URL + 'getCarList'
