import axios from 'axios';
import {Alert} from 'react-native'
import {useNavigation,CommonActions} from '@react-navigation/native'
import { logoutUser, getLanguage } from '../database/UserData'
import NavigatorService from '../services/navigator';
const BASE_URL = 'http://188.166.65.5/jadidly/public/api/'

export const checkAuth = async (res) => {
  if(res.status_code==401){
    return { status_code:res.status_code, status: false, error: "Unauthorized Access Request" }
  }else{
    return res
  }
}
export const checkError = async (err) => {
  console.log(err)

  if (err.response) {
    console.log("response => ",err.response)
    //return err.response.data
    if(err.response.data.status_code == "401"){
      return { status_code:err.response.data.status_code, status: false, error: "Unauthorized Access Request" }
    }else{
      return { status_code:err.response.data.status_code, status: false, error: "Something went wrong. It could be your internet connection." }
    }
  } 
  else if (err.request) {
    console.log("request => ",err.request)
    return { status_code:'1000', status: false, error: "Something went wrong. It could be your internet connection." }
  } 
  else {
    console.log("anything")
    return { status_code:err.request.data.status_code, status: false, error: "Something went wrong. It could be your internet connection." }
  }
}
export const callApi = async (name,data) => {
  const options = {
    headers: {
      'Lang': await getLanguage()
    }
  };
  return axios.post(BASE_URL+name, data,options).then(res => {
    //console.log(" RES CALL API=>", res)
    return checkAuth(res.data);
  })
  .catch(err => {
    console.log("catch Error => ",err)
    return checkError(err)
  })
}
export const callApiNoData = async (name,token) => {

  const options = {
    headers: {
      'Lang': await getLanguage(),
      'Authorization': "Bearer "+token,
    }
  };
  //console.log("options =>", options)
  return axios.get(BASE_URL+name,options).then(res => {
    //console.log("RES CALL API=>", res)
    return checkAuth(res.data);
  })
  .catch(err => {
    console.log("catch Error => ",err)
    return checkError(err)
  })
}

export const callApiWithData = async (name,data,token) => {

  const options = {
    headers: {
      'Lang': await getLanguage(),
      'Authorization': "Bearer "+token,
    }
  };
  console.log("options =>", options)
  return axios.post(BASE_URL+name,data,options).then(res => {
    //console.log("RES CALL API=>", res)
    return checkAuth(res.data);
  })
  .catch(err => {
    console.log("catch Error => ",err)
    return checkError(err)
  })
}
export const callApiGetWithData = async (name,data,token) => {

  const options = {
    headers: {
      'Lang': await getLanguage(),
      'Authorization': "Bearer "+token,
    }
  };
  console.log("options headers=> ", options)
  return axios.get(BASE_URL+name,data,options).then(res => {
    //console.log("RES CALL API=>", res)
    return checkAuth(res.data);
  })
  .catch(err => {
    console.log("catch Error => ",err)
    return checkError(err)
  })
}


