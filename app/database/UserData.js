import AsyncStorage from '@react-native-community/async-storage'

export const saveDataOfUser = async userData => {

  const data = {
    id: userData.id,
    name: userData.name,
    email: userData.email,
    country_code: userData.country_code,
    mobile_number: userData.mobile_number,
    image: userData.image,
    token: userData.token,
  }
  await AsyncStorage.setItem('UserData', JSON.stringify(data))
}

export const getDataOfUser = async () => {
  const userData = await AsyncStorage.getItem('UserData')
  if (userData !== null) {return JSON.parse(userData) }
}

export const logoutUser = async () => {
  await isLoggedInSet('0')
  await saveDataOfUser({})
}
export const setLanguage = async result => {
  await AsyncStorage.setItem('lang', result)
}
export const getLanguage = async () => {
  const langData = await AsyncStorage.getItem('lang')
  if (langData !== null) { return langData }
  else{ return 'en' }
}

export const setFirstTime = async result => {
  await AsyncStorage.setItem('firstTime', result)
}
export const getFirstTime = async () => {
  const firstTime = await AsyncStorage.getItem('firstTime')
  if (firstTime !== null) { return firstTime }
  else{ return '1' }
}

export const isLoggedInSet = async result => {
  await AsyncStorage.setItem('isLoggedIn', result)
}
export const isLoggedInGet = async () => {
  const isLoggedIn = await AsyncStorage.getItem('isLoggedIn')
  if (isLoggedIn !== null) { return isLoggedIn }
  else{ return "0" }
}

export const setFingerPrintEnable = async result => {
  await AsyncStorage.setItem('fingerPrintEnable', result)
}
export const getFingerPrintEnable = async () => {
  const fingerPrintEnable = await AsyncStorage.getItem('fingerPrintEnable')
  if (fingerPrintEnable !== null) { return fingerPrintEnable }
  else{ return "0" }
}

