import AsyncStorage from '@react-native-community/async-storage';

export class Prefs 
{

    setUserId(userId){AsyncStorage.setItem('user_id', userId);}
    getUserId(){return AsyncStorage.getItem('user_id')}

    setName(name){AsyncStorage.setItem('name', name);}
    getName(){return AsyncStorage.getItem('name')}

    setEmail(email){AsyncStorage.setItem('email', email);}
    getEmail(){return AsyncStorage.getItem('email')}

    setCountryCode(countryCode){AsyncStorage.setItem('country_code', countryCode);}
    getCountryCode(){return AsyncStorage.getItem('country_code')}
    
    setMobile(mobile){AsyncStorage.setItem('mobile', mobile);}
    getMobile(){return AsyncStorage.getItem('mobile')}

    setImage(image){AsyncStorage.setItem('image', image);}
    getImage(){return AsyncStorage.getItem('image')}

    setDeviceId(deviceId){AsyncStorage.setItem('device_id', deviceId);}
    getDeviceId(){return AsyncStorage.getItem('device_id')}
    
    setSecurityToken(securityToken){AsyncStorage.setItem('security_token', securityToken);}
    getSecurityToken(){return AsyncStorage.getItem('security_token')}

    setDeviceToken(deviceToken){AsyncStorage.setItem('device_token', deviceToken);}
    getDeviceToken(){return AsyncStorage.getItem('device_token')}
    
    setLoggedIn(logedIn){AsyncStorage.setItem('logged_in', logedIn);}
    getLoggedIn(){return AsyncStorage.getItem('logged_in')}
    
    setFirstTime(first_time){AsyncStorage.setItem('first_time', first_time);}
    getFirstTime(){return AsyncStorage.getItem('first_time')}

    setLocation(location){AsyncStorage.setItem('location', location);}
    getLocation(){return AsyncStorage.getItem('location')}
    
    setLat(lat){AsyncStorage.setItem('lat', lat);}
    getLat(){return AsyncStorage.getItem('lat')}

    setLng(lng){AsyncStorage.setItem('lng', lng);}
    getLng(){return AsyncStorage.getItem('lng')}

    setLang(lang){AsyncStorage.set('lang', lang);}
    getLang(){return AsyncStorage.getItem('lang')}

    setClearPrefs(){
        AsyncStorage.setItem('user_id', "");
        AsyncStorage.setItem('user_type', "");
        AsyncStorage.setItem('name', "")
        AsyncStorage.setItem('email', "");
        AsyncStorage.setItem('country_code', "");
        AsyncStorage.setItem('mobile', "");
        AsyncStorage.setItem('image', "");
        AsyncStorage.setItem('location', "");
        AsyncStorage.setItem('lat', "");
        AsyncStorage.setItem('lng', "");
        AsyncStorage.setItem('security_token', "");
        AsyncStorage.setItem('logged_in', "0");
        AsyncStorage.setItem('first_time', "1");

    }

}