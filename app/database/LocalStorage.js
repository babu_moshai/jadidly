import SyncStorage from 'sync-storage';

export class Prefs 
{
    constructor(){
        SyncStorage.init()
    }

    setUserId(userId){SyncStorage.set('user_id', userId);}
    getUserId(){return SyncStorage.get('user_id')}

    setName(name){SyncStorage.set('name', name);}
    getName(){return SyncStorage.get('name')}

    setEmail(email){SyncStorage.set('email', email);}
    getEmail(){return SyncStorage.get('email')}

    setCountryCode(countryCode){SyncStorage.set('country_code', countryCode);}
    getCountryCode(){return SyncStorage.get('country_code')}
    
    setMobile(mobile){SyncStorage.set('mobile', mobile);}
    getMobile(){return SyncStorage.get('mobile')}

    setImage(image){SyncStorage.set('image', image);}
    getImage(){return SyncStorage.get('image')}

    setDeviceId(deviceId){SyncStorage.set('device_id', deviceId);}
    getDeviceId(){return SyncStorage.get('device_id')}
    
    setSecurityToken(securityToken){SyncStorage.set('security_token', securityToken);}
    getSecurityToken(){return SyncStorage.get('security_token')}

    setDeviceToken(deviceToken){SyncStorage.set('device_token', deviceToken);}
    getDeviceToken(){return SyncStorage.get('device_token')}
    
    setLoggedIn(logedIn){SyncStorage.set('logged_in', logedIn);}
    getLoggedIn(){return SyncStorage.get('logged_in')}
    
    setFirstTime(first_time){SyncStorage.set('first_time', first_time);}
    getFirstTime(){return SyncStorage.get('first_time')}

    setLocation(location){SyncStorage.set('location', location);}
    getLocation(){return SyncStorage.get('location')}
    
    setLat(lat){SyncStorage.set('lat', lat);}
    getLat(){return SyncStorage.get('lat')}

    setLng(lng){SyncStorage.set('lng', lng);}
    getLng(){return SyncStorage.get('lng')}

    setLang(lang){SyncStorage.set('lang', lang);}
    getLang(){return SyncStorage.get('lang')}

    setClearPrefs(){
        SyncStorage.set('user_id', "");
        SyncStorage.set('user_type', "");
        SyncStorage.set('name', "")
        SyncStorage.set('email', "");
        SyncStorage.set('country_code', "");
        SyncStorage.set('mobile', "");
        SyncStorage.set('image', "");
        SyncStorage.set('location', "");
        SyncStorage.set('lat', "");
        SyncStorage.set('lng', "");
        SyncStorage.set('security_token', "");
        SyncStorage.set('logged_in', "0");
        SyncStorage.set('first_time', "1");

    }

}