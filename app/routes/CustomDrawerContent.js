import React, {Component, useState, useRef, useEffect} from 'react'

import {View, TouchableOpacity, Text, Alert} from 'react-native'
import FastImage from 'react-native-fast-image'
import { connect } from 'react-redux'
import MenuComponent from '../components/MenuComponent'
import {colorLightBackground, colorPrimaryDark, colorWhite, whiteTranslucent, colorPrimary, lightFont} from '../styles/Color'
import { fontFamily, fontSize } from '../styles/Fonts'
import {useNavigation, CommonActions} from '@react-navigation/native'
import { DEVICE_WIDTH, DEVICE_HEIGHT } from '../styles/Dimens'
import { showSnackBarOne } from '../helpers/showSnackBarOne'
import {callApiNoData,callApiWithData} from '../api/NetworkRequest'
import Spinner from '../components/Spinner'
import { getDataOfUser, saveDataOfUser, logoutUser } from '../database/UserData'
import { ScrollView } from 'react-native-gesture-handler'
const CustomDrawerContent = ({navigation,userDetail}) => {
  const [userData,setUserData] = useState({})
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [mobile, setMobile] = useState('')
  const [image, setImage] = useState('')
  const [loader, setLoader] = useState(false)
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setData()
    });
    return unsubscribe;
  }, [navigation]);
  useEffect(()=>{
    setData()
  },[userData])
  const setData = async() => {
    setUserData(await getDataOfUser())
    setName(userData.name)
    setEmail(userData.email)
    setMobile("+"+userData.country_code+" "+userData.mobile_number)
    setImage(userData.image)
  }
  const handleScreen = async(where) =>{
    navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [
          { name: where }
        ],
      })
    );
  }

  const logout = async (where) => {
    Alert.alert("Logout", "Do you want to logout ?", 
      [
        {text: "No", onPress: () => {}},
        {text: "Yes", onPress: () => { logoutAPI(where)
          
        }},
      ]
      ,{ cancelable: true }
    )
  }

  const checkGuest = async (where) => {
    if(userData.id != "0"){
      navigation.navigate(where)
      navigation.closeDrawer()
    }else{
      Alert.alert("Login", "You need to register/login to get access.", 
        [
          {text: "Not Now", onPress: () => {}},
          {text: "Register/Login", onPress: () => { 
            logoutUser()
            handleScreen("WelcomeScreen")
          }},
        ]
        ,{ cancelable: true }
      )
    }
  }
  const logoutAPI = async (where) => {
    let userDataLocale =  await getDataOfUser()
    const data = new FormData();
    data.append('user_id', userDataLocale.id)

    setLoader(true)
    const apiData = await callApiWithData('logout',data,userDataLocale.token)
    console.log("API RESPONSE=> ",apiData)
    setLoader(false)
  
    if(apiData.status){
      showSnackBarOne(apiData.message)
      logoutUser()
      handleScreen(where)
    }else if(apiData.status_code == "401"){
      Alert.alert("Logout", apiData.error, 
      [
        {text: "ok", onPress: () => {
          logoutUser()
          handleScreen("WelcomeScreen")
        }},
      ]
      ,{ cancelable: false }
    )
    }else{
      showSnackBarOne(apiData.error)
    }
  }

  return (
    <View style={{backgroundColor:'transparent',height:'100%',marginTop:30}}>
      <Spinner visible={loader}  />
      <View style={{ backgroundColor: colorWhite,height: 120,padding: 20,borderTopEndRadius:30}}>
          <View style={{flexDirection:'row'}}>
              <TouchableOpacity onPress={() => {}} activeOpacity={0.6}>
                <FastImage source={image===undefined || image =='' ?
                  require('../assets/images/placeholder.png'): {uri:image} }
                  style={{height: 80, width: 80, borderRadius: 40,borderColor:colorPrimary,borderWidth:2,backgroundColor:colorLightBackground}} />
              </TouchableOpacity>
              <View style={{flexDirection:'column',alignSelf:'center',marginStart:15}}>
                <Text style={{maxWidth:DEVICE_WIDTH/2,color:colorPrimary,fontFamily:fontFamily.bold,fontSize:fontSize.xl}} fontColor={colorPrimary} numberOfLines={1} onPress={() => {}}>{name}</Text>
                <Text style={{color:lightFont,fontFamily:fontFamily.semibold,fontSize:fontSize.xs,marginTop:-5}} fontColor={colorPrimary}  onPress={() => {}}>{mobile}</Text>
              </View>
          </View>
      </View>

      <ScrollView style={{}}>
      <View
        style={{ backgroundColor: colorWhite,height:DEVICE_HEIGHT}}>
          <MenuComponent
          onPressButton={() => {checkGuest("Profile")}}
          buttonText={'My Profile'}
          imgUrl={require('../assets/icons/my_profile_black.png')}
          />
          <MenuComponent
            onPressButton={() => {checkGuest("OurServices")}}
            buttonText={'Our Service'}
            imgUrl={require('../assets/icons/our_service_icon.png')}
          />
          <MenuComponent
            // onPressButton={() => null}
            onPressButton={() => {checkGuest("MyBookings")}}
            buttonText={'My Booking'}
            imgUrl={require('../assets/icons/my_booking_black.png')}
          />
          <MenuComponent
            // onPressButton={() => null}
            onPressButton={() => {checkGuest("MyCars")}}
            buttonText={'My Cars'}
            imgUrl={require('../assets/icons/my_car_black.png')}
          />
          <MenuComponent
            // onPressButton={() => null}
            onPressButton={() => {navigation.navigate('AboutUs')
            navigation.closeDrawer()}}
            buttonText={'About Us'}
            imgUrl={require('../assets/icons/about_us.png')}
          />
          <MenuComponent
            // onPressButton={() => null}
            onPressButton={() => {checkGuest("Settings")}}
            buttonText={'Settings'}
            imgUrl={require('../assets/icons/settings.png')}
          />
          <MenuComponent
            // onPressButton={() => null}
            onPressButton={() => {checkGuest("Notifications")}}
            buttonText={'Notification'}
            imgUrl={require('../assets/icons/notification_black.png')}
          />
          <MenuComponent
            // onPressButton={() => null}
            onPressButton={() => {navigation.navigate('PrivacyPolicy')
            navigation.closeDrawer()}}
            buttonText={'Privacy Policy'}
            imgUrl={require('../assets/icons/privacy_policy.png')}
          />
          <MenuComponent
            // onPressButton={() => null}
            onPressButton={() => {checkGuest("HelpAndSupport")}}
            buttonText={'Help and Support'}
            imgUrl={require('../assets/icons/help_and_support.png')}
          />
          <MenuComponent
            // onPressButton={() => null}
            onPressButton={()  => {
                if(userData.id != "0"){
                  logout('WelcomeScreen')
                  navigation.closeDrawer()
                }else{
                  logoutUser()
                  handleScreen("WelcomeScreen")
                }
              }}
            buttonText={'Logout'}
            imgUrl={require('../assets/icons/logout.png')}
          />
      </View>
      </ScrollView>
     
      
    </View>
  )
}

const handleOnPressButton = async () => {
  const navigation = useNavigation()
  
}

const mapStateToProps = (state,ownState)=>({
  userDetail:state.user
})

export default connect(mapStateToProps)(CustomDrawerContent)
