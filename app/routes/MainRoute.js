import React, {Component,useEffect,useState} from 'react'

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';

import Splash from '../screens/preLogin/Splash'
import WelcomeScreen from '../screens/preLogin/WelcomeScreen'
import SignIn from '../screens/preLogin/SignIn'
import SignUp from '../screens/preLogin/SignUp'
import OtpVerification from '../screens/preLogin/OtpVerification'
import ForgotPassword from '../screens/preLogin/ForgotPassword'
import ResetPassword from '../screens/preLogin/ResetPassword'
import Home from '../screens/postLogin/Home'
import Profile from '../screens/postLogin/Profile'
import HelpAndSupport from '../screens/postLogin/HelpAndSupport'
import AboutUs from '../screens/postLogin/AboutUs'
import PrivacyPolicy from '../screens/postLogin/PrivacyPolicy'
import TermsAndConditions from '../screens/postLogin/TermsAndConditions'
import Settings from '../screens/postLogin/Settings'
import AddCar from '../screens/postLogin/AddCar'
import MyCars from '../screens/postLogin/MyCars'
import MyBookings from '../screens/postLogin/MyBookings'
import OurServices from '../screens/postLogin/OurServices'
import Notifications from '../screens/postLogin/Notifications'

import BiomatricPopup from '../screens/postLogin/BiomatricPopup'

import CustomDrawerContent from './CustomDrawerContent';
import { getDataOfUser } from '../database/UserData';
import { add_user,logout_user } from '../actions/UserAction';
import { connect } from 'react-redux';
import Loading from '../components/Loading';

const Stack = createStackNavigator()
const Drawer = createDrawerNavigator();
const options = {headerShown: false}
const mapStateToProps= (state,ownState)=>({userDetail:state.user})
const mapDispatchToProps = {add_user,logout_user}

function HomeScreen() {
  return (
          <Drawer.Navigator initialRouteName="Home" drawerStyle={{ backgroundColor: 'transparent',width: '85%'}}
          drawerContent={(props) => <CustomDrawerContent {...props} />} >
              <Drawer.Screen name="Home" component={Home} />
          </Drawer.Navigator> 
  );
}
  

const MainRoute = ({add_user,userDetail,logout_user}) => {
  
  const [hideLoading, setHideLoading] = useState(false);

  useEffect(() => {
      const bootstrapAsync = async () => {
        const userData = await getDataOfUser()
        if (userData) {
          let user = {
            id : userData.id,
            name : userData.name,
            email : userData.email,
            country_code : userData.country_code,
            mobile_number : userData.mobile_number,
            image : userData.image,
            token : userData.token,
          }
          console.log(user)
          add_user(user)
          setHideLoading(true)
        } else {
          setHideLoading(true)
          logout_user()
        }
    };
    bootstrapAsync();
  }, []);

  if (!hideLoading) {
    // We haven't finished checking for the token yet
    return <Loading />;
  }
    return (
        <NavigationContainer>
         <Stack.Navigator initialRouteName={'Splash'}>
         
            <>
              <Stack.Screen name='Splash' component={Splash} options={options} />
              <Stack.Screen name='WelcomeScreen' component={WelcomeScreen} options={options} />
              <Stack.Screen name='SignIn' component={SignIn} options={options}  />
              <Stack.Screen name='SignUp' component={SignUp} options={options} /> 
              <Stack.Screen name='OtpVerification' component={OtpVerification} options={options} /> 
              <Stack.Screen name='ForgotPassword' component={ForgotPassword} options={options} /> 
              <Stack.Screen name='ResetPassword' component={ResetPassword} options={options} />
              <Stack.Screen name='HomeScreen' component={HomeScreen} options={options} />
              <Stack.Screen name='Profile' component={Profile} options={options} />
              <Stack.Screen name='HelpAndSupport' component={HelpAndSupport} options={options} />
              <Stack.Screen name='AboutUs' component={AboutUs} options={options} />
              <Stack.Screen name='PrivacyPolicy' component={PrivacyPolicy} options={options} />
              <Stack.Screen name='TermsAndConditions' component={TermsAndConditions} options={options} />
              <Stack.Screen name='Settings' component={Settings} options={options} />
              <Stack.Screen name='AddCar' component={AddCar} options={options} />
              <Stack.Screen name='MyCars' component={MyCars} options={options} />
              <Stack.Screen name='MyBookings' component={MyBookings} options={options} />
              <Stack.Screen name='Notifications' component={Notifications} options={options} />
              <Stack.Screen name='OurServices' component={OurServices} options={options} />
              <Stack.Screen name='BiomatricPopup' component={BiomatricPopup} options={options} />
            </>
          
        </Stack.Navigator>
        
      </NavigationContainer>
    )
}
export default connect(mapStateToProps,mapDispatchToProps)(MainRoute);