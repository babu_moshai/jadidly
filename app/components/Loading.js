import React, { Component } from 'react';
import { View ,ActivityIndicator,Text} from 'react-native';
// import { Spinner, Text } from 'native-base';
import { colorPrimary, colorWhite, colorBlack} from '../styles/Color';


const Loading = ({ loading=true, color='white' }) =>{
    if(!loading)
        return null;
    return(
    <View style={{flex:1, alignItems: 'center', justifyContent: 'center',backgroundColor:colorPrimary}}>
        <ActivityIndicator color={color} />
        <Text style={{ color: color }} >Please wait...</Text>
    </View>
)}

export default Loading;