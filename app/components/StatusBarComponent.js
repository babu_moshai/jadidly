import React, { Component } from 'react';
import { StatusBar } from 'react-native';




const StatusBarComponent = ({textColor,visible=true,background='transparent'}) => {
    return (
        <StatusBar translucent={visible} backgroundColor={background} barStyle={textColor?'light-content' :'dark-content'} />
    )
}

export default StatusBarComponent;