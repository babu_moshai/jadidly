import React, { Component } from 'react';
import { TouchableOpacity, Text, Platform, StyleSheet } from 'react-native';
import { pagePadding, btnFontSize } from '../styles/Dimens'
import { colorBlack, colorPrimary, colorWhite } from '../styles/Color'
import { fontFamily } from '../styles/Fonts';



const Button = ({ position,color, onPressButton, buttonText, buttonType, marginTop,noPadding, colorCode,borderWidth,borderColor, fontSizeReceive, textColor,btnHeight ,radius,fontStyle}) => {
    return (
        <TouchableOpacity
            style={position == 'absolute' ? {
                backgroundColor: color, justifyContent: 'center',
                height: btnHeight?btnHeight:50,
                position: 'absolute', flexBasis: 1, left: pagePadding, right: pagePadding, bottom: pagePadding,
                alignItems: 'center', paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10,  borderRadius: 15, ...Platform.select({
                    android: {
                        elevation: 1,
                    },
                    ios: {
                        shadowColor: '#000',
                        shadowOffset: {
                            width: 0,
                            height: 1,
                        },
                        shadowOpacity: 0.2,
                        shadowRadius: 1.41,
                    },
                }),
            } : buttonType == 'blank' ? {
                height: btnHeight?btnHeight:50,
                backgroundColor: 'transparent', justifyContent: 'center',
                alignItems: 'center', borderRadius: 15, borderWidth: 1,
                borderColor:colorPrimary,
                marginTop: marginTop
            } :
                    buttonType == 'wrap' ?
                        {

                            backgroundColor: colorCode ? colorCode : colorPrimary, paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10, justifyContent: 'center',
                            alignItems: 'center', borderRadius: 10,
                            marginTop: marginTop
                        }
                        :
                        {
                            height: btnHeight?btnHeight:50,
                            backgroundColor: colorCode ? colorCode : colorPrimary, justifyContent: 'center',
                            alignItems: 'center',paddingLeft: 20, paddingRight: 20, paddingTop:noPadding? 0: 10, paddingBottom:noPadding?0: 10, borderRadius:radius?radius: 15, ...Platform.select({
                                android: {
                                    elevation: 1,
                                },
                                ios: {
                                    shadowColor: '#000',
                                    shadowOffset: {
                                        width: 0,
                                        height: 1,
                                    },
                                    shadowOpacity: 0.2,
                                    shadowRadius: 1.41,
                                },
                            }), marginTop: marginTop,borderWidth: borderWidth?borderWidth:0,
                            borderColor:borderColor?borderColor:null,
                        }
            }
            onPress={() => onPressButton()}
            activeOpacity={0.5}
        >
            <Text style={{ color: textColor ? textColor : colorBlack, fontFamily:fontStyle?fontStyle: fontFamily.bold, fontSize: fontSizeReceive ? fontSizeReceive : btnFontSize }}>{buttonText}</Text>
        </TouchableOpacity>
    )
}


export default Button;