import React, {Component} from 'react'
import {TouchableHighlight, View, TouchableOpacity} from 'react-native'
import FastImage from 'react-native-fast-image'
import {
  colorWhite,
  modelBackgroundColor,
  colorBlack,
  darkFont,
  lightFont,
  dividerColor,
  pinkColor,
  lightGrey,
  colorPrimary,
} from '../../styles/Color'
import {
  pagePadding,
  headingFontSize,
  secondFontSize,
  secHeadingFontSize,
  thirdFontSize,
  DEVICE_WIDTH,
} from '../../styles/Dimens'
import {fontFamily, poppinsBold, poppinsRegular} from '../../styles/Fonts'
import Button from '../Button'
import Modal from 'react-native-modal'
import {Text} from '../Elements'
import ImageButton from '../ImageButton'
import CircleTimer from '../TImerComponent/CircleTimer'

export default class WinModel extends Component {
  state = {
    note: '',
  }

  render () {
    return (
      <Modal
        animationType='slide'
        transparent={true}
        style={{flex: 1, margin: -pagePadding}}
        isVisible={this.props.visibility}
        onBackdropPress={() => {
          this.props.onBackPress()
        }}
        onBackButtonPress={() => {
          this.props.onBackPress()
        }}>
        <View
          style={{
            backgroundColor: colorWhite,
            height: '100%',
            width: '100%',
            padding: 20,
            paddingTop:60
          }}>
          {/* <TouchableOpacity
            style={{height: 40, width: 40, zIndex: 10, margin: 20}}
            onPress={() => this.props.onBackPress()}>
            <FastImage
              source={require('../../assets/icons/back_icon.png')}
              resizeMode={FastImage.resizeMode.contain}
              style={{width: 20, height: 20}}
            />
          </TouchableOpacity> */}
          {/* <View
            style={{alignSelf: 'flex-end', marginRight: 20, marginTop: -20}}>
            <CircleTimer
              radius={28}
              borderWidth={5}
              seconds={5}
              textStyle={{fontSize: 10, textAlign: 'center'}}
              borderColor={colorWhite}
              borderBackgroundColor={colorBlack}
              circleColor={colorPrimary}
              onTimeElapsed={() => {
                this.props.onCounterFinish()
                console.log('Timer Finished!')
              }}
              showSecond={true}
            />
          </View> */}
          <View
            style={{
              backgroundColor: colorWhite,
              flex: 1,
              alignItems: 'center',
            }}>
            <Text
              size='xxxl'
              fontWeight={'forte'}
              style={{textAlign: 'center', marginTop: 20}}>
              {'Congratulations'}
            </Text>
            <Text size='xxl' fontWeight={'forte'} style={{textAlign: 'center'}}>
              {'You Win'}
            </Text>

            <FastImage
              source={this.props.headerImage}
              resizeMode={FastImage.resizeMode.contain}
              style={{
                height: DEVICE_WIDTH / 1.5,
                width: '60%',
                alignSelf: 'center',
                marginTop: 10,
              }}
            />
            <Text
              size='s'
              fontColor={colorBlack}
              style={{textAlign: 'center'}}>
              {this.props.winnerName}
            </Text>
            <Text
              size='xxl'
              fontColor={colorBlack}
              fontWeight={'bold'}
              style={{textAlign: 'center', marginTop: 5}}>
              {this.props.winnerScore}
            </Text>
          </View>

          <View
            style={{
              flex: 1,
              width: '100%',
            }}>
            <Button
              buttonText={'Ok'}
              position={'absolute'}
              onPressButton={() => {
                this.props.onOkCLick()
              }}
            />
          </View>
        </View>
      </Modal>
    )
  }
}
