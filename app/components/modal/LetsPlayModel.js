import React, {Component} from 'react'
import {TouchableHighlight, View, TouchableOpacity} from 'react-native'
import FastImage from 'react-native-fast-image'
import {
  colorWhite,
  modelBackgroundColor,
  colorBlack,
  darkFont,
  lightFont,
  dividerColor,
  pinkColor,
  lightGrey,
  colorPrimary,
} from '../../styles/Color'
import {
  pagePadding,
  headingFontSize,
  secondFontSize,
  secHeadingFontSize,
  thirdFontSize,
  DEVICE_WIDTH,
} from '../../styles/Dimens'
import {fontFamily, poppinsBold, poppinsRegular} from '../../styles/Fonts'
import Button from '../Button'
import Modal from 'react-native-modal'
import {Text} from '../Elements'
import ImageButton from '../ImageButton'
import CircleTimer from '../TImerComponent/CircleTimer'

export default class LetsPlayModel extends Component {
  state = {
    note: '',
  }

  render () {
    return (
      <Modal
        animationType='slide'
        transparent={true}
        style={{flex: 1, margin: -pagePadding}}
        isVisible={this.props.visibility}
        onBackdropPress={() => {
          this.props.onBackPress()
        }}
        onBackButtonPress={() => {
          this.props.onBackPress()
        }}>
        <View
          style={{
            backgroundColor: colorWhite,
            height: '100%',
            width: '100%',
            padding: 20,
          }}>
          <TouchableOpacity
            style={{height: 40, width: 40, zIndex: 10, margin: 20}}
            onPress={() => this.props.onBackPress()}>
            <FastImage
              source={require('../../assets/icons/back_icon.png')}
              resizeMode={FastImage.resizeMode.contain}
              style={{width: 20, height: 20}}
            />
          </TouchableOpacity>
          <View
            style={{alignSelf: 'flex-end', marginRight: 20, marginTop: -20}}>
            <CircleTimer
              radius={28}
              borderWidth={5}
              seconds={5}
              textStyle={{fontSize: 10, textAlign: 'center'}}
              borderColor={colorWhite}
              borderBackgroundColor={colorBlack}
              circleColor={colorPrimary}
              onTimeElapsed={() => {
                this.props.onCounterFinish()
                console.log('Timer Finished!')
              }}
              showSecond={true}
            />
          </View>
          <View
            style={{
              backgroundColor: colorWhite,
              flex: 1,
              alignItems: 'center',
            }}>
            <FastImage
              source={this.props.headerImage}
              resizeMode={FastImage.resizeMode.contain}
              style={{
                height: DEVICE_WIDTH / 2,
                width: '50%',
                alignSelf: 'center',
                marginTop: this.props.gameRound < 1 ? 60 : -40,
              }}
            />
            <Text
              size='xxxl'
              fontWeight={'forte'}
              style={{textAlign: 'center', marginTop: 20}}>
              {this.props.gameRound < 1
                ? "Let's Play"
                : 'Round ' + this.props.gameRound + ' Clear'}
            </Text>
            <Text size='xxl' fontWeight={'bold'} style={{textAlign: 'center'}}>
              {this.props.headingText}
            </Text>
            <View
              style={{
                width:'70%',
                flexDirection: 'row',
                justifyContent: 'space-around',
              }}>
              <View>
                <FastImage
                  source={this.props.yourImage}
                  resizeMode={FastImage.resizeMode.contain}
                  style={{
                    height: 70,
                    width: 70,
                    borderRadius: 35,
                    alignSelf: 'center',
                    marginBottom: 10,
                  }}
                />
                <Text
                  size='s'
                  fontColor={lightGrey}
                  style={{textAlign: 'center', marginTop: 5}}>
                  {this.props.yourName}
                </Text>
                {this.props.gameRound < 1 ? null : (
                  <Text
                    size='m'
                    fontColor={colorBlack}
                    fontWeight={'bold'}
                    style={{textAlign: 'center', marginTop: 5}}>
                    {this.props.yourScore}
                  </Text>
                )}
              </View>
              <FastImage
                source={require('../../assets/icons/back_icon.png')}
                resizeMode={FastImage.resizeMode.contain}
                style={{
                  height: 45,
                  width: 60,
                  marginTop:15
                
                }}
              />
              <View>
                <FastImage
                  source={this.props.opponentImage}
                  resizeMode={FastImage.resizeMode.contain}
                  style={{
                    height: 70,
                    width: 70,
                    borderRadius: 35,
                    alignSelf: 'center',
                    marginBottom: 10,
                  }}
                />
                <Text
                  size='s'
                  fontColor={lightGrey}
                  style={{textAlign: 'center', marginTop: 5}}>
                  {this.props.opponentName}
                </Text>
                {this.props.gameRound < 1 ? null : (
                  <Text
                    size='m'
                    fontColor={colorBlack}
                    fontWeight={'bold'}
                    style={{textAlign: 'center', marginTop: 5}}>
                    {this.props.opponentScore}
                  </Text>
                )}
              </View>
            </View>
          </View>
          {this.props.gameRound < 1 ? null : (
            <View
              style={{
                flex: 1,
                width: '100%',
              }}>
              <Button
                buttonText={'Submit'}
                position={'absolute'}
                onPressButton={() => {
                  this.props.onNext()
                }}
              />
            </View>
          )}
        </View>
      </Modal>
    )
  }
}
