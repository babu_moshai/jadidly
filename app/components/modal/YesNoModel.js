import React, { Component } from 'react';
import {  TouchableHighlight, View, Alert } from 'react-native';
import FastImage from 'react-native-fast-image';
import { colorWhite, modelBackgroundColor, colorBlack, darkFont, lightFont, dividerColor, pinkColor, lightGrey } from '../../styles/Color';
import { pagePadding, headingFontSize, secondFontSize, secHeadingFontSize, thirdFontSize, DEVICE_WIDTH } from '../../styles/Dimens';
import { fontFamily, poppinsBold, poppinsRegular } from '../../styles/Fonts';
import Button from '../Button';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import Modal from 'react-native-modal';
import { Text } from '../Elements';

export default class YesNoModel extends Component {

    state = {
        note: ''
    }

    render() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                isVisible={this.props.visibility}
                onBackdropPress={() => {
                    this.props.leftButtonPress()
                }}
                onBackButtonPress={() => {
                    this.props.leftButtonPress()
                }}
                
                >
                    <View style={{ backgroundColor: colorWhite, position: 'absolute', bottom: 0, left: 0, right: 0,margin:-20,padding:pagePadding,borderTopLeftRadius:20,borderTopRightRadius:20 }}>
                    <Text size="xxl" fontWeight={"bold"}  style={{textAlign:'center'}} >{this.props.headingText}</Text>
                    <Text size="s" fontColor={lightGrey}  style={{paddingTop:10,textAlign:'center'}}  >{this.props.secondLine}</Text>

              
                          <FastImage source={require('../../assets/images/well_done.png')}
                           resizeMode={FastImage.resizeMode.contain} style={{height:(DEVICE_WIDTH/2),
                           width:'50%',alignSelf:'center',marginTop:20}} />

                              
                         <Button buttonText={this.props.buttonRight} marginTop={20} colorCode={this.props.type == 'cancel' ? pinkColor : null} onPressButton={() => { this.props.rightButtonPress() }} />
                    </View>
            </Modal>
        );
    }
}