import React, {Component} from 'react'
import {View,Text,FlatList,TouchableOpacity} from 'react-native'
import {colorWhite, colorBlack,colorPrimary, lightFont, cardBorderColor} from '../../styles/Color'
import {pagePadding} from '../../styles/Dimens'
import Modal from 'react-native-modal'
import { fontFamily,fontSize } from '../../styles/Fonts'
import FastImage from 'react-native-fast-image'

const data = []

export default class RequestModel extends Component {
  state = {
    note: '',
  }
  

  render () {
    return (
      <Modal data={this.props.dataList} animationType='slide' transparent={true} isVisible={this.props.visibility}
        crossButton={() => {
          this.props.crossButton()
        }}
        onSelection={(index) =>  {
          this.props.onSelection(index)
        }}>
        
        <View style={{ backgroundColor: colorWhite, position: 'absolute',bottom: 0,left: 0,right: 0,margin: -20,padding: pagePadding,borderTopLeftRadius: 20,borderTopRightRadius:20}}>
          <View style={{flex:1,flexDirection:'row',alignContent:'center'}}>
            <Text style={{fontFamily:fontFamily.medium, color:colorBlack,fontSize:fontSize.xl,flex:1}}>{this.props.headingText}</Text>
            <TouchableOpacity onPress={() => {this.props.crossButton()}}>
                <FastImage  source={require('../../assets/icons/cross_icon.png')} style={{height: 15,width:15,alignSelf:'center'}}/>
            </TouchableOpacity>
            
          </View>
          
          <FlatList
              data={this.props.data}
              showsVerticalScrollIndicator={false}
              style={{flexGrow:0}}
              renderItem={({ item: rowData,index }) => {
                  return(
                      <View style={{flex:1}}>
                        <TouchableOpacity style={{justifyContent:'center',marginTop:5,marginBottom:5}} onPress={() => {this.props.onSelection(index)}}>
                            <Text style={{fontFamily:fontFamily.regular, color:colorPrimary,fontSize:fontSize.m}}>{this.props.headingText=='Select a Subject'?rowData.subject:rowData}</Text>
                        </TouchableOpacity>
                        <View style={{flex:1,backgroundColor:cardBorderColor,height:1}}/>
                      </View>                               
                  );
              }}
              horizontal={false}
              keyExtractor={(item, index) => index}
          />
        </View>
      </Modal>
    )
  }
}
