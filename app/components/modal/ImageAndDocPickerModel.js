import React, {Component} from 'react'
import {Modal, Text, TouchableHighlight, View, StyleSheet} from 'react-native'

import ImagePicker from 'react-native-image-crop-picker'
import { fontFamily } from '../../styles/Fonts'

export default class ImageAndDocPickerModel extends Component {
  state = {
    note: '',
  }

  imagePicker () {
    //console.log('this click')
    const {imageWidth, imageHeight} = this.props
    ImagePicker.openPicker({
    //   height: imageWidth || 600,
    //   width: imageHeight || 900,
      cropping: true,
    }).then(image => {
      console.log(image)
      this.props.imageObject(image)
    })
  }

  openCamera () {
    const {imageWidth, imageHeight} = this.props
    ImagePicker.openCamera({
    //   height: imageWidth || 600,
    //   width: imageHeight || 900,
    compressImageMaxWidth:1000,
    compressImageMaxHeight:1000,
      cropping: true,
    }).then(image => {
      console.log(image)
      this.props.imageObject(image)
    })
  }

  

  render () {
    const {type} = this.props
    return (
      <Modal
        animationType='slide'
        transparent={true}
        visible={this.props.visibility}
        onRequestClose={() => {}}>
        <View style={styles.container}>
          <View style={styles.subContainer}>
            <Text style={styles.title}>{'Select a photo'}</Text>
            <View style={{paddingLeft: 10, paddingTop: 10}}>
              <Text style={styles.secTitle} onPress={() => this.openCamera()}>
                {'Take A Photo'}
              </Text>
              <Text
                style={styles.secTitle}
                onPress={() =>
                 this.imagePicker() 
                }>
                {'Choose from library'}
              </Text>
              <Text
                style={[
                  styles.secTitle,
                  {
                    textAlign: 'right',
                    paddingTop: 0,
                    marginRight: 10,
                    fontWeight: 'bold',
                  },
                ]}
                onPress={() => this.props.onCancel()}>
                {'Cancel'}
              </Text>
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  subContainer: {
    backgroundColor: '#FFF',
    height: 180,
    width: '80%',
    borderRadius: 10,
  },
  secTitle: {
    fontFamily: fontFamily.regular,
    color: '#000',
    fontSize: 15,
    padding: 10,
  },
  title: {
    fontFamily: fontFamily.bold,
    paddingLeft: 20,
    paddingTop: 20,
    fontSize: 20,
    color: '#000',
  },
})
