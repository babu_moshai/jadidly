import React, { Component } from "react";
import {
  View,
  Image,
  ActivityIndicator,
  TouchableOpacity,
  Dimensions
} from "react-native";

import Carousel, { Pagination } from "react-native-snap-carousel"; //Thank From distributer(s) of this lib
import styles from "./MySliderBox.style";
import FastImage from "react-native-fast-image";
// import { placeholderImageColor } from "../../constants/constant";
// import ImageLoad from 'react-native-image-placeholder';
import ImageLoader from "../../ImageLoadWithPlaceHolder/ImageLoader";

// -------------------Props---------------------
// images
// onCurrentImagePressed
// sliderBoxHeight
// parentWidth
// dotColor
// inactiveDotColor
// dotStyle
// paginationBoxVerticalPadding
// circleLoop
// autoplay
// ImageComponent
// paginationBoxStyle
// resizeMethod
// resizeMode
// ImageComponentStyle,
// imageLoadingColor = "#E91E63"

const width = Dimensions.get("window").width;

export class MySliderBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentImage: 0,
      loading: []
    };
    this.onCurrentImagePressedHandler = this.onCurrentImagePressedHandler.bind(
      this
    );
    this.onSnap = this.onSnap.bind(this);
  }
  componentDidMount() {
    let a = [...Array(this.props.images.length).keys()].map(i => false);
  }
  onCurrentImagePressedHandler() {
    if (this.props.onCurrentImagePressed) {
      this.props.onCurrentImagePressed(this.state.currentImage);
    }
  }

  onSnap(index) {
    const { currentImageEmitter } = this.props;
    this.setState({ currentImage: index }, () => {
      if (currentImageEmitter) currentImageEmitter(this.state.currentImage);
    });
  }

  _renderItem({ item, index }) {
    const {
      ImageComponent,
      ImageComponentStyle = {},
      sliderBoxHeight,
      disableOnPress,
      resizeMethod,
      resizeMode,
      imageLoadingColor = "#E91E63"
    } = this.props;
    return (
      <View
        style={{
          position: "relative",
          justifyContent: "center",
        }}
      >
        <TouchableOpacity
          key={index}
        
          activeOpacity={1}
          onPress={() => !disableOnPress && this.onCurrentImagePressedHandler()}
        >
          <ImageLoader
          // placeholderSource={require('../../assets/placeholder-image.png')}
          resizeMode={FastImage.resizeMode.stretch}
          style={[
              {
                width: "100%",
                height: sliderBoxHeight || 200,
                alignSelf: "center",
              },
              ImageComponentStyle
            ]}
            source={ typeof item === "string" ? { uri: item } : item}
           
          // {...this.props}
          />
        </TouchableOpacity>
     

       
      </View>
    );
  }

  get pagination() {
    const { currentImage } = this.state;
    const {
      images,
      dotStyle,
      dotColor,
      inactiveDotColor,
      paginationBoxStyle,
      paginationBoxVerticalPadding
    } = this.props;
    return (
      <Pagination
        borderRadius={2}
        dotsLength={images.length}
        activeDotIndex={currentImage}
        dotStyle={dotStyle || styles.dotStyle}
        inactiveDotStyle={dotStyle || styles.inActiveDotStyle}
        inactiveDotScale={0.4}
        carouselRef={this._ref}
        inactiveDotOpacity={0.8}
        tappableDots={!!this._ref}
        containerStyle={[
          styles.paginationBoxStyle,
          paginationBoxVerticalPadding
            ? { paddingVertical: paginationBoxVerticalPadding }
            : {},
          paginationBoxStyle ? paginationBoxStyle : {}
        ]}
        {...this.props}
      />
    );
  }


  render() {
    const {
      images,
      circleLoop,
      autoplay,
      parentWidth,
      loopClonesPerSide
    } = this.props;
    return (
      <View>
        <Carousel
          layout={"default"}
          data={images}
          ref={c => (this._ref = c)}
          loop={circleLoop || false}
          enableSnap={true}
          autoplay={autoplay || false}
          itemWidth={parentWidth || width}
          sliderWidth={parentWidth || width}
          loopClonesPerSide={loopClonesPerSide || 5}
          renderItem={item => this._renderItem(item)}
          onSnapToItem={index => this.onSnap(index)}
          {...this.props}
        />
        {images.length > 1 && this.pagination}
      </View>
    );
  }
}

const colors = {
  dotColors: "#BDBDBD",
  white: "#FFFFFF"
};

MySliderBox.defaultProps = {
  ImageComponent: Image
};
