export default {
  paginationBoxStyle: {
    bottom: 60,
  
  },
  dotStyle: {
    width: 10,
    height: 4,
    borderRadius: 2,
    marginHorizontal: -14,
    backgroundColor: "#ffe392"
  },
  inActiveDotStyle: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: -14,
    backgroundColor: "#fcb153"
  }
};
