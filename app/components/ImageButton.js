import React, { Component } from 'react';
import { TouchableOpacity, Platform, StyleSheet } from 'react-native';
import { pagePadding, btnFontSize } from '../styles/Dimens'
import { colorBlack, colorPrimary, colorWhite } from '../styles/Color'
import { fontFamily } from '../styles/Fonts';
import FastImage from 'react-native-fast-image';
import { Text } from './Elements';



const ImageButton = ({ buttonType, onPressButton, buttonText, marginTop,noPadding, colorCode,borderWidth,borderColor, fontSizeReceive, textColor,btnHeight ,radius,fontStyle}) => {
    return (
        <TouchableOpacity
          style={{
            alignSelf: 'flex-end',
            height: 50,
            width: 120,
            justifyContent: 'center',
            alignItems: 'center',
            marginRight: 10,
          }}
          onPress={()=>onPressButton()}
          >
          <FastImage
            source={buttonType=='yes' ? require('../assets/images/empty_image.png') : require('../assets/images/empty_image.png')}
            resizeMode={FastImage.resizeMode.contain}
            style={{height: 50, width: 120}}
          />
          <Text
            style={{position: 'absolute'}}
            size={'l'}
            fontColor={colorWhite}
            fontWeight='bold'>
            {buttonText}
          </Text>
        </TouchableOpacity>
    )
}


export default ImageButton;