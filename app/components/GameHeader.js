import React, { Component } from 'react';
import { TouchableOpacity, Platform, View, Image } from 'react-native';
import FastImage from 'react-native-fast-image';
import { colorBlack, colorPrimary, colorPrimaryDark, colorWhite } from '../styles/Color';
import { fontFamily } from '../styles/Fonts';
import { Text } from './Elements';
import CircleTimer from './TImerComponent/CircleTimer';



const GameHeader = ({ yourScore,opponentScore, gameRound,yourImage,opponentImage,yourName,opponentName,timmer  }) => {
    return (
        <View  style={{backgroundColor:colorPrimaryDark,height:110}}>
        <View
          style={{
            flex:3,
            paddingTop:20,
            paddingLeft:20,
            paddingRight:20,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View style={{flex:1}}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <FastImage
                source={yourImage}
                resizeMode={FastImage.resizeMode.contain}
                style={{
                  height: 50,
                  width: 50,
                  borderRadius: 25,
                  alignSelf: 'center',
                }}
              />
              <View style={{marginLeft: 10}}>
                <Text size={'xs'} fontColor={colorWhite} fontWeight='bold'>
                  Score
                </Text>
                <Text size={'m'} fontColor={colorBlack} fontWeight='bold'>
                  {yourScore}
                </Text>
              </View>
            </View>
            <Text
              size={'xs'}
              fontColor={colorWhite}
              style={{marginTop: 5}}
              fontWeight='bold'>
              {yourName}
            </Text>
          </View>
          <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <CircleTimer
              radius={25}
              borderWidth={5}
              seconds={timmer}
              textStyle={{fontSize: 10, textAlign: 'center'}}
              borderColor={colorWhite}
              borderBackgroundColor={colorBlack}
              circleColor={colorWhite}
              onTimeElapsed={() => {
                console.log('Timer Finished!')
              }}
              showSecond={true}
            />
            <Text
              size={'m'}
              fontColor={colorBlack}
              fontWeight='bold'>
              Round-0{gameRound}
            </Text>
          </View>
          <View style={{flex:1,alignItems:'flex-end'}}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <View style={{marginLeft: 10, marginRight: 10}}>
                <Text
                  size={'xs'}
                  fontColor={colorWhite}
                  style={{textAlign: 'right'}}
                  fontWeight='bold'>
                  Score
                </Text>
                <Text
                  size={'m'}
                  fontColor={colorBlack}
                  style={{textAlign: 'right'}}
                  fontWeight='bold'>
                  {opponentScore}
                </Text>
              </View>

              <FastImage
                source={opponentImage}
                resizeMode={FastImage.resizeMode.contain}
                style={{
                  height: 50,
                  width: 50,
                  borderRadius: 25,
                  alignSelf: 'center',
                }}
              />
            </View>
            <Text
              size={'xs'}
              fontColor={colorWhite}
              style={{textAlign: 'right',marginTop: 5}}
              fontWeight='bold'>
              {opponentName}
            </Text>
          </View>
        </View>
      </View>
    )
}


export default GameHeader;