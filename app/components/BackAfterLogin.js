import React, { Component } from 'react';
import { TouchableOpacity, Text, Platform, View, Image } from 'react-native';
import FastImage from 'react-native-fast-image';
import { colorBlack, colorPrimary, colorPrimaryDark, colorWhite, windowBackground } from '../styles/Color';
import { fontFamily } from '../styles/Fonts';



const BackAfterLogin = ({ backgroundColor,position, onPressButton, pageText, type, onSearchClick, colorCode, fontSizeReceive, textColor, paddingFrom,search }) => {
    return (
        <View style={{backgroundColor:type=='white'?colorWhite:colorPrimaryDark,height:74,justifyContent:'center'}}>
                <View style={{ flex: 1,flexDirection: 'row', alignItems: 'center',marginTop:25}}>
                        <TouchableOpacity style={{ width: null }} onPress={() => onPressButton()}>
                            <View style={{ flexDirection: 'row', alignItems: 'center', marginStart: 20, width: null }}>
                                <Image style={{ width: 20, height: 20, marginEnd: 5 }} resizeMode={'contain'} source={type=='white'?require('../assets/icons/back_icon.png') :require('../assets/icons/back_icon.png')}></Image>
                               
                            </View>
                        </TouchableOpacity>
               
                    <Text style={{ fontFamily: fontFamily.bold, fontSize: 18, color:type=='white'? colorBlack: colorWhite, marginStart:10 }} numberOfLines={1}>{pageText}</Text>
                    {search?
                    <TouchableOpacity style={{ width: null,position:'absolute',right:10,height:40,
                    width:40,justifyContent:'center',alignItems:'center' }} onPress={() => onSearchClick()}>

                    <Image style={{ width: 25, height: 25,  }} resizeMode={'contain'} source={require('../assets/icons/search_icon.png')}></Image>
                     </TouchableOpacity>
                     :null}
                </View>
               
        </View>
    )
}


export default BackAfterLogin;