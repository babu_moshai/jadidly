import React, {Component, useState, useRef, useEffect} from 'react'

import {View, TouchableOpacity, TextInput, FlatList} from 'react-native'
import FastImage from 'react-native-fast-image'
import {
  colorPrimary,
  colorPrimaryDark,
  colorPrimaryLight,
  colorWhite,
  darkFont,
  pinkColor,
} from '../../styles/Color'
import {DEVICE_WIDTH} from '../../styles/Dimens'
import {fontFamily, fontSize} from '../../styles/Fonts'
import Button from '../Button'
import {Text} from '../Elements'

const ConnectedList = ({item, type,onRequestClick}) => {
  return (
    <View
      style={{
        padding: 10,
        marginTop: 10,
        borderRadius: 10,
        overflow: 'hidden',
        backgroundColor: colorPrimaryLight,
      }}>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <FastImage
          style={{width: 60, height: 60, borderRadius: 30}}
          resizeMode={FastImage.resizeMode.contain}
          source={item.image=='undefined' || item.image =='' ?
            require('../../assets/images/dummy_image.png'): {uri:item.image} }
        />
        <View style={{flex: 1, marginLeft: 10}}>
          <Text fontWeight={'bold'}>{item.user.name}</Text>
          <View
            style={{
              flex: 1,
              justifyContent: 'space-evenly',
              flexDirection: 'row',
              marginTop: 10,
            }}>
            <View style={{flex: 1, marginRight: 10}}>
              <Button
                btnHeight={30}
                buttonText={'Request'}
                onPressButton={() => onRequestClick()}
                colorCode={colorWhite}
                textColor={darkFont}
                fontSizeReceive={fontSize.s}
                radius={10}
                fontStyle={fontFamily.light}
                borderWidth={1}
                borderColor={colorPrimary}
                noPadding
              />
            </View>
            <View style={{flex: 1, marginLeft: 10}}>
              {type == 'new' ? null : (
                <Button
                  btnHeight={30}
                  fontSizeReceive={fontSize.s}
                  buttonText={'Remove'}
                  fontStyle={fontFamily.light}
                  colorCode={colorWhite}
                  radius={10}
                  noPadding
                  borderWidth={1}
                  borderColor={colorPrimary}
                  onPressButton={() => {
                    null
                  }}
                />
              )}
            </View>
          </View>
        </View>
      </View>
    </View>
  )
}

export default ConnectedList
