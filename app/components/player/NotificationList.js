import React, {Component, useState, useRef, useEffect} from 'react'

import {View, TouchableOpacity, TextInput, FlatList} from 'react-native'
import FastImage from 'react-native-fast-image'
import {
  colorPrimary,
  colorPrimaryDark,
  colorPrimaryLight,
  colorWhite,
  darkFont,
  lightGrey,
  pinkColor,
} from '../../styles/Color'
import {DEVICE_WIDTH} from '../../styles/Dimens'
import {fontFamily, fontSize} from '../../styles/Fonts'
import Button from '../Button'
import {Text} from '../Elements'

const NotificationList = ({item, type, onRequestClick}) => {
  return (
    <View
      style={{
        padding: 10,
        marginTop: 10,
        borderRadius: 10,
        overflow: 'hidden',
        backgroundColor: colorPrimaryLight,
      }}>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <FastImage
          style={{width: 60, height: 60, borderRadius: 30}}
          resizeMode={FastImage.resizeMode.contain}
          source={require('../../assets/images/dummy_image.png')}
        />
        <View style={{flex: 1, marginLeft: 10}}>
          <View style={{justifyContent: 'space-between', flexDirection: 'row'}}>
            <Text fontWeight={'bold'}>{item.name}</Text>
            <Text size='xss' fontColor={lightGrey}>
              {'02 Min ago'}
            </Text>
          </View>
          <Text size='xss' fontColor={lightGrey} style={{marginTop:5}}>
            {
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do e est laborum.'
            }
          </Text>
        </View>
      </View>
    </View>
  )
}

export default NotificationList
