import React, {Component, useState, useRef, useEffect} from 'react'

import {View, TouchableOpacity, TextInput, FlatList} from 'react-native'
import FastImage from 'react-native-fast-image'
import {
  brownColor,
  colorPrimary,
  colorPrimaryDark,
  colorPrimaryLight,
  colorWhite,
  darkFont,
  lightGrey,
  pinkColor,
} from '../../styles/Color'
import {DEVICE_WIDTH} from '../../styles/Dimens'
import {fontFamily, fontSize} from '../../styles/Fonts'
import Button from '../Button'
import {Text} from '../Elements'


const TextsComponent =({heading,value})=>{
  return(
    <View style={{flex:1}}>
    <Text size='xss' fontColor={lightGrey}>
      {heading}
    </Text>
    <Text fontColor={brownColor} style={{marginTop: 10}} fontWeight='bold'>
     {value}
    </Text>
  </View>
  )
}

const HistoryList = ({item, type, onRequestClick}) => {
  return (
    <View
      style={{
        padding: 15,
        paddingBottom:20,
        paddingTop:20,
        marginTop: 10,
        borderRadius: 10,
        overflow: 'hidden',
        backgroundColor: colorPrimaryLight,
        flexDirection: 'row',
        alignItems: 'center',

      }}>

        <TextsComponent heading='Game id' value='WN21411'/>
        <TextsComponent heading='v/s' value='Swati'/>
        <TextsComponent heading='Status' value='WON'/>
        <TextsComponent heading='Date' value='25||Dec'/>
     
    </View>
  )
}

export default HistoryList
