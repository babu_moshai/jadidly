import React, {Component} from 'react'
import {TouchableOpacity, View, StyleSheet} from 'react-native'
import {pagePadding, btnFontSize, DEVICE_WIDTH} from '../styles/Dimens'
import {colorPrimaryDark, colorPrimary, colorWhite} from '../styles/Color'
import {fontFamily} from '../styles/Fonts'
import LinearGradient from 'react-native-linear-gradient'
import FastImage from 'react-native-fast-image'
import { Text } from './Elements'

const HomeCard = ({image,text,onClick,imageCenter}) => {
  return (
    <TouchableOpacity onPress={() => onClick()}>
      <LinearGradient
        style={{
          width: DEVICE_WIDTH - 40,
          height: 120,
          marginTop: 20,
          padding: 10,
          alignSelf: 'center',
          justifyContent: 'center',
          borderRadius: 15,
        }}
        colors={[colorPrimary, colorPrimaryDark]}
        start={{x: 0, y: 1}}
        end={{x: 1, y: 0}}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            flex: 1,
          }}>
          <FastImage
            style={{
              width: 90,
              height: 100,
              flex: 0.5,
              borderRadius: 15,
              marginTop:imageCenter?0 : 20,
            }}
            resizeMode={FastImage.resizeMode.contain}
            source={image}
          />
          <Text
            style={{flex: 0.5, textAlign: 'center'}}
            fontColor={colorWhite}
            fontWeight={'bold'}
            size={'xl'}
           >
            {text}
          </Text>
        </View>
      </LinearGradient>
    </TouchableOpacity>
  )
}

export default HomeCard
