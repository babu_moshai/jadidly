import React, { Component, useState } from 'react';
import { TouchableOpacity, Text, StyleSheet, View } from 'react-native';
import { colorPrimary, colorWhite, colorBlack, lightGrey } from '../styles/Color'
import { fontFamily, fontSize } from '../styles/Fonts'
import FastImage from 'react-native-fast-image';
import {useNavigation, CommonActions} from '@react-navigation/native'


const MenuComponent = ({ position, onPressButton, buttonText, buttonType, marginTop, colorCode, fontSizeReceive, imgUrl }) => {
    const navigation = useNavigation()
    const [pressStatus,setPressStatus] = useState(false)
    const click = async () =>{
        setPressStatus(true)
    }
    const click2 = async () =>{
        setPressStatus(false)
    }


    return (
        <TouchableOpacity onPressIn={() => {click()}} onPressOut={() => {click2()}} style={pressStatus?{backgroundColor:colorPrimary}:{colorWhite}} onPress={() => {onPressButton()}} activeOpacity={1} >
            <View style={{ flexDirection: 'row', alignItems: 'center',marginStart:10, padding: 10,marginTop:5 }}>
                <FastImage source={imgUrl} tintColor={pressStatus?colorWhite:colorBlack} resizeMode={FastImage.resizeMode.contain} style={{ height: 20, width: 20 }} />
                <Text style={{color:pressStatus?colorWhite:colorBlack,fontFamily:fontFamily.regular,fontSize:fontSize.m,marginStart:15}}>{buttonText}</Text>
            </View>
        </TouchableOpacity>
    )

}


export default MenuComponent;