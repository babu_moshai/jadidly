import React, {Component, useState, useRef, useEffect} from 'react'

import {View, TouchableOpacity, TextInput, FlatList,Animated} from 'react-native'
import FastImage from 'react-native-fast-image'
import {colorPrimaryLight, colorWhite} from '../../styles/Color'
import {DEVICE_WIDTH} from '../../styles/Dimens'
import {Text} from '../Elements'
import ImageButton from '../ImageButton'

export default class TempWordList extends Component {

  constructor(props) {
    super(props);

    this._animated = new Animated.Value(-100);
  }

  componentDidMount() {
    Animated.timing(this._animated, {
      toValue: 1,
      duration: 300
    }).start();
    
  }

  render () {
    const {item} = this.props
    return (
      <Animated.View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'row',
          margin: 2,
          transform: [
            { translateY: this._animated },
           
          ]
        }}>
        <FastImage
          source={require('../../assets/images/game_circle.png')}
          resizeMode={FastImage.resizeMode.contain}
          style={{
            height: 35,
            width: 35,
           
          }}
        />
        <Text
          style={{position: 'absolute'}}
          size={'l'}
          fontColor={colorWhite}
          fontWeight='bold'>
          {item}
        </Text>
      </Animated.View>
    )
  }
}

