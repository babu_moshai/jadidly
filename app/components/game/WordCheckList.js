import React, {Component, useState, useRef, useEffect} from 'react'

import {View, TouchableOpacity, TextInput, FlatList} from 'react-native'
import FastImage from 'react-native-fast-image'
import {colorPrimaryLight, colorWhite} from '../../styles/Color'
import {DEVICE_WIDTH} from '../../styles/Dimens'
import {Text} from '../Elements'
import ImageButton from '../ImageButton'

const WordCheckList = ({item,onYesClick}) => {
  return (
    <View style={{padding:20,marginTop:10,borderRadius:10,overflow:'hidden', alignItems: 'center',backgroundColor:colorPrimaryLight}}>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        {item.word.split('').map(charcter => (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
              margin: 2,
            }}>
            <FastImage
              source={require('../../assets/images/game_circle.png')}
              resizeMode={FastImage.resizeMode.contain}
              style={{height: 35, width: 35}}
            />
            <Text
              style={{position: 'absolute'}}
              size={'l'}
              fontColor={colorWhite}
              fontWeight='bold'>
              {charcter}
            </Text>
          </View>
        ))}
      </View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-around',
          marginTop:5
        }}>

          <ImageButton 
          onPressButton={()=>onYesClick()}
           buttonText={'Word'}
           buttonType={'yes'}
          />
        <ImageButton 
          onPressButton={()=>null}
           buttonText={'Not'}
          />
      </View>
    </View>
  )
}

export default WordCheckList
