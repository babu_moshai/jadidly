import React, {Component, useState, useRef, useEffect} from 'react'

import {View, TouchableOpacity, TextInput, Image, ImageBackground} from 'react-native'
import FastImage from 'react-native-fast-image'
import { ScrollView } from 'react-native-gesture-handler'
import { colorWhite } from '../../styles/Color'
import {Text} from '../Elements'

const GameList = ({item,onListRemoveClick}) => {
  return (
  <ImageBackground source={require('../../assets/images/box.png')}
  resizeMode='contain'
  style={{flexDirection:'row',marginTop:1,height:65,marginBottom:1,padding:10,paddingBottom:15,alignItems:'center',justifyContent:'space-between'}}>
 {/* <ImageBackground source={require('../../assets/images/box.png')}
 resizeMode={FastImage.resizeMode.contain}
 style={{height:60,width:'100%',marginTop:10,marginBottom:10}}/> */}
<View style={{flexDirection:'row',alignItems:'center',flex:1}}>
  <ScrollView horizontal={true}>
  {item.word.split('').map(charcter => (
    <View style={{justifyContent: 'center', alignItems: 'center',flexDirection:'row',margin:2}}>
      <FastImage
        source={require('../../assets/images/game_circle.png')}
        resizeMode={FastImage.resizeMode.contain}
        style={{height: 32, width: 32}}
      />
      <Text
        style={{position: 'absolute'}}
        size={'l'}
        fontColor={colorWhite}
        fontWeight='bold'>
        {charcter}
      </Text>
    </View>
  ))}
  </ScrollView>
  </View>
  <TouchableOpacity
  style={{height:30,width:30,justifyContent:'center',alignItems:'center'}}
  onPress={()=>onListRemoveClick()}
  >
  <Image source={require('../../assets/icons/back_icon.png')} resizeMode={FastImage.resizeMode.contain}
   style={{height:15,width:15,tintColor:colorWhite}} />

  </TouchableOpacity>
  </ImageBackground>
  )
}

export default GameList
