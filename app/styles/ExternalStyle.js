import { StyleSheet,Platform } from 'react-native'
import { colorPrimary, colorPrimaryDark, colorWhite, colorBlack } from './Color';
import { DEVICE_WIDTH, inputFieldHeight } from './Dimens';
import { fontFamily } from './Fonts';

export const uniformShadow = {
    ...Platform.select({
        android: {
            elevation: 2,
        },
        ios: {
            shadowColor: '#000',
            shadowOffset: {
                width: 0,
                height: 1,
            },
            shadowOpacity: 0.2,
            shadowRadius: 1.41,
        },
    })

}
export const uniformShadowBubble = {
    ...Platform.select({
        android: {
            elevation: 8,
        },
        ios: {
            shadowColor: '#000',
            shadowOffset: {
                width: 0,
                height: 1,
            },
            shadowOpacity: 0.2,
            shadowRadius: 1.41,
        },
    })
}
export const uniformShadowBubbleInside = {
    ...Platform.select({
        android: {
            elevation: 10,
        },
        ios: {
            shadowColor: '#000',
            shadowOffset: {width: 0,height: 1,},
            shadowOpacity: 0.2,
            shadowRadius: 1.41,
        },
    })
}

const circleDesign = StyleSheet.create({  
    halfRound: {backgroundColor:colorPrimaryDark,height:DEVICE_WIDTH+50,width:DEVICE_WIDTH+50,borderRadius:(DEVICE_WIDTH+25)/2,marginTop:-240,left:-30,right:50},
    homeRound: {backgroundColor:colorPrimaryDark,height:DEVICE_WIDTH+50,width:DEVICE_WIDTH+50,borderRadius:(DEVICE_WIDTH+25)/2,marginTop:-150,left:-30,right:50},
    fullRound: {backgroundColor:'rgba(240,209,152,.55)',height:DEVICE_WIDTH-10,alignItems:'center',justifyContent:'center',alignSelf:'center',width:DEVICE_WIDTH-10,borderRadius:(DEVICE_WIDTH-10)/2,marginTop:10,},
    fullLittleRound: {backgroundColor:'rgba(240,209,152,1)',height:DEVICE_WIDTH-80,width:DEVICE_WIDTH-80,borderRadius:(DEVICE_WIDTH-80)/2,},

});
const trangleDesign = StyleSheet.create({  
    triangle: {
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderLeftWidth: (DEVICE_WIDTH-100)/2,
        borderRightWidth: (DEVICE_WIDTH-100)/2,
        borderBottomWidth: DEVICE_WIDTH-100,
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'red'
      }
});
const styles = StyleSheet.create({
    defaultTextFieldStyle : {
        width : 55, 
        height : 55, 
        textAlign : 'center',
        color: '#000', 
    }
});
const myStyles = StyleSheet.create({  
        inputBox: {
            height: inputFieldHeight,
            flexDirection: 'row',
            marginTop:15,
            alignContent: 'center', 
            alignItems: 'center',
            backgroundColor:colorWhite,
            borderRadius:10,
            ...Platform.select({
                android: {
                    elevation: 10,
                },
                ios: {
                    shadowColor: '#000',
                    shadowOffset: {width: 0,height: 1,},
                    shadowOpacity: 0.2,
                    shadowRadius: 1.41,
                },
            })
        },
        inputBoxMessage: {
            height: 15,
            flexDirection: 'row',
            alignContent: 'center', 
            alignItems: 'flex-start',
            backgroundColor:colorWhite,
            borderRadius:7,
        },
        inputStyle: {
            flex: 1, fontFamily: fontFamily.regular,paddingLeft:10,paddingRight:10,marginBottom:-5,
            color:colorBlack
        },
        inputPlaceHolderStyle: {
            flex: 1, fontFamily: fontFamily.regular,paddingLeft:10,paddingRight:10,
            color:colorBlack
        },
        ccpStyleText:{
            fontFamily:fontFamily.regular,
            height:25,
            marginStart:10,
            marginBottom:-5,
            alignSelf:'center',
            position:'absolute',
            top:13
        },
        buttonBackground:{
            backgroundColor:colorWhite, borderRadius:10, height:45,
            ...Platform.select({
                android: {
                    elevation: 10,
                },
                ios: {
                    shadowColor: '#000',
                    shadowOffset: {width: 0,height: 1,},
                    shadowOpacity: 0.2,
                    shadowRadius: 1.41,
                },
            })
        },
        otpInputBox: {
            height: 50,
            width:50,
            flex:1,
            flexDirection: 'row',
            alignContent: 'center', 
            alignItems: 'center',
            justifyContent:'center',
            backgroundColor:colorWhite,
            borderRadius:25,
            ...Platform.select({
                android: {
                    elevation: 10,
                },
                ios: {
                    shadowColor: '#000',
                    shadowOffset: {width: 0,height: 1,},
                    shadowOpacity: 0.2,
                    shadowRadius: 1.41,
                },
            })
        },
        otpInputStyle: {
            fontFamily: fontFamily.bold,textAlign:'center',alignSelf:'center', height: 45,width:22,fontSize:20
        },
        otpInputPlaceHolderStyle: {
            fontFamily: fontFamily.bold,textAlign:'center',fontSize:20
        },
        elevation:{
            ...Platform.select({
                android: {
                    elevation: 10,
                },
                ios: {
                    shadowColor: '#000',
                    shadowOffset: {width: 0,height: 1,},
                    shadowOpacity: 0.2,
                    shadowRadius: 1.41,
                },
            })
        },
        cardBox: {
            padding:10,
            marginTop:15,
            backgroundColor:colorWhite,
            borderRadius:10,
            ...Platform.select({
                android: {
                    elevation: 10,
                },
                ios: {
                    shadowColor: '#000',
                    shadowOffset: {width: 0,height: 1,},
                    shadowOpacity: 0.2,
                    shadowRadius: 1.41,
                },
            })
        },
        selectedButton:{
            backgroundColor:colorPrimary, borderRadius:10, height:30, paddingStart:10,paddingEnd:10,justifyContent:'center',
            ...Platform.select({
                android: {
                    elevation: 10,
                },
                ios: {
                    shadowColor: '#000',
                    shadowOffset: {width: 0,height: 1,},
                    shadowOpacity: 0.2,
                    shadowRadius: 1.41,
                },
            })
        },
        unselectedButton:{
            backgroundColor:colorWhite, borderRadius:10, height:30, paddingStart:10,paddingEnd:10,justifyContent:'center',
            ...Platform.select({
                android: {
                    elevation: 10,
                },
                ios: {
                    shadowColor: '#000',
                    shadowOffset: {width: 0,height: 1,},
                    shadowOpacity: 0.2,
                    shadowRadius: 1.41,
                },
            })
        },
  });
  export { styles,myStyles,circleDesign,trangleDesign} 