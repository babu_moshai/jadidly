import { Platform } from 'react-native'

export const fontSize = {
    xss: 10,
    xs: 12,
    s: 14,
    m: 14.5,
    l: 18,
    xl: 20,
    xxl: 25,
    xxxl: 30,
    xxxxl: 35,
}


export const fontFamily = {
    bold: 'Poppins-Bold',
    medium: 'Poppins-Medium',
    regular: 'Poppins-Regular',
    semibold: 'Poppins-SemiBold',
    light: 'Poppins-Light'
}
