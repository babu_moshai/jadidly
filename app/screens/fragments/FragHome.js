import React,{useEffect,useState,useRef} from 'react'
import {View,TouchableOpacity,Text,FlatList,StyleSheet,Alert} from 'react-native'
import StatusBarComponent from '../../components/StatusBarComponent';
import { colorPrimary, colorWhite, colorBlack, whiteTranslucent, colorPrimaryLight, lightFont} from '../../styles/Color';
import { DEVICE_HEIGHT, DEVICE_WIDTH} from '../../styles/Dimens';
import FastImage from 'react-native-fast-image'
import { fontFamily, fontSize } from '../../styles/Fonts';
import { useNavigation } from '@react-navigation/native'
import { isLoggedInSet, isLoggedInGet , getDataOfUser, logoutUser } from '../../database/UserData';
import CircleList from 'react-native-circle-list'
import { myStyles } from '../../styles/ExternalStyle';
import Carousel from 'react-native-snap-carousel';

const FragHome = ({dataRes}) => {
    const navigation = useNavigation()
    const [updated, setUpdated] = useState(true)
    const [userData,setUserData] = useState({})
    const [length, setLength] = useState(5)
    const [cateList, setCateList] = useState([])
    const [offersList, setOffersList] = useState([])
    const data = [{id:0,value:0},{id:1,value:1},{id:2,value:2},{id:3,value:3},{id:4,value:4},{id:5,value:5},{id:6,value:6},{id:7,value:7}]
    const carouselRef = useRef(null);
    useEffect(() => {
      checkPrefs()
      setData()
    },[])

    const setData = async() => {
        setUserData(await getDataOfUser())
    }
    const generateMockData = elementCount => {
      const _calc = (data, count) => {
          const newCount = count + 1
          const newData = data.concat({
              id: count,
              value: count,
          })
          if (count < elementCount) {
              return _calc(newData, newCount)
          } else {
              return newData
          }
      }
      return _calc([], 0)
    }
    const checkPrefs = async () => {
      await isLoggedInSet("1")
      setLength(10)
      setCateList(dataRes.category_list)
      carouselRef.current.scrollToIndex(1,250)
    }
    const _keyExtractor = item => item.id
    const _renderItem2 = ({item,index}) => {
      return (
          <TouchableOpacity activeOpacity={1} style={[ myStyles.elevation,{justifyContent:'center',backgroundColor:'white',height:120,width:120,borderRadius:60,marginTop:30}]} 
            onPress={() => { console.log(index)}}>
              <FastImage style={{width: 50, height: 50,alignSelf:'center'}} resizeMode={'contain'} source={require('../../assets/images/empty_image.png')}/>
              <Text style={{fontSize:fontSize.s,textAlignVertical:'center',marginTop:5,alignSelf:'center',fontFamily:fontFamily.medium,color:colorPrimary,textAlign:'center'}}>{item.value}</Text>
          </TouchableOpacity>
          )
    };
    const _renderItem = ({item,index}) => {
      return (
          <TouchableOpacity style={[myStyles.elevation,{transform: [{ scaleY: -1 }],justifyContent:'center',backgroundColor:'white',height:86,width:86,borderRadius:43,marginBottom:-110}]} 
            onPress={() => { console.log(index)}}>
              <FastImage style={{width: 35, height: 30,alignSelf:'center'}} resizeMode={'stretch'} source={require('../../assets/images/empty_image.png')}/>
              <Text style={{fontSize:fontSize.xs,textAlignVertical:'center',marginTop:5,alignSelf:'center',fontFamily:fontFamily.regular,color:colorPrimary,textAlign:'center'}}>{item.value}</Text>
          </TouchableOpacity>
          )
    };
    const showGuestDialog = async () => {
      Alert.alert("Login", "You need to register/login to get access.", 
        [
          {text: "Not Now", onPress: () => {}},
          {text: "Register/Login", onPress: () => { 
            logoutUser()
            handleScreen("WelcomeScreen")
          }},
        ]
        ,{ cancelable: true }
      )
    }
    return (
      <View style={{flex:1}}>
      <StatusBarComponent textColor={true}/>
      <FastImage style={{width: DEVICE_WIDTH+120,marginStart:-60, height: DEVICE_HEIGHT/2.6,position:'absolute',borderBottomLeftRadius:DEVICE_WIDTH,borderBottomRightRadius:DEVICE_WIDTH}}
      source={require('../../assets/images/splash_bg.png')}/>
      <View style={{width:DEVICE_WIDTH/1.5,height:DEVICE_WIDTH/1.5,top:-80,start:-50,position:'absolute', backgroundColor:whiteTranslucent,borderRadius:DEVICE_WIDTH/1.5}}></View>
      <View style={{width:DEVICE_WIDTH/2.5,height:DEVICE_WIDTH/2.5,top:-60,start:150,position:'absolute', backgroundColor:whiteTranslucent,borderRadius:DEVICE_WIDTH/2.5}}></View>
      <View style={{flexDirection:'row',marginTop:40,marginStart:15,marginEnd:15, alignItems:'center'}}>
          <TouchableOpacity onPress={() => {navigation.openDrawer()}}>
            <FastImage style={{width: 25, height: 15}} source={require('../../assets/icons/menu.png')}/>
          </TouchableOpacity>
          <View style={{flex:1}}/>
          <TouchableOpacity onPress={() => {userData.id != "0"?navigation.navigate('Notifications'):showGuestDialog()}}>
            <FastImage style={{width: 18, height: 23}} source={require('../../assets/icons/Notification.png')}/>
          </TouchableOpacity>
      </View>
      <FastImage
      style={{width: 150, height: 150,alignSelf:'center',marginTop:-20}}
      resizeMode={"contain"}
      source={require('../../assets/icons/splash_image.png')}/>
      <View style={styles.carouselContainer}>
      <Carousel
        ref={carouselRef}
        data={data}
        renderItem={_renderItem2}
        sliderWidth={DEVICE_WIDTH+15}
        itemWidth={140}
        inactiveSlideOpacity={1}
        inactiveSlideScale={0.8}
        loop={true}
        enableMomentum={true}
        inactiveSlideShift={-40}
      />
          {/* <CircleList containerStyle={{height:160 , width:DEVICE_WIDTH,transform: [{ scaleY: -1 }]}}
              data={generateMockData(length)}
              keyExtractor={_keyExtractor}
              renderItem={_renderItem}
              elementCount={3}
              selectedItemScale={1.20}
              radius={DEVICE_WIDTH/1.4}
              swipeSpeedMultiplier={80}
              visiblityPadding={5}
              ref={carouselRef}
          /> */}
        {/* <Carousel  
          style={styles.carousel}
          data={data}
          renderItem={renderItem}
          itemWidth={125}
          itemContainerStyle={{marginTop:30}}
          containerWidth={DEVICE_WIDTH} 
          separatorWidth={0}
          ref={carouselRef}
          inActiveOpacity={0.8}
          //pagingEnable={false}
          //minScrollDistance={20}
          />         */}
      </View>
      <View style={{flexDirection:'row',marginStart:15,marginEnd:15, alignItems:'center'}}>
          <Text style={{fontSize:fontSize.l, fontFamily:fontFamily.semibold,color:colorBlack,textAlignVertical:'center',maxHeight:'90%'}} numberOfLines={2}>Offers</Text>    
          <View style={{flex:1}}/>
          <TouchableOpacity onPress={() => {}}>
            <Text style={{fontSize:fontSize.xs,textAlign:'center',textAlignVertical:'center',fontFamily:fontFamily.medium, color:lightFont}}>View All</Text>
          </TouchableOpacity>
      </View>
      <View style={{width:40,height:3,backgroundColor:colorPrimary,margin:15,marginTop:-4,borderRadius:10}}/>
      <FlatList
          extraData={updated}
          data={data}
          showsVerticalScrollIndicator={false}
          style={{flexGrow:0}}
          renderItem={({ item: rowData }) => {
              return(
                  <View style={styles.cardBackground}>
                      <FastImage style={{position:'absolute',width:'100%',height:150}} source={require('../../assets/images/offer_list_dummy.png')}/>
                      <View style={{flexDirection:'row'}}>
                        <View style={{height:60,width:'50%',backgroundColor:colorPrimaryLight,justifyContent:'center'}}>
                          <Text style={{fontSize:15, fontFamily:fontFamily.bold, marginStart:10 , marginEnd:15,color:colorWhite,textAlignVertical:'center',maxHeight:'90%'}} numberOfLines={2}>Registration & Renewal asdds sdsdsdd sdsd sdssds </Text>    
                        </View> 
                        <View style={{height:40,width:40,backgroundColor:colorWhite,alignSelf:'center',marginStart:-20,borderRadius:20}}>
                          <Text style={{height:40,width:40,fontSize:fontSize.xs,textAlign:'center',textAlignVertical:'center',fontFamily:fontFamily.medium, color:colorPrimary}}>25% OFF</Text>    
                        </View> 
                      </View>
                  </View>                               
              );
          }}
          horizontal={false}
          keyExtractor={(item, index) => index}
      />
  </View>
    )
}
export default FragHome 

const styles = StyleSheet.create({ 
  carouselContainer: {
      height:160,
      marginTop:-20
  },
    carousel: {
          flex:1,
  },
  cardBackground:{
    marginBottom:10,height:150, marginEnd:15, marginStart:15,justifyContent:'center',
    ...Platform.select({
        android: {
            elevation: 10,
        },
        ios: {
            shadowColor: '#000',
            shadowOffset: {width: 0,height: 1,},
            shadowOpacity: 0.2,
            shadowRadius: 1.41,
        },
    })
}, 
})


