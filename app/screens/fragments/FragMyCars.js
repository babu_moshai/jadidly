import React,{Component,useState,useRef,useEffect} from 'react'
import {View,Text,FlatList,StyleSheet,Alert} from 'react-native'
import StatusBarComponent from '../../components/StatusBarComponent';
import { colorPrimary, colorWhite, colorBlack, whiteTranslucent, colorPrimaryLight, lightFont} from '../../styles/Color';
import { DEVICE_HEIGHT,DEVICE_WIDTH} from '../../styles/Dimens';
import FastImage from 'react-native-fast-image'
import { fontFamily, fontSize } from '../../styles/Fonts';
import {useNavigation, CommonActions} from '@react-navigation/native'
import { myStyles } from '../../styles/ExternalStyle';
import {TouchableOpacity} from 'react-native-gesture-handler'
import { isLoggedInSet, isLoggedInGet , getDataOfUser, logoutUser } from '../../database/UserData';



const FragMyCars = ({image,text,onClick,imageCenter}) => {
  const navigation = useNavigation()
  const [updated, setUpdated] = useState(false)
  const [selectedTab, setSelectedTab] = useState(1)
  const [userData,setUserData] = useState({})
  const data = ["1","2","3","1","2","3"]

  useEffect(()=>{
    setData()
  },[])
  const setData = async() => {
    setUserData(await getDataOfUser())
  }
  const showGuestDialog = async () => {
    Alert.alert("Login", "You need to register/login to get access.", 
      [
        {text: "Not Now", onPress: () => {}},
        {text: "Register/Login", onPress: () => { 
          logoutUser()
          handleScreen("WelcomeScreen")
        }},
      ]
      ,{ cancelable: true }
    )
}

  return (
    <View style={{flex:1}}>
    <StatusBarComponent textColor={true}/>
    <FastImage style={{width: DEVICE_WIDTH, height: 80,position:'absolute'}}
    source={require('../../assets/images/splash_bg.png')}/>
    <View style={{width:DEVICE_WIDTH/1.5,height:DEVICE_WIDTH/1.5,top:-80,start:-50,position:'absolute', backgroundColor:whiteTranslucent,borderRadius:DEVICE_WIDTH/1.5}}></View>
    <View style={{width:DEVICE_WIDTH/2.5,height:DEVICE_WIDTH/2.5,top:-60,start:150,position:'absolute', backgroundColor:whiteTranslucent,borderRadius:DEVICE_WIDTH/2.5}}></View>
    <View style={{flexDirection:'row',marginTop:40,marginStart:15,marginEnd:15, alignItems:'center'}}>
        <TouchableOpacity onPress={() => {navigation.openDrawer()}}>
          <FastImage style={{width: 25, height: 15}} source={require('../../assets/icons/menu.png')}/>
        </TouchableOpacity>
        <View style={{flex:1}}/>
        <TouchableOpacity onPress={() => {userData.id != "0"?navigation.navigate('Notifications'):showGuestDialog()}}>
          <FastImage style={{width: 18, height: 23}} source={require('../../assets/icons/Notification.png')}/>
        </TouchableOpacity>
    </View>
    {/* <View style={{flexDirection:'row',marginTop:20,marginStart:15,marginEnd:15, alignItems:'center'}}>
        <Text style={{fontSize:fontSize.l, fontFamily:fontFamily.semibold,color:colorBlack,textAlignVertical:'center',maxHeight:'90%'}} numberOfLines={2}>Offers</Text>    
        <View style={{flex:1}}/>
        <TouchableOpacity onPress={() => {}}>
        <Text style={{fontSize:fontSize.xs,textAlign:'center',textAlignVertical:'center',fontFamily:fontFamily.medium, color:lightFont}}>View All</Text>
        </TouchableOpacity>
    </View> */}
    
    
    
    <FlatList
        extraData={updated}
        data={data}
        showsVerticalScrollIndicator={false}
        style={{flexGrow:1,marginTop:25,height:DEVICE_HEIGHT}}
        renderItem={({ item: rowData }) => {
            return(
                <View style={[myStyles.cardBox,{marginStart:15,marginEnd:15,marginBottom:15,marginTop:10}]}>
                    <TouchableOpacity style={{}} onPress={() => {}}>
                        <View style={{flexDirection:'row',flex:1}}>
                            <FastImage style={{width: 70, height: 70,borderRadius:10,}} source={require('../../assets/images/car_image.png')}/>
                            <View style={{flex:1,marginStart:10}}>
                                <Text style={{fontFamily:fontFamily.semibold, color:colorBlack, fontSize:fontSize.m}}>Car_Name</Text>
                                <Text style={{fontFamily:fontFamily.semibold, color:lightFont, fontSize:fontSize.xs}}>Model_Name</Text>
                                <Text style={{fontFamily:fontFamily.semibold, color:lightFont, fontSize:fontSize.xs}}>Model_Year</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <View style={{flexDirection:'row-reverse',flex:1}}>
                        <TouchableOpacity style={{}} onPress={() => {}}>
                            <FastImage style={{width: 18, height: 18}} resizeMode={"stretch"} source={require('../../assets/icons/delete.png')}/>
                        </TouchableOpacity>
                        <TouchableOpacity style={{marginStart:10,marginEnd:10}} onPress={() => {}}>
                            <FastImage style={{width: 18, height: 18}} resizeMode={"stretch"} source={require('../../assets/icons/edit.png')}/>
                        </TouchableOpacity>
                    </View>
                </View>                              
            );
        }}
        horizontal={false}
        keyExtractor={(item, index) => index}
    />
    <View style={{position:'absolute',bottom:30,end:20}}>
    <TouchableOpacity style={[myStyles.elevation,{justifyContent:'center',borderRadius:30,width:60,height:60,backgroundColor:colorWhite}]} 
    onPress={() => {navigation.navigate('AddCar')}}>
        <FastImage style={{width: 25, height: 25,alignSelf:'center'}} resizeMode={"stretch"} source={require('../../assets/icons/plus_icon.png')}/>
    </TouchableOpacity>
    </View>
</View>
)
}
export default FragMyCars 

const styles = StyleSheet.create({ 
  cardBackground:{
    marginBottom:10,height:150, marginEnd:15, marginStart:15,justifyContent:'center',
    ...Platform.select({
        android: {
            elevation: 10,
        },
        ios: {
            shadowColor: '#000',
            shadowOffset: {width: 0,height: 1,},
            shadowOpacity: 0.2,
            shadowRadius: 1.41,
        },
    })
}, 
})