import React,{useEffect,useState,useRef} from 'react'
import {View,TouchableOpacity,Text,TextInput,StyleSheet,Alert} from 'react-native'
import StatusBarComponent from '../../components/StatusBarComponent';
import { colorPrimary, colorWhite, colorBlack, whiteTranslucent, colorPrimaryLight, lightFont} from '../../styles/Color';
import { DEVICE_HEIGHT,DEVICE_WIDTH} from '../../styles/Dimens';
import FastImage from 'react-native-fast-image'
import { fontFamily, fontSize } from '../../styles/Fonts';
import {useNavigation, CommonActions} from '@react-navigation/native'
import {myStyles} from '../../styles/ExternalStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import { getDataOfUser,logoutUser,isLoggedInSet  } from '../../database/UserData'
import { showSnackBarOne } from '../../helpers/showSnackBarOne'
import {callApiGetWithData,callApiNoData} from '../../api/NetworkRequest'
import { DEVICE_TYPE } from '../../utils/Constant'
import Spinner from '../../components/Spinner'
import CountryPicker from 'react-native-country-picker-modal';

const FragProfile = ({text,onClick,imageCenter}) => {
  const navigation = useNavigation()
  const [userData,setUserData] = useState({})

  const [editable, setEditable] = useState(false)
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [mobile, setMobile] = useState('')
  const [ccStr, setccStr] = useState('965')
  const [image, setImage] = useState('')
  
  const emailRef = useRef(null);
  const mobileRef = useRef(null);
  const ccpRef = useRef(null);
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setData()
    });
    return unsubscribe;
  }, [navigation]);
  useEffect(()=>{
    setData()
  },[userData])
  const setData = async() => {
    setUserData(await getDataOfUser())
    setName(userData.name)
    setEmail(userData.email)
    setccStr(userData.country_code)
    setMobile(userData.mobile_number)
    setImage(userData.image?userData.image:'')
  }

  const showGuestDialog = async () => {
    Alert.alert("Login", "You need to register/login to get access.", 
      [
        {text: "Not Now", onPress: () => {}},
        {text: "Register/Login", onPress: () => { 
          logoutUser()
          handleScreen("WelcomeScreen")
        }},
      ]
      ,{ cancelable: true }
    )
}

  return (
    <View style={{flex:1}}>
    <StatusBarComponent textColor={true}/>
    <FastImage style={{width: DEVICE_WIDTH, height: 250,position:'absolute'}}
    source={require('../../assets/images/splash_bg.png')}/>
      <View style={{width:DEVICE_WIDTH/1.5,height:DEVICE_WIDTH/1.5,top:-80,start:-50,position:'absolute', backgroundColor:whiteTranslucent,borderRadius:DEVICE_WIDTH/1.5}}></View>
      <View style={{width:DEVICE_WIDTH/2.5,height:DEVICE_WIDTH/2.5,top:-60,start:150,position:'absolute', backgroundColor:whiteTranslucent,borderRadius:DEVICE_WIDTH/2.5}}></View>
      <View style={{flexDirection:'row',marginTop:40,marginStart:15,marginEnd:15, alignItems:'center'}}>
          <TouchableOpacity onPress={() => {navigation.openDrawer()}}>
            <FastImage style={{width: 25, height: 15}} source={require('../../assets/icons/menu.png')}/>
          </TouchableOpacity>
          <View style={{flex:1}}/>
          <TouchableOpacity onPress={() => {userData.id != "0"?navigation.navigate('Notifications'):showGuestDialog()}}>
            <FastImage style={{width: 18, height: 23}} source={require('../../assets/icons/Notification.png')}/>
          </TouchableOpacity>
      </View>
      <FastImage style={{width: 150, height:150,alignSelf:'center',marginTop:20,marginBottom:20,borderRadius:75}}  
      source={image==="undefined" || image =='' ? require('../../assets/images/placeholder.png'): {uri:image} }/>
      <KeyboardAwareScrollView bounce={false} style={{flex:1,height:DEVICE_HEIGHT,backgroundColor:'white'}}  keyboardShouldPersistTaps='handled'>
          <View style={{flex:1,padding:15,backgroundColor:whiteTranslucent}}>
            <View style={myStyles.inputBox}>
              <TextInput
                autoCorrect={false}
                style={myStyles.inputStyle}
                placeholderStyle={myStyles.inputPlaceHolderStyle}
                placeholder={'Name'}
                onChangeText={name => setName(name)}
                onSubmitEditing={()=>emailRef.current.focus()}
                blurOnSubmit={false}
                returnKeyType='next'
                keyboardType={'default'} 
                value={name}
                editable={editable}
              />
            </View>

            <View style={myStyles.inputBox}>
              <TextInput
                autoCorrect={false}
                style={myStyles.inputStyle}
                placeholderStyle={myStyles.inputPlaceHolderStyle}
                ref={emailRef}
                placeholder={'Email'}
                onChangeText={email => setEmail(email)}
                onSubmitEditing={()=>mobileRef.current.focus()}
                blurOnSubmit={false}
                returnKeyType='next'
                keyboardType={'email-address'} 
                value={email}
                editable={false}
              />
            </View>

            <View style={myStyles.inputBox}>
              <>
              <Text style={[myStyles.ccpStyleText,{}]} maxLength={5} ref={ccpRef}>{"+"+ccStr} </Text>
                {/* <CountryPicker
                  containerButtonStyle={{width:50,height:40,start:10}}
                  withFilter
                  placeholder={''}
                  withCallingCode={true}
                  onSelect={data => {
                    setccStr(data.callingCode[0])
                  }}
                /> */}
              <TextInput
                autoCorrect={false}
                style={[myStyles.inputStyle,{marginStart:50}]}
                placeholderStyle={myStyles.inputPlaceHolderStyle}
                ref={mobileRef}
                placeholder={'Mobile Number'}
                onChangeText={mobile => setMobile(mobile)}
                returnKeyType='done'
                keyboardType={'phone-pad'} 
                value={mobile}
                editable={false}
              />
              </>
            </View>
            
            {/* <TouchableOpacity style={[myStyles.buttonBackground, {justifyContent:'center',marginTop:40, backgroundColor:colorPrimary}]} onPress={() => {setEditable(!editable)}}>
                <Text style={{fontFamily:fontFamily.medium, color:colorWhite, fontSize:fontSize.m, alignSelf:'center'}}>{editable?'Save Profile':'Edit Profile'}</Text>
            </TouchableOpacity> */}
        </View>
      </KeyboardAwareScrollView>      
    </View>
)
}
export default FragProfile 

const styles = StyleSheet.create({ 
  cardBackground:{
    marginBottom:10,height:150, marginEnd:15, marginStart:15,justifyContent:'center',
    ...Platform.select({
        android: {
            elevation: 10,
        },
        ios: {
            shadowColor: '#000',
            shadowOffset: {width: 0,height: 1,},
            shadowOpacity: 0.2,
            shadowRadius: 1.41,
        },
    })
}, 
})