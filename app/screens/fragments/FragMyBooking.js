import React,{Component,useState,useEffect} from 'react'
import {View,TouchableOpacity,Text,FlatList,StyleSheet,Alert} from 'react-native'
import StatusBarComponent from '../../components/StatusBarComponent';
import { colorPrimary, colorWhite, colorBlack, whiteTranslucent, colorPrimaryLight, lightFont, cardBorderColor} from '../../styles/Color';
import { DEVICE_HEIGHT,DEVICE_WIDTH} from '../../styles/Dimens';
import FastImage from 'react-native-fast-image'
import { fontFamily, fontSize } from '../../styles/Fonts';
import {useNavigation, CommonActions} from '@react-navigation/native'
import { myStyles } from '../../styles/ExternalStyle';
import { isLoggedInSet, isLoggedInGet , getDataOfUser, logoutUser } from '../../database/UserData';



const FragMyBooking = ({image,text,onClick,imageCenter}) => {
    const navigation = useNavigation()
    const [updated, setUpdated] = useState(false)
    const [selectedTab, setSelectedTab] = useState(1)
    const [userData,setUserData] = useState({})
    const data = ["1","2","3","1","2","3"]

    useEffect(()=>{
        setData()
    },[])
    const setData = async() => {
        setUserData(await getDataOfUser())
    }

    const showGuestDialog = async () => {
        Alert.alert("Login", "You need to register/login to get access.", 
          [
            {text: "Not Now", onPress: () => {}},
            {text: "Register/Login", onPress: () => { 
              logoutUser()
              handleScreen("WelcomeScreen")
            }},
          ]
          ,{ cancelable: true }
        )
    }

    return (
      <View style={{flex:1}}>
      <StatusBarComponent textColor={true}/>
      <FastImage style={{width: DEVICE_WIDTH, height: 80,position:'absolute'}}
      source={require('../../assets/images/splash_bg.png')}/>
      <View style={{width:DEVICE_WIDTH/1.5,height:DEVICE_WIDTH/1.5,top:-80,start:-50,position:'absolute', backgroundColor:whiteTranslucent,borderRadius:DEVICE_WIDTH/1.5}}></View>
      <View style={{width:DEVICE_WIDTH/2.5,height:DEVICE_WIDTH/2.5,top:-60,start:150,position:'absolute', backgroundColor:whiteTranslucent,borderRadius:DEVICE_WIDTH/2.5}}></View>
      <View style={{flexDirection:'row',marginTop:40,marginStart:15,marginEnd:15, alignItems:'center'}}>
          <TouchableOpacity onPress={() => {navigation.openDrawer()}}>
            <FastImage style={{width: 25, height: 15}} source={require('../../assets/icons/menu.png')}/>
          </TouchableOpacity>
          <View style={{flex:1}}/>
          <TouchableOpacity onPress={() => {userData.id != "0"?navigation.navigate('Notifications'):showGuestDialog()}}>
            <FastImage style={{width: 18, height: 23}} source={require('../../assets/icons/Notification.png')}/>
          </TouchableOpacity>
      </View>
      {/* <View style={{flexDirection:'row',marginTop:20,marginStart:15,marginEnd:15, alignItems:'center'}}>
          <Text style={{fontSize:fontSize.l, fontFamily:fontFamily.semibold,color:colorBlack,textAlignVertical:'center',maxHeight:'90%'}} numberOfLines={2}>Offers</Text>    
          <View style={{flex:1}}/>
          <TouchableOpacity onPress={() => {}}>
          <Text style={{fontSize:fontSize.xs,textAlign:'center',textAlignVertical:'center',fontFamily:fontFamily.medium, color:lightFont}}>View All</Text>
          </TouchableOpacity>
      </View> */}
      
      <FlatList
          extraData={updated}
          data={data}
          showsVerticalScrollIndicator={false}
          style={{flexGrow:1,marginTop:25,height:DEVICE_HEIGHT}}
            renderItem={({ item: rowData }) => {
                return(
                    <View style={[myStyles.cardBox,{marginStart:15,marginEnd:15,marginBottom:15,marginTop:10}]}>
                        <TouchableOpacity style={{}} onPress={() => {}}>
                            <View style={{flexDirection:'row',flex:1}}>
                                <View style={{backgroundColor:colorPrimary,padding:10,width: 70, height: 70,borderRadius:10,alignItems:'center',justifyContent:'center'}}>
                                    <FastImage style={{height:40,width:40}} tintColor={colorWhite} source={require('../../assets/icons/booking_list_icon.png')}/>
                                </View>
                                <View style={{flex:1,marginStart:10}}>
                                    <Text style={{fontFamily:fontFamily.semibold, color:colorBlack, fontSize:fontSize.m}}>Booking</Text>
                                    <Text style={{fontFamily:fontFamily.semibold, color:lightFont, fontSize:fontSize.xs}}>Description</Text>
                                    <Text style={{fontFamily:fontFamily.semibold, color:lightFont, fontSize:fontSize.xss}}>24 January, 2020 1:55 PM</Text>
                                    <Text style={{fontFamily:fontFamily.semibold, color:lightFont, fontSize:fontSize.m}}>*123456</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <View style={{marginStart:-10,marginEnd:-10,height:1,backgroundColor:cardBorderColor}}/>
                        <View style={{flexDirection:'row',flex:1,padding:10,paddingBottom:0,alignItems:'center'}}>
                            <View style={{height:10,width:10,borderRadius:5,backgroundColor:'green',marginBottom:4}}/>
                            <Text style={{fontFamily:fontFamily.semibold, color:colorBlack, fontSize:fontSize.m,marginStart:10,flex:1}}>COMPLETED</Text>
                            <Text style={{fontFamily:fontFamily.semibold, color:colorPrimary, fontSize:fontSize.m,marginEnd:-10}}>KWD 100</Text>
                        </View>
                    </View>                              
                );
            }}
          horizontal={false}
          keyExtractor={(item, index) => index}
      />
  </View>
)
}
export default FragMyBooking 
const styles = StyleSheet.create({ 
  cardBackground:{
    marginBottom:10,height:150, marginEnd:15, marginStart:15,justifyContent:'center',
    ...Platform.select({
        android: {
            elevation: 10,
        },
        ios: {
            shadowColor: '#000',
            shadowOffset: {width: 0,height: 1,},
            shadowOpacity: 0.2,
            shadowRadius: 1.41,
        },
    })
}, 
})

