import React, {useEffect, useState, useRef} from 'react'
import {View,TouchableOpacity,Text,TextInput,Keyboard,Alert} from 'react-native'
import StatusBarComponent from '../../components/StatusBarComponent';
import { colorPrimary, colorWhite, colorBlack} from '../../styles/Color';
import { DEVICE_HEIGHT,DEVICE_WIDTH} from '../../styles/Dimens';
import FastImage from 'react-native-fast-image'
import {useNavigation ,CommonActions} from '@react-navigation/native'
import { fontFamily, fontSize } from '../../styles/Fonts';
import {myStyles} from '../../styles/ExternalStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import CountryPicker from 'react-native-country-picker-modal';
import ImageAndDocPickerModel from '../../components/modal/ImageAndDocPickerModel'
import { showSnackBarOne } from '../../helpers/showSnackBarOne'
import {callApi} from '../../api/NetworkRequest'
import { DEVICE_TYPE } from '../../utils/Constant'
import Spinner from '../../components/Spinner'
import { getLanguage, saveDataOfUser } from '../../database/UserData'

function SignIn(){
    const navigation = useNavigation()
    const [loader, setLoader] = useState(false)

    const [loginType, setLoginType] = useState('1')
    const [mobile, setMobile] = useState('')
    const [ccStr, setccStr] = useState('965')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [passwordVisibility, setPasswordVisibility] = useState(true)
    const passwordRef = useRef(null);
    const ccpRef = useRef(null);
    
    // useEffect(() => {
    //   const unsubscribe = navigation.addListener('focus', () => {
    //     setData()
    //   });
    //   return unsubscribe;
    // }, [navigation]);
    // useEffect(()=>{
    //   setData()
    // },[])
    // const setData = async() => {
    //   // setLoginType('1')
    //   // setMobile('')
    //   // setccStr('964')
    //   // setEmail('')
    //   // setPassword('')
    // }
    const validate = async () => {
      Keyboard.dismiss()
      if(loginType=="1"){
        if (mobile == "") {
          showSnackBarOne("Enter mobile number")
          return
        } else if (mobile.trim().length < 8) {
          showSnackBarOne("Mobile number contain 8 digits at least")
          return
        } else if (mobile.trim().startsWith('0',0)) {
          showSnackBarOne("Enter valid mobile number. It cannot start with zero")
          return
        }else if (password.trim().length == 0) {
          showSnackBarOne("Enter password")
          return
        } else if (password.trim().length < 8) {
          showSnackBarOne("Password contain 8 characters at least")
          return
        } 
      }else{
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (email.trim().length == 0) {
          showSnackBarOne("Enter email address")
          return
        } else if (reg.test(email.trim()) === false) {
          showSnackBarOne("Enter valid email address")
          return
        } else if (password.trim().length == 0) {
          showSnackBarOne("Enter Password")
          return
        } else if (password.trim().length < 8) {
          showSnackBarOne("Password contain 8 characters at least")
          return
        } 
      }
      loginApi()
    }

    const loginApi = async () => {
      const data = new FormData();
      if(loginType=="1"){
        data.append('type', "2")
        data.append('country_code', ccStr)
        data.append('mobile_number', mobile)
      }else if(loginType=="2"){
        data.append('email', email)
        data.append('type', "1")
      }
      data.append('password', password)
      data.append('device_token',"12345")
      data.append('device_type', DEVICE_TYPE)
        
      console.log(data)
      setLoader(true)
      const apiData = await callApi('login',data)
      console.log(apiData)
      setLoader(false)
      if(apiData.status){
        

        if(apiData.status_code=="201"){
          showSnackBarOne(apiData.message)
          let setUserData = {
            id : apiData.data.user_id,
            email : apiData.data.email,
            country_code : ccStr,
            mobile_number : mobile,
          }
          console.log(setUserData)
          saveDataOfUser(setUserData)
          navigation.navigate('OtpVerification',{from:'SignIn',loginType:loginType}) 
        }else{
          showSnackBarOne(apiData.message)
          let setUserData = {
            id : apiData.data.id,
            name : apiData.data.name,
            email : apiData.data.email,
            country_code : apiData.data.country_code,
            mobile_number : apiData.data.mobile_number,
            token:apiData.data.token,
            image:apiData.data.image
          }
          console.log(setUserData)
          saveDataOfUser(setUserData)
          if(apiData.data.is_otp_verified=="yes"){
              if(apiData.data.status=="active"){
                handleScreen('HomeScreen')
              }else{
                showSnackBarOne("You are inactive.")
              }
          }else{
            navigation.navigate('OtpVerification',{from:'SignIn',loginType:loginType}) 
          }
        }
      }else{
        showSnackBarOne(apiData.error)
      }
    }

  
    const handleScreen = async (where) => {
      navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [
            { name: where }
          ],
        })
      );
    }

    return(
         <View style={{flex:1,backgroundColor:colorPrimary}}>
             <StatusBarComponent textColor={false}/>
             <Spinner visible={loader}  />
                <FastImage style={{width: DEVICE_WIDTH, height: DEVICE_HEIGHT+30,position:'absolute',backgroundColor:'white'}}/>
                <View style={{flexDirection:'row',marginTop:40,marginStart:15, alignItems:'center'}}>
                    <TouchableOpacity onPress={() => {navigation.pop()}}>
                      <FastImage style={{width: 11, height: 18}} resizeMode={"center"} source={require('../../assets/icons/back_icon.png')}/>
                    </TouchableOpacity>
                    <Text style={{fontFamily:fontFamily.medium, color:colorBlack,marginStart:15,fontSize:fontSize.xl}}>Sign In</Text>
                </View>
                <Text style={{fontFamily:fontFamily.regular,color:colorBlack,marginTop:5,marginStart:15,fontSize:fontSize.xs}}>Please sign in to account to continue.</Text>
                <KeyboardAwareScrollView bounce={false} style={{flex:1,height:DEVICE_HEIGHT}}  keyboardShouldPersistTaps='handled'>
                  <View style={{margin:15,flex:1}}>

                  <View style={{flexDirection:'row'}}>
                  <TouchableOpacity style={loginType=="1"?[myStyles.selectedButton]:[myStyles.unselectedButton]} onPress={() => {setLoginType('1')}}>
                      <Text style={{fontFamily:fontFamily.regular, color:loginType=="1"?colorWhite:colorBlack, fontSize:fontSize.s, alignSelf:'center'}}>Mobile Number</Text>
                  </TouchableOpacity>
                  <View style={{width:15}}/>
                   <TouchableOpacity style={loginType=="2"?[myStyles.selectedButton]:[myStyles.unselectedButton]} onPress={() => {setLoginType('2')}}>
                      <Text style={{fontFamily:fontFamily.regular, color:loginType=="2"?colorWhite:colorBlack, fontSize:fontSize.s, alignSelf:'center'}}>Email Address</Text>
                  </TouchableOpacity>
                  </View>
                  <View style={myStyles.inputBox}>
                    {loginType=="1"?
                      <><Text style={[myStyles.ccpStyleText,{}]} maxLength={5} ref={ccpRef}>{"+"+ccStr} </Text>
                      <CountryPicker
                      containerButtonStyle={{width:50,height:40,start:10}}
                        withFilter
                        placeholder={''}
                        withCallingCode={true}
                        onSelect={data => {
                          setccStr(data.callingCode[0]===undefined?'964':data.callingCode[0])
                        }}
                      />
                      <TouchableOpacity>
                        <FastImage style={{width: 10, height: 10}} tintColor={colorBlack} resizeMode={"contain"} source={require('../../assets/icons/drop_down.png')}/>
                      </TouchableOpacity>
                      <TextInput
                        autoCorrect={false}
                        style={[myStyles.inputStyle,{}]}
                        placeholderStyle={myStyles.inputPlaceHolderStyle}
                        placeholder={'Mobile Number'}
                        onChangeText={mobile => setMobile(mobile)}
                        onSubmitEditing={()=>passwordRef.current.focus()}
                        blurOnSubmit={false}
                        maxLength={13}
                        returnKeyType='next'
                        keyboardType='number-pad'
                        value={mobile}
                      /></>:
                      <TextInput
                        autoCorrect={false}
                        style={myStyles.inputStyle}
                        placeholderStyle={myStyles.inputPlaceHolderStyle}
                        placeholder={'Email'}
                        onChangeText={email => setEmail(email)}
                        onSubmitEditing={()=>passwordRef.current.focus()}
                        blurOnSubmit={false}
                        returnKeyType='next'
                        value={email}/>
                    }
                  </View>
                  

                  <View style={myStyles.inputBox}>
                    <TextInput
                      autoCorrect={false}
                      style={myStyles.inputStyle}
                      placeholderStyle={myStyles.inputPlaceHolderStyle}
                      ref={passwordRef}
                      placeholder={'Password'}
                      onChangeText={password => setPassword(password)}
                      returnKeyType='done'
                      secureTextEntry={passwordVisibility}
                      value={password}
                    />
                    <TouchableOpacity style={{position:'absolute',end:20}} onPress={() => {setPasswordVisibility(!passwordVisibility)}}>
                      <FastImage style={{width: 20, height: 20}} resizeMode={"stretch"} source={passwordVisibility?require('../../assets/icons/show.png'):require('../../assets/icons/hide.png')}/>
                    </TouchableOpacity>
                  </View>

                  <TouchableOpacity style={{flexWrap: "wrap",alignSelf:'flex-end',margin:10}} onPress={() => {navigation.push('ForgotPassword',{loginType:loginType})}}>
                      <Text style={{fontFamily:fontFamily.light, color:colorBlack, fontSize:fontSize.xs}}>Forgot Password</Text>
                  </TouchableOpacity>
                  
                  <TouchableOpacity style={[myStyles.buttonBackground, {justifyContent:'center',marginTop:30,backgroundColor:colorPrimary}]} onPress={() => {validate()}}>
                      <Text style={{fontFamily:fontFamily.medium, color:colorWhite, fontSize:fontSize.m, alignSelf:'center'}}>Sign In</Text>
                  </TouchableOpacity>
                  
                  <View style={{flex:1}}/>
                  
                  <View style={{flexDirection:'row',marginTop:20}}>
                      <View style={{height:1,backgroundColor:colorBlack,flex:1,alignSelf:'center'}}/>
                      <Text style={{fontFamily:fontFamily.light, color:colorBlack,alignSelf:'center',fontSize:fontSize.m,marginStart:10,marginEnd:10}}>OR</Text>
                      <View style={{height:1,backgroundColor:colorBlack,flex:1,alignSelf:'center'}}/>
                  </View>

                  <View style={{flex:1}}/>

                  <TouchableOpacity style={{flexWrap: "wrap",alignSelf:'center',margin:10}} onPress={() => {navigation.replace('SignUp')}}>
                      <Text style={{fontFamily:fontFamily.light, color:colorBlack, fontSize:fontSize.xs}}>Don't have an account? <Text style={{fontFamily:fontFamily.semibold, color:colorPrimary, fontSize:fontSize.xs}}>Sign Up</Text></Text>
                  </TouchableOpacity>

                  <Text style={{fontFamily:fontFamily.light,margin:10, color:colorBlack,alignSelf:'center', fontSize:fontSize.xs}}>Log in with</Text>
              
                  <View style={{flexDirection:'row',alignSelf:'center'}}>
                      <TouchableOpacity style={{width: 60, height: 60}} onPress={() => {}}>
                          <FastImage style={{width: 60, height: 60}} source={require('../../assets/icons/google_icon.png')}/>
                      </TouchableOpacity>
                      
                      <TouchableOpacity style={{width: 60, height: 60}} onPress={() => {}}>
                          <FastImage
                          style={{width: 60, height: 60}}
                          source={require('../../assets/icons/facebook_icon.png')}/>
                      </TouchableOpacity>

                      <TouchableOpacity style={{width: 60, height: 60}} onPress={() => {}}>
                          <FastImage
                          style={{width: 60, height: 60}}
                          source={require('../../assets/icons/twiter_icon.png')}/>
                      </TouchableOpacity>
                  </View>
                </View>
                </KeyboardAwareScrollView>
         </View>
    );
}

export default SignIn
