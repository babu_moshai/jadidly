import React, {Component, useState, useRef} from 'react'
import {View,TouchableOpacity,Text,TextInput,Keyboard} from 'react-native'
import StatusBarComponent from '../../components/StatusBarComponent';
import { colorPrimary, colorWhite, colorBlack} from '../../styles/Color';
import { DEVICE_HEIGHT,DEVICE_WIDTH} from '../../styles/Dimens';
import FastImage from 'react-native-fast-image'
import {useNavigation} from '@react-navigation/native'
import { fontFamily, fontSize } from '../../styles/Fonts';
import {myStyles} from '../../styles/ExternalStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import { showSnackBarOne } from '../../helpers/showSnackBarOne'
import {callApi} from '../../api/NetworkRequest'
import { DEVICE_TYPE } from '../../utils/Constant'
import Spinner from '../../components/Spinner'
import { getDataOfUser,getLanguage, saveDataOfUser } from '../../database/UserData'

function ResetPassword(){
    const navigation = useNavigation()
    const [loader, setLoader] = useState(false)

    const [newPassword, setNewPassword] = useState('')
    const [confirmNewPassword, setConfirmNewPassword] = useState('')
    const newPasswordRef = useRef(null);
    const confirmNewPasswordRef = useRef(null);
    
    const validate = async () => {
      Keyboard.dismiss()
      if (newPassword.trim().length == 0) {
        showSnackBarOne("Enter new password")
        return
      } else if (newPassword.trim().length < 6) {
        showSnackBarOne("New password contain 6 characters at least")
        return
      } else if (confirmNewPassword.trim().length == 0) {
        showSnackBarOne("Enter confirm new password")
        return
      } else if (confirmNewPassword.trim().length < 6) {
        showSnackBarOne("Confirm new password contain 6 characters at least")
        return
      } else if (newPassword != confirmNewPassword) {
        showSnackBarOne("Confirm new password not matched to the new password")
        return
      } 
      resetPassword()
    }

    const resetPassword = async () => {
      const data = new FormData();
      const userDataLocal = await getDataOfUser()
      data.append('user_id', userDataLocal.id)
      data.append('new_password', newPassword)
      data.append('confirm_password', confirmNewPassword)
      
      console.log(data)
      setLoader(true)
      const apiData = await callApi('updatePassword',data)
      console.log("API RESPONSE=> ",apiData)
      setLoader(false)
    if(apiData.status){
      showSnackBarOne(apiData.message)
      navigation.pop()
    }else{
      showSnackBarOne(apiData.error)
    }
  }
    return(
         <View style={{flex:1,backgroundColor:colorPrimary}}>
             <StatusBarComponent textColor={false}/>
             <Spinner visible={loader}/>
                <FastImage style={{width: DEVICE_WIDTH, height: DEVICE_HEIGHT+30,position:'absolute',backgroundColor:'white'}}/>
                <View style={{flexDirection:'row',marginTop:40,marginStart:15, alignItems:'center'}}>
                    <TouchableOpacity onPress={() => {navigation.pop()}}>
                      <FastImage style={{width: 11, height: 18}} resizeMode={"center"} source={require('../../assets/icons/back_icon.png')}/>
                    </TouchableOpacity>
                    <Text style={{fontFamily:fontFamily.medium, color:colorBlack,marginStart:15,fontSize:fontSize.xl}}>Reset Password</Text>
                </View>
                
                <KeyboardAwareScrollView bounce={false} style={{flex:1,height:DEVICE_HEIGHT}}  keyboardShouldPersistTaps='handled'>
                  <View style={{margin:15,flex:1}}>

                  <View style={{flexWrap:"wrap", alignSelf:'center', marginTop:40, marginBottom:40}}>
                      <Text style={{fontFamily:fontFamily.light, color:colorBlack,textAlign:'center', fontSize:fontSize.m}}>Enter your new password for continue.</Text>
                  </View>

                  <View style={myStyles.inputBox}>
                    <TextInput
                      autoCorrect={false}
                      style={myStyles.inputStyle}
                      placeholderStyle={myStyles.inputPlaceHolderStyle}
                      ref={newPasswordRef}
                      placeholder={'New Password'}
                      onChangeText={newPassword => setNewPassword(newPassword)}
                      onSubmitEditing={()=>confirmNewPasswordRef.current.focus()}
                      blurOnSubmit={false}
                      returnKeyType='next'
                      keyboardType={'default'} 
                      value={newPassword}
                      secureTextEntry={true}
                    />
                  </View>

                  <View style={myStyles.inputBox}>
                    <TextInput
                      autoCorrect={false}
                      style={myStyles.inputStyle}
                      placeholderStyle={myStyles.inputPlaceHolderStyle}
                      ref={confirmNewPasswordRef}
                      placeholder={'Confirm New Password'}
                      onChangeText={confirmNewPassword => setConfirmNewPassword(confirmNewPassword)}
                      returnKeyType='done'
                      keyboardType={'default'}
                      value={confirmNewPassword}
                      secureTextEntry={true}
                    />
                  </View>
                  
                  <TouchableOpacity style={[myStyles.buttonBackground, {justifyContent:'center',marginTop:30,backgroundColor:colorPrimary}]} onPress={() => {validate()}}>
                      <Text style={{fontFamily:fontFamily.medium, color:colorWhite, fontSize:fontSize.m, alignSelf:'center'}}>Submit</Text>
                  </TouchableOpacity>
                  
                </View>
                </KeyboardAwareScrollView>
         </View>
    );
}

export default ResetPassword
