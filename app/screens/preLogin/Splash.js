import React, {Component, useState, useEffect} from 'react'
import {View,TouchableOpacity,Text} from 'react-native'
import StatusBarComponent from '../../components/StatusBarComponent';
import { colorPrimary, colorWhite } from '../../styles/Color';
import { DEVICE_HEIGHT,DEVICE_WIDTH } from '../../styles/Dimens';
import {useNavigation,CommonActions} from '@react-navigation/native'
import FastImage from 'react-native-fast-image'
import { fontFamily, fontSize } from '../../styles/Fonts';
import { myStyles } from '../../styles/ExternalStyle';
import { strings } from '../../multilingual/strings';
import { getLanguage,getFirstTime,isLoggedInGet,setLanguage,setFirstTime,isLoggedInSet,getFingerPrintEnable } from '../../database/UserData';
function Splash(){
    const navigation = useNavigation()
    const [isFirst, setIsFirst] = useState()
    const [isLoggedIn, setIsLoggedIn] = useState('')
    const [lang, setLang] = useState('')

    useEffect(() => {
        checkPrefs()
    },[])

    const checkPrefs = async () => {
      // if(await getFingerPrintEnable() == "1"){
      //   navigation.push('BiomatricPopup')
      // }else{
        var f = await getFirstTime()
        setIsFirst(f)
        if(await getFirstTime() == "1"){
            console.log("FIRST TIME => "+isFirst)
        }else{
            let myInterval = setInterval( async ()=>{
                strings.setLanguage(await getLanguage())
                if(await isLoggedInGet() == "1"){
                    handleToHome()
                }else{
                    navigation.replace('WelcomeScreen')
                }
                clearInterval(myInterval);
            },3000)
        }
     // }
    }
    const nextScreen = async lang => {
      await setLanguage(lang)
      await setFirstTime("0")
      navigation.replace('WelcomeScreen')
      console.log("nextScreen => "+ await getFirstTime())
      console.log("goto Home2 => "+await isLoggedInGet())
    }
    const handleToHome = async () => {
      console.log("goto Home1 => "+ await getFirstTime())
      console.log("goto Home2 => "+await isLoggedInGet())
      navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [
            { name: 'HomeScreen' }
          ],
        })
      );
    }
    return(
         <View style={{flex:1,backgroundColor:colorPrimary}}>
             <StatusBarComponent textColor={'light-content'}/>
             <FastImage
                style={{width: DEVICE_WIDTH, height: DEVICE_HEIGHT+30,position:'absolute'}}
                source={require('../../assets/images/splash_bg.png')}/>
             <View style={{flex:1,justifyContent:'center'}}>
                <FastImage
                style={{width: 222, height: 200,alignSelf:'center'}}
                source={require('../../assets/icons/splash_image.png')}/>
                <Text style={{fontFamily:fontFamily.semibold, letterSpacing:2, color:colorWhite,marginTop:10, fontSize:fontSize.xxxl,alignSelf:'center'}}>Jadidly</Text>
             </View>
             {isFirst=="1"?
                <View style={{flexDirection:'row',margin:20}}>
                <TouchableOpacity style={[myStyles.buttonBackground, {justifyContent:'center',flex:1}]} onPress={() => {nextScreen('en')}}>
                    <Text style={{fontFamily:fontFamily.medium, color:colorPrimary, fontSize:fontSize.l, alignSelf:'center'}}>English</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[myStyles.buttonBackground, {marginStart:20,justifyContent:'center',flex:1}]} onPress={() => {nextScreen('en')}}>
                    <Text style={{fontFamily:fontFamily.medium, color:colorPrimary, fontSize:fontSize.l, alignSelf:'center'}}>عربى</Text>
                </TouchableOpacity>
             </View>:null}
             
         </View>
    );
}
export default Splash