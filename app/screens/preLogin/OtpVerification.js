import React, {useEffect, useState, useRef,} from 'react'
import {View,TouchableOpacity,Text,TextInput,Keyboard,Alert} from 'react-native'
import StatusBarComponent from '../../components/StatusBarComponent';
import { colorPrimary, colorWhite, colorBlack} from '../../styles/Color';
import { DEVICE_HEIGHT,DEVICE_WIDTH} from '../../styles/Dimens';
import FastImage from 'react-native-fast-image'
import {useNavigation,CommonActions} from '@react-navigation/native'
import { fontFamily, fontSize } from '../../styles/Fonts';
import {myStyles} from '../../styles/ExternalStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import { showSnackBarOne } from '../../helpers/showSnackBarOne'
import {callApi} from '../../api/NetworkRequest'
import { DEVICE_TYPE } from '../../utils/Constant'
import Spinner from '../../components/Spinner'
import { getDataOfUser, saveDataOfUser } from '../../database/UserData'

function OtpVerification ({route}) {
    const navigation = useNavigation()
    const [loader, setLoader] = useState(false)

    const [from, fromSet] = useState('')
    const [otp1, setotp1] = useState('')
    const [otp2, setotp2] = useState('')
    const [otp3, setotp3] = useState('')
    const [otp4, setotp4] = useState('')
    const otp1Ref = useRef(null);
    const otp2Ref = useRef(null);
    const otp3Ref = useRef(null);
    const otp4Ref = useRef(null);
    const [resendEnable, setResendEnable] = useState(false)
    const [timmer, setTimmer] = useState(30)

    useEffect(()=>{
    })
    useEffect(() => {
      if(timmer===0){
         console.log("TIME LEFT IS 0");
         setResendEnable(true)
         setTimmer(0)
      }
      if (!timmer) return;
      const intervalId = setInterval(() => {
        setTimmer(timmer - 1);
      }, 1000);
  
      return () => clearInterval(intervalId);
    }, [timmer]);

    const resendOtp= async()=>{
       resendOTP() 
    }

    const onChangeOtpOne = async (text) => {
      setotp1(text)
      let newText = '';
      let numbers = '0123456789';
      if(text.length == 0){
        setotp1('')
        otp1Ref.current.blur()
      }
      for (var i=0; i < 2; i++) {
        if(numbers.indexOf(text[i]) > -1 ) {
          setotp1(text[i])
          otp2Ref.current.focus()
        }
        else {
          // your call back function
          //alert("please enter numbers only");
        }
      }
    }

    const onChangeOtpTwo = async (text) => {
      setotp2(text)
      let newText = '';
      let numbers = '0123456789';
      if(text.length == 0){
        setotp2(newText)
        otp1Ref.current.focus()
      }
      for (var i=0; i < 2; i++) {
        if(numbers.indexOf(text[i]) > -1 ) {
          newText = newText + text[i];
          setotp2(newText)
          otp3Ref.current.focus()
        }
        else {
          // your call back function
          //alert("please enter numbers only");
        }
      }
    }

    const onChangeOtpThree = async (text) => {
      setotp3(text)
      let newText = '';
      let numbers = '0123456789';
      if(text.length == 0){
        setotp3(newText)
        otp2Ref.current.focus()
      }
      for (var i=0; i < 2; i++) {
        if(numbers.indexOf(text[i]) > -1 ) {
          newText = newText + text[i];
          setotp3(newText)
          otp4Ref.current.focus()
        }
        else {
          // your call back function
          //alert("please enter numbers only");
        }
      }
    }

    const onChangeOtpFour = async (text) => {
      setotp4(text)
      let newText = '';
      let numbers = '0123456789';
      if(text.length == 0){
        setotp4(newText)
        otp3Ref.current.focus()
      }
    
      for (var i=0; i < 2; i++) {
        if(numbers.indexOf(text[i]) > -1 ) {
          newText = newText + text[i];
          setotp4(newText)
          otp4Ref.current.blur()
        }
        else {
          // your call back function
          //alert("please enter numbers only");
        }
      }
    }

    const focusOne = async (otp,pos) => {
      if(otp==''){
        switch(pos){
          case 1:{
            setotp1(otp)
            Keyboard.dismiss()
            break;
          }
          case 2:{
            setotp2(otp)
            otp1Ref.current.focus()
            break;
          }
          case 3:{
            setotp3(otp)
            otp2Ref.current.focus()
            break;
          }
          case 4:{
            setotp4(otp)
            otp3Ref.current.focus()
            break;
          }
      }
      }else{
        switch(pos){
          case 1:{
            setotp1(otp)
            otp2Ref.current.focus()
            break;
          }
          case 2:{
            setotp2(otp)
            otp3Ref.current.focus()
            break;
          }
          case 3:{
            setotp3(otp)
            otp4Ref.current.focus()
            break;
          }
          case 4:{
            setotp4(otp)
            Keyboard.dismiss()
            break;
          }
      }
      } 
    }
    const validate = async () => {
      Keyboard.dismiss()
      var otp = otp1+otp2+otp3+otp4
      if (otp == ''){
        showSnackBarOne("Please enter OTP")
        return
      }else if (otp != '1111'){
        showSnackBarOne("Invalid OTP")
        return
      }
      validateOTP()
    }

    const validateOTP = async () => {
      let userDataLocal =  await getDataOfUser()
      const data = new FormData();
        data.append('user_id', userDataLocal.id)
        data.append('otp', otp1+otp2+otp3+otp4+otp3+otp4)
        if(route.params.from == "ForgotPassword"){
          data.append('type', "ForgotPassword")
        }else if(route.params.from == "SignUp"){
          data.append('type', "registration")
        }else if(route.params.from == "SignIn" && route.params.loginType=="1"){
          data.append('type', "registration")
        }else if(route.params.from == "SignIn" && route.params.loginType=="2"){
          data.append('type', "email")
        }
        
        console.log(data)
        setLoader(true)
        const apiData = await callApi('otp',data)
        console.log("API RESPONSE=> ",apiData)
        setLoader(false)
      if(apiData.status){
        //showSnackBarOne(apiData.message)
        showSnackBarOne("Verification Successful")
        //route.params.from == "ForgotPassword"?navigation.replace('ResetPassword'):navigation.pop()
        if(route.params.from == "ForgotPassword"){
          navigation.replace('ResetPassword')
        }else{
          let setUserData = {
            id : apiData.data.user_id,
            name : apiData.data.name,
            email : apiData.data.email,
            country_code : apiData.data.country_code,
            mobile_number : apiData.data.mobile_number,
            image : apiData.data.image,
            token:apiData.data.token,
          }
          console.log(setUserData)
          saveDataOfUser(setUserData)
          handleScreen('HomeScreen')
        }
      }else{
        showSnackBarOne(apiData.error)
      }
    }
    const handleScreen = async (where) => {
      navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [
            { name: where }
          ],
        })
      );
    }
    const resendOTP = async () => {
      let userData =  await getDataOfUser()
      const data = new FormData();
      if(route.params.loginType=="1"){
        data.append('type', "2")
        data.append('country_code', userData.country_code)
        data.append('mobile_number', userData.mobile_number)
      }else if(route.params.loginType=="2"){
        data.append('type', "1")
        data.append('email', userData.email)
      }
        
        console.log(data)
        setLoader(true)
        const apiData = await callApi('forgotPassword',data)
        console.log("API RESPONSE=> ",apiData)
        setLoader(false)
      if(apiData.status){
        showSnackBarOne(apiData.message)
        setResendEnable(false)
        setotp1('')
        setotp2('')
        setotp3('')
        setotp4('')
        setTimmer(30) 
      }else{
        showSnackBarOne(apiData.error)
      }
    }

  return(
    <View style={{flex:1,backgroundColor:colorPrimary}}>
        <StatusBarComponent textColor={false}/>
        <Spinner visible={loader}  />
           <FastImage style={{width: DEVICE_WIDTH, height: DEVICE_HEIGHT+30,position:'absolute',backgroundColor:'white'}}/>
           <View style={{flexDirection:'row',marginTop:40,marginStart:15, alignItems:'center'}}>
               <TouchableOpacity onPress={() => {navigation.pop()}}>
                 <FastImage style={{width: 11, height: 18}} resizeMode={"center"} source={require('../../assets/icons/back_icon.png')}/>
               </TouchableOpacity>
               <Text style={{fontFamily:fontFamily.medium, color:colorBlack,marginStart:15,fontSize:fontSize.xl}}>Verification</Text>
           </View>
           <KeyboardAwareScrollView bounce={false} style={{flex:1,height:DEVICE_HEIGHT}}  keyboardShouldPersistTaps='handled'>
             <View style={{margin:15,flex:1}}>

             <View style={{flexWrap:"wrap", alignSelf:'center', marginTop:40, marginBottom:40}}>
                <Text style={{fontFamily:fontFamily.light, color:colorBlack,textAlign:'center', fontSize:fontSize.m}}>We've sent a verification code to{"\n"}<Text style={{fontFamily:fontFamily.semibold, color:colorBlack, fontSize:fontSize.m}}>Your {route.params.loginType=="1"?"Mobile Number":"Email Address"}</Text></Text>
             </View>
             <View style={{flexDirection:'row',alignItems:'center',width:300,height:80,alignSelf:'center'}}>
                <View style={[myStyles.otpInputBox,{marginStart:20,marginEnd:10}]}>
                  <TextInput
                    autoCorrect={false}
                    autoCompleteType={'off'}
                    style={myStyles.otpInputStyle}
                    placeholderStyle={myStyles.otpInputPlaceHolderStyle}
                    placeholder={'_'}
                    ref={otp1Ref}
                    onChangeText={otp1 => focusOne(otp1,1)}
                    onSubmitEditing={()=>otp2Ref.current.focus()}
                    blurOnSubmit={false}
                    returnKeyType='next'
                    keyboardType={'number-pad'} 
                    value={otp1}
                    maxLength={1}
                  />
                </View>
                <View style={[myStyles.otpInputBox,{marginStart:10,marginEnd:10}]}>
                  <TextInput
                    autoCorrect={false}
                    autoCompleteType={'off'}
                    style={myStyles.otpInputStyle}
                    placeholderStyle={myStyles.otpInputPlaceHolderStyle}
                    placeholder={'_'}
                    ref={otp2Ref}
                    onChangeText={otp2 => focusOne(otp2,2)}
                    onSubmitEditing={()=>otp3Ref.current.focus()}
                    blurOnSubmit={false}
                    returnKeyType='next'
                    keyboardType={'number-pad'} 
                    value={otp2}
                    maxLength={1}
                  />
                </View>
                <View style={[myStyles.otpInputBox,{marginStart:10,marginEnd:10}]}>
                  <TextInput
                    autoCorrect={false}
                    autoCompleteType={'off'}
                    style={myStyles.otpInputStyle}
                    placeholderStyle={myStyles.otpInputPlaceHolderStyle}
                    placeholder={'_'}
                    ref={otp3Ref}
                    onChangeText={otp3 => focusOne(otp3,3)}
                    onSubmitEditing={()=>otp4Ref.current.focus()}
                    blurOnSubmit={false}
                    returnKeyType='next'
                    keyboardType={'number-pad'} 
                    value={otp3}
                    maxLength={1}
                  />
                </View>
                <View style={[myStyles.otpInputBox,{marginStart:10,marginEnd:20}]}>
                  <TextInput
                    autoCorrect={false}
                    autoCompleteType={'off'}
                    style={myStyles.otpInputStyle}
                    placeholderStyle={myStyles.otpInputPlaceHolderStyle}
                    placeholder={'_'}
                    ref={otp4Ref}
                    onChangeText={otp4 => focusOne(otp4,4)}
                    returnKeyType='done'
                    keyboardType={'number-pad'} 
                    value={otp4}
                    maxLength={1}
                  />
                </View>
             </View>
             
             <Text style={{fontFamily:fontFamily.medium,margin:40, color:'red',alignSelf:'center', fontSize:fontSize.xs}}>00:{timmer > 9? timmer: '0' + timmer}</Text>

             <TouchableOpacity style={[myStyles.buttonBackground, {justifyContent:'center',backgroundColor:colorPrimary}]} onPress={() => {validate()}}>
                 <Text style={{fontFamily:fontFamily.medium, color:colorWhite, fontSize:fontSize.m, alignSelf:'center'}}>Verify</Text>
             </TouchableOpacity>

             <TouchableOpacity style={{flexWrap: "wrap",alignSelf:'center',margin:20}} activeOpacity={resendEnable?0.4:1} onPress={() => {if (resendEnable) resendOtp()}}>
                 <Text style={{fontFamily:fontFamily.light, color:colorBlack, fontSize:fontSize.xs}}>If you don't receive a code! <Text style={{fontFamily:fontFamily.semibold, color:colorPrimary, fontSize:fontSize.xs}}>Resend</Text></Text>
             </TouchableOpacity>

             
           </View>
           </KeyboardAwareScrollView>
    </View>
);
}

export default OtpVerification
