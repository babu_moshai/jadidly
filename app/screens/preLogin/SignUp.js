import React, {Component, useState, useRef} from 'react'
import {View,TouchableOpacity,Text,TextInput,Keyboard} from 'react-native'
import StatusBarComponent from '../../components/StatusBarComponent';
import { colorPrimary, colorWhite, colorBlack} from '../../styles/Color';
import { DEVICE_HEIGHT,DEVICE_WIDTH} from '../../styles/Dimens';
import FastImage from 'react-native-fast-image'
import {useNavigation,CommonActions} from '@react-navigation/native'
import { fontFamily, fontSize } from '../../styles/Fonts';
import {myStyles} from '../../styles/ExternalStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import CountryPicker from 'react-native-country-picker-modal';
import ImageAndDocPickerModel from '../../components/modal/ImageAndDocPickerModel'
import { showSnackBarOne } from '../../helpers/showSnackBarOne'
import {callApi} from '../../api/NetworkRequest'
import { DEVICE_TYPE } from '../../utils/Constant'
import Spinner from '../../components/Spinner'
import { getLanguage, saveDataOfUser } from '../../database/UserData'

function SignUp(){
    const navigation = useNavigation()
    const [loader, setLoader] = useState(false)

    const [from, fromSet] = useState('')
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [mobile, setMobile] = useState('')
    const [ccStr, setccStr] = useState('965')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const emailRef = useRef(null);
    const ccpRef = useRef(null);
    const mobileRef = useRef(null);
    const passwordRef = useRef(null);
    const confirmPasswordRef = useRef(null);
    
    const handleGuest = async () => {
      let setUserData = {
          id : "0",
          name : "Guest",
          email : "XXX@XXXXX.XXX",
          country_code : "XXX",
          mobile_number : "XXXXXXXXX",
          image : "",
          token:"",
        }
        console.log(setUserData)
        saveDataOfUser(setUserData)
        handleScreen('HomeScreen')
    }
    const handleScreen = async (where) => {
        navigation.dispatch(
          CommonActions.reset({
            index: 1,
            routes: [
              { name: where }
            ],
          })
        );
    }

    const validate = async () => {
      Keyboard.dismiss()
      let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (name == "") {
        showSnackBarOne("Enter name")
        return
      } else if (name.trim().length < 3) {
        showSnackBarOne("Enter a valid name")
        return
      }/* else if (email.trim().length == 0) {
        showSnackBarOne("Enter email address")
        return
      }*/ else if (email.trim().length != 0 && reg.test(email.trim()) === false) {
        showSnackBarOne("Enter valid email address")
        return
      } else if (mobile == "") {
        showSnackBarOne("Enter mobile number")
        return
      } else if (mobile.trim().length < 8) {
        showSnackBarOne("Mobile number contain 8 digits at least")
        return
      } else if (mobile.trim().startsWith('0',0)) {
        showSnackBarOne("Enter valid mobile number. It cannot start with zero")
        return
      } else if (password.trim().length == 0) {
        showSnackBarOne("Enter password")
        return
      } else if (password.trim().length < 6) {
        showSnackBarOne("Password contain 6 characters at least")
        return
      }  else if (confirmPassword.trim().length == 0) {
        showSnackBarOne("Enter confirm password")
        return
      } else if (confirmPassword.trim().length < 6) {
        showSnackBarOne("Confirm password contain 6 characters at least")
        return
      } else if (password != confirmPassword) {
        showSnackBarOne("Confirm password not matched to the password")
        return
      } 
      registrationAPI()
    }

    const registrationAPI = async () => {
      const data = new FormData();
        data.append('name', name)
        if (email.trim().length != 0){
          data.append('email', email)
        }
        data.append('country_code', ccStr)
        data.append('mobile_number', mobile)
        data.append('password', password)
        
        console.log(data)
        setLoader(true)
        const apiData = await callApi('register',data)
        console.log(apiData)
        setLoader(false)
      if(apiData.status){
        showSnackBarOne(apiData.message)
        let userData = {
          id : apiData.data.id,
          name : apiData.data.name,
          email : apiData.data.email,
          country_code : apiData.data.country_code,
          mobile_number : apiData.data.mobile_number
        }
        console.log(userData)
        saveDataOfUser(userData)
        navigation.navigate('OtpVerification',{from:'SignUp',loginType:"1"}) 
      }else{
        showSnackBarOne(apiData.error)
      }
    }

    return(
         <View style={{flex:1,backgroundColor:colorPrimary}}>
             <StatusBarComponent textColor={false}/>
             <Spinner visible={loader}/>
                <FastImage style={{width: DEVICE_WIDTH, height: DEVICE_HEIGHT+30,position:'absolute',backgroundColor:'white'}}/>
                <View style={{flexDirection:'row',marginTop:40,marginStart:15, alignItems:'center'}}>
                    <TouchableOpacity onPress={() => {navigation.pop()}}>
                      <FastImage style={{width: 11, height: 18}} resizeMode={"center"} source={require('../../assets/icons/back_icon.png')}/>
                    </TouchableOpacity>
                    <Text style={{fontFamily:fontFamily.medium, color:colorBlack,marginStart:15,fontSize:fontSize.xl}}>Sign Up</Text>
                </View>
                <Text style={{fontFamily:fontFamily.regular,color:colorBlack,marginTop:5,marginStart:15,fontSize:fontSize.xs}}>Please enter details to register in the app</Text>
                <KeyboardAwareScrollView bounce={false} style={{flex:1,height:DEVICE_HEIGHT}}  keyboardShouldPersistTaps='handled'>
                  <View style={{margin:15,flex:1}}>

                  <View style={myStyles.inputBox}>
                    <TextInput
                      autoCorrect={false}
                      style={myStyles.inputStyle}
                      placeholderStyle={myStyles.inputPlaceHolderStyle}
                      placeholder={'Name'}
                      onChangeText={name => setName(name)}
                      onSubmitEditing={()=>emailRef.current.focus()}
                      blurOnSubmit={false}
                      returnKeyType='next'
                      keyboardType={'default'} 
                      value={name}
                    />
                  </View>

                  <View style={myStyles.inputBox}>
                    <TextInput
                      autoCorrect={false}
                      style={myStyles.inputStyle}
                      placeholderStyle={myStyles.inputPlaceHolderStyle}
                      ref={emailRef}
                      placeholder={'Email (optional)'}
                      onChangeText={email => setEmail(email)}
                      onSubmitEditing={()=>mobileRef.current.focus()}
                      blurOnSubmit={false}
                      returnKeyType='next'
                      keyboardType={'email-address'} 
                      value={email}
                    />
                  </View>

                  <View style={myStyles.inputBox}>
                    <Text style={[myStyles.ccpStyleText,{}]} maxLength={5} ref={ccpRef}>{"+"+ccStr} </Text>
                    <CountryPicker
                    containerButtonStyle={{width:50,height:40,start:10}}
                      withFilter
                      placeholder={''}
                      withCallingCode={true}
                      onSelect={data => {
                        console.log(JSON.stringify("CCP ==>"+data))
                        setccStr(data.callingCode[0]===undefined?'964':data.callingCode[0])
                      }}
                    />
                    <TouchableOpacity>
                      <FastImage style={{width: 10, height: 10}} tintColor={colorBlack} resizeMode={"contain"} source={require('../../assets/icons/drop_down.png')}/>
                    </TouchableOpacity>
                    <TextInput
                      autoCorrect={false}
                      style={[myStyles.inputStyle,{}]}
                      placeholderStyle={myStyles.inputPlaceHolderStyle}
                      ref={mobileRef}
                      placeholder={'Mobile Number'}
                      onChangeText={mobile => setMobile(mobile)}
                      maxLength={13}
                      onSubmitEditing={()=>passwordRef.current.focus()}
                      blurOnSubmit={false}
                      returnKeyType='next'
                      keyboardType={'phone-pad'} 
                      value={mobile}
                    />
                  </View>

                  <View style={myStyles.inputBox}>
                    <TextInput
                      autoCorrect={false}
                      style={myStyles.inputStyle}
                      placeholderStyle={myStyles.inputPlaceHolderStyle}
                      ref={passwordRef}
                      placeholder={'Password'}
                      onChangeText={password => setPassword(password)}
                      onSubmitEditing={()=>confirmPasswordRef.current.focus()}
                      blurOnSubmit={false}
                      returnKeyType='next'
                      keyboardType={'default'} 
                      value={password}
                      secureTextEntry={true}
                    />
                  </View>

                  <View style={myStyles.inputBox}>
                    <TextInput
                      autoCorrect={false}
                      style={myStyles.inputStyle}
                      placeholderStyle={myStyles.inputPlaceHolderStyle}
                      ref={confirmPasswordRef}
                      placeholder={'Confirm Password'}
                      onChangeText={confirmPassword => setConfirmPassword(confirmPassword)}
                      returnKeyType='done'
                      keyboardType={'default'}
                      value={confirmPassword}
                      secureTextEntry={true}
                    />
                  </View>

                  <Text style={{flexWrap: "wrap",alignSelf:'center',margin:20,color:colorBlack,fontSize:fontSize.xs,fontFamily:fontFamily.regular, alignSelf:'center',textAlign:'center',lineHeight:20}}>By registering you agree to our{"\n"}<Text onPress={() => {navigation.navigate('TermsAndConditions')}} style={[{fontFamily:fontFamily.regular,fontSize:fontSize.xs,color:colorPrimary}]} numberOfLines={1}>Terms & Conditions</Text></Text>
                  
                  <TouchableOpacity style={[myStyles.buttonBackground, {justifyContent:'center',marginTop:10,backgroundColor:colorPrimary}]} onPress={() => {validate()}}>
                      <Text style={{fontFamily:fontFamily.medium, color:colorWhite, fontSize:fontSize.m, alignSelf:'center'}}>Sign Up</Text>
                  </TouchableOpacity>
                  
                  <View style={{flex:1}}/>
                  
                  <View style={{flexDirection:'row',marginTop:20}}>
                      <View style={{height:1,backgroundColor:colorBlack,flex:1,alignSelf:'center'}}/>
                      <Text style={{fontFamily:fontFamily.light, color:colorBlack,alignSelf:'center',fontSize:fontSize.m,marginStart:10,marginEnd:10}}>OR</Text>
                      <View style={{height:1,backgroundColor:colorBlack,flex:1,alignSelf:'center'}}/>
                  </View>

                  <View style={{flex:1}}/>

                  <TouchableOpacity style={{flexWrap: "wrap",alignSelf:'center',margin:10}} onPress={() => {navigation.replace('SignIn')}}>
                      <Text style={{fontFamily:fontFamily.light, color:colorBlack, fontSize:fontSize.xs}}>Already an account? <Text style={{fontFamily:fontFamily.semibold, color:colorPrimary, fontSize:fontSize.xs}}>Sign In</Text></Text>
                  </TouchableOpacity>

                  <Text style={{fontFamily:fontFamily.light,margin:10, color:colorBlack,alignSelf:'center', fontSize:fontSize.xs}}>Log in with</Text>
              
                  <View style={{flexDirection:'row',alignSelf:'center'}}>
                      <TouchableOpacity style={{width: 60, height: 60}} onPress={() => {}}>
                          <FastImage style={{width: 60, height: 60}} source={require('../../assets/icons/google_icon.png')}/>
                      </TouchableOpacity>
                      
                      <TouchableOpacity style={{width: 60, height: 60}} onPress={() => {}}>
                          <FastImage
                          style={{width: 60, height: 60}}
                          source={require('../../assets/icons/facebook_icon.png')}/>
                      </TouchableOpacity>

                      <TouchableOpacity style={{width: 60, height: 60}} onPress={() => {}}>
                          <FastImage
                          style={{width: 60, height: 60}}
                          source={require('../../assets/icons/twiter_icon.png')}/>
                      </TouchableOpacity>
                  </View>
                  <TouchableOpacity style={{flexWrap: "wrap",alignSelf:'center',margin:15}} onPress={() => {handleGuest()}}>
                    <Text style={{fontFamily:fontFamily.light, color:colorBlack, fontSize:fontSize.xs}}>Continue as a Guest</Text>
                </TouchableOpacity>
                </View>
                </KeyboardAwareScrollView>
         </View>
    );
}

export default SignUp
