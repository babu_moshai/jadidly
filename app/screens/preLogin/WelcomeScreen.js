import React from 'react'
import {View,TouchableOpacity,Text} from 'react-native'
import StatusBarComponent from '../../components/StatusBarComponent';
import { colorPrimary, colorWhite} from '../../styles/Color';
import { DEVICE_HEIGHT,DEVICE_WIDTH} from '../../styles/Dimens';
import FastImage from 'react-native-fast-image'
import {useNavigation,CommonActions} from '@react-navigation/native'
import { fontFamily, fontSize } from '../../styles/Fonts';
import { myStyles } from '../../styles/ExternalStyle';
import { getDataOfUser, saveDataOfUser } from '../../database/UserData'

function WelcomeScreen(){
    const navigation = useNavigation()

    const handleGuest = async () => {
        let setUserData = {
            id : "0",
            name : "Guest",
            email : "XXX@XXXXX.XXX",
            country_code : "XXX",
            mobile_number : "XXXXXXXXX",
            image : "",
            token:"",
          }
          console.log(setUserData)
          saveDataOfUser(setUserData)
          handleScreen('HomeScreen')
    }
    const handleScreen = async (where) => {
        navigation.dispatch(
          CommonActions.reset({
            index: 1,
            routes: [
              { name: where }
            ],
          })
        );
    }

    return(
         <View style={{flex:1,backgroundColor:colorPrimary}}>
             <StatusBarComponent textColor={'light-content'}/>
                <FastImage
                style={{width: DEVICE_WIDTH, height: DEVICE_HEIGHT+30,position:'absolute'}}
                source={require('../../assets/images/welcome_bg.png')}/>
                <Text style={{fontFamily:fontFamily.medium, letterSpacing:2, color:colorWhite,marginTop:45,marginStart:20,lineHeight:45,fontSize:fontSize.xxxl}}>Welcome to{"\n"}Jadidly</Text>
                <View style={{flexDirection:'row'}}>
                    <Text style={{fontFamily:fontFamily.medium, letterSpacing:2, color:colorWhite,marginStart:20, fontSize:fontSize.l}}>You</Text>
                    <Text style={{fontFamily:fontFamily.regular, letterSpacing:2, color:colorWhite,marginStart:5,marginTop:5, fontSize:fontSize.s,position:'absolute',start:16}}>{"        "}are only a few clicks{"\n"}away to start your car registration{"\n"}renewal</Text>
                </View>
                
            
             <View style={{margin:20,flex:1}}>
                 <View style={{flex:1}}/>
                <TouchableOpacity style={[myStyles.buttonBackground, {justifyContent:'center',backgroundColor:colorPrimary}]} onPress={() => {navigation.navigate('SignIn')}}>
                    <Text style={{fontFamily:fontFamily.medium, color:colorWhite, fontSize:fontSize.l, alignSelf:'center'}}>Sign In</Text>
                </TouchableOpacity>
                
                <TouchableOpacity style={[myStyles.buttonBackground, {justifyContent:'center',marginTop:20,marginBottom:10}]} onPress={() => {navigation.navigate('SignUp')}}>
                    <Text style={{fontFamily:fontFamily.medium, color:colorPrimary, fontSize:fontSize.l, alignSelf:'center'}}>Sign Up</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{flexWrap: "wrap",alignSelf:'center',margin:15}} onPress={() => {handleGuest()}}>
                    <Text style={{fontFamily:fontFamily.light, color:colorWhite, fontSize:fontSize.xs}}>Continue as a Guest</Text>
                </TouchableOpacity>
                
                <View style={{flexDirection:'row'}}>
                    <View style={{height:1,backgroundColor:colorWhite,flex:1,alignSelf:'center'}}/>
                    <Text style={{fontFamily:fontFamily.light, color:colorWhite,alignSelf:'center',fontSize:fontSize.m,marginStart:10,marginEnd:10}}>OR</Text>
                    <View style={{height:1,backgroundColor:colorWhite,flex:1,alignSelf:'center'}}/>
                </View>

                <Text style={{fontFamily:fontFamily.light,margin:10, color:colorWhite,alignSelf:'center', fontSize:fontSize.xs}}>Log in with</Text>
             
                <View style={{flexDirection:'row',alignSelf:'center'}}>
                    <TouchableOpacity style={{width: 60, height: 60}} onPress={() => {}}>
                        <FastImage
                        style={{width: 60, height: 60}}
                        source={require('../../assets/icons/google_icon.png')}/>
                    </TouchableOpacity>
                    
                    <TouchableOpacity style={{width: 60, height: 60}} onPress={() => {}}>
                        <FastImage
                        style={{width: 60, height: 60}}
                        source={require('../../assets/icons/facebook_icon.png')}/>
                    </TouchableOpacity>

                    <TouchableOpacity style={{width: 60, height: 60}} onPress={() => {}}>
                        <FastImage
                        style={{width: 60, height: 60}}
                        source={require('../../assets/icons/twiter_icon.png')}/>
                    </TouchableOpacity>
                </View>
             </View>
         </View>
    );
}

export default WelcomeScreen