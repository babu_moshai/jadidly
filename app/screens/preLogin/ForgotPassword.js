import React, {Component, useState, useRef} from 'react'
import {View,TouchableOpacity,Text,TextInput,Keyboard} from 'react-native'
import StatusBarComponent from '../../components/StatusBarComponent';
import { colorPrimary, colorWhite, colorBlack} from '../../styles/Color';
import { DEVICE_HEIGHT,DEVICE_WIDTH} from '../../styles/Dimens';
import FastImage from 'react-native-fast-image'
import {useNavigation} from '@react-navigation/native'
import { fontFamily, fontSize } from '../../styles/Fonts';
import {myStyles} from '../../styles/ExternalStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import CountryPicker from 'react-native-country-picker-modal';
import { showSnackBarOne } from '../../helpers/showSnackBarOne'
import {callApi} from '../../api/NetworkRequest'
import { DEVICE_TYPE } from '../../utils/Constant'
import Spinner from '../../components/Spinner'
import { getDataOfUser,getLanguage, saveDataOfUser } from '../../database/UserData'

function ForgotPassword({route}){
    const navigation = useNavigation()
    const [loader, setLoader] = useState(false)

    const [loginType, setLoginType] = useState('1')
    const [mobile, setMobile] = useState('')
    const [email, setEmail] = useState('')
    const [from, fromSet] = useState('')
    const [ccStr, setccStr] = useState('965')
    const ccpRef = useRef(null);
    
    const validate = async () => {
      Keyboard.dismiss()
      if(loginType=="1"){
        if (mobile == "") {
          showSnackBarOne("Enter mobile number")
          return
        } else if (mobile.trim().length < 8) {
          showSnackBarOne("Mobile number contain 8 digits at least")
          return
        } else if (mobile.trim().startsWith('0',0)) {
          showSnackBarOne("Enter valid mobile number. It cannot start with zero")
          return
        } 
        forgotPassword()
      }else{
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (email.trim().length == 0) {
          showSnackBarOne("Enter email address")
          return
        } else if (reg.test(email.trim()) === false) {
          showSnackBarOne("Enter valid email address")
          return
        } 
        forgotPassword()
      }
      
    }

    const forgotPassword = async () => {
        const data = new FormData();
        if(loginType=="1"){
          data.append('type', "2")
          data.append('country_code', ccStr)
          data.append('mobile_number', mobile)
        }else if(loginType=="2"){
          data.append('type', "1")
          data.append('email', email)
        }
        
        console.log(data)
        setLoader(true)
        const apiData = await callApi('forgotPassword',data)
        console.log("API RESPONSE=> ",apiData)
        setLoader(false)
      if(apiData.status){
        showSnackBarOne(apiData.message)
        let setUserData = {
          id : apiData.data.user_id,
          email : email,
          country_code : ccStr,
          mobile_number : mobile,
        }
        console.log(setUserData)
        saveDataOfUser(setUserData)
        navigation.replace('OtpVerification',{from:'ForgotPassword',loginType:loginType}) 
      }else{
        showSnackBarOne(apiData.error)
      }
    }

    return(
         <View style={{flex:1,backgroundColor:colorPrimary}}>
             <StatusBarComponent textColor={false}/>
             <Spinner visible={loader}/>
                <FastImage style={{width: DEVICE_WIDTH, height: DEVICE_HEIGHT+30,position:'absolute',backgroundColor:'white'}}/>
                <View style={{flexDirection:'row',marginTop:40,marginStart:15, alignItems:'center'}}>
                    <TouchableOpacity onPress={() => {navigation.pop()}}>
                      <FastImage style={{width: 11, height: 18}} resizeMode={"center"} source={require('../../assets/icons/back_icon.png')}/>
                    </TouchableOpacity>
                    <Text style={{fontFamily:fontFamily.medium, color:colorBlack,marginStart:15,fontSize:fontSize.xl}}>Forgot Password</Text>
                </View>
                
                <KeyboardAwareScrollView bounce={false} style={{flex:1,height:DEVICE_HEIGHT}}  keyboardShouldPersistTaps='handled'>
                  <View style={{margin:15,flex:1}}>
                
                  <View style={{flexWrap:"wrap", alignSelf:'center', marginTop:40, marginBottom:40}}>
                      <Text style={{fontFamily:fontFamily.light, color:colorBlack,textAlign:'center', fontSize:fontSize.m}}>Please enter your phone number or email{"\n"}to receive an OTP</Text>
                  </View>
                  <View style={{flexDirection:'row'}}>
                  <TouchableOpacity style={loginType=="1"?[myStyles.selectedButton]:[myStyles.unselectedButton]} onPress={() => {setLoginType('1')}}>
                      <Text style={{fontFamily:fontFamily.regular, color:loginType=="1"?colorWhite:colorBlack, fontSize:fontSize.s, alignSelf:'center'}}>Mobile Number</Text>
                  </TouchableOpacity>
                  <View style={{width:15}}/>
                   <TouchableOpacity style={loginType=="2"?[myStyles.selectedButton]:[myStyles.unselectedButton]} onPress={() => {setLoginType('2')}}>
                      <Text style={{fontFamily:fontFamily.regular, color:loginType=="2"?colorWhite:colorBlack, fontSize:fontSize.s, alignSelf:'center'}}>Email Address</Text>
                  </TouchableOpacity>
                  </View>
                  <View style={myStyles.inputBox}>
                    {loginType=="1"?
                    <><Text style={[myStyles.ccpStyleText,{}]} maxLength={5} ref={ccpRef}>{"+"+ccStr} </Text>
                    <CountryPicker
                    containerButtonStyle={{width:50,height:40,start:10}}
                      withFilter
                      placeholder={''}
                      withCallingCode={true}
                      onSelect={data => {
                        setccStr(data.callingCode[0]===undefined?'964':data.callingCode[0])
                      }}
                    />
                      <TouchableOpacity>
                        <FastImage style={{width: 10, height: 10}} tintColor={colorBlack} resizeMode={"contain"} source={require('../../assets/icons/drop_down.png')}/>
                      </TouchableOpacity>
                      <TextInput
                        autoCorrect={false}
                        style={myStyles.inputStyle}
                        placeholderStyle={myStyles.inputPlaceHolderStyle}
                        placeholder={'Mobile Number'}
                        onChangeText={mobile => setMobile(mobile)}
                        returnKeyType='done'
                        maxLength={13}
                        keyboardType='number-pad'
                        value={mobile}
                      />
                      </>:
                      <TextInput
                        autoCorrect={false}
                        style={myStyles.inputStyle}
                        placeholderStyle={myStyles.inputPlaceHolderStyle}
                        placeholder={'Email'}
                        onChangeText={email => setEmail(email)}
                        returnKeyType='done'
                        value={email}/>
                    }
                  </View>

                  {/* <View style={myStyles.inputBox}>
                    <TextInput
                      autoCorrect={false}
                      style={myStyles.inputStyle}
                      placeholderStyle={myStyles.inputPlaceHolderStyle}
                      placeholder={'Phone Number or Email'}
                      onChangeText={numberOrEmail => setNumberOrEmail(numberOrEmail)}
                      onSubmitEditing={()=>passwordRef.current.focus()}
                      blurOnSubmit={false}
                      returnKeyType='next'
                      value={numberOrEmail}
                    />
                  </View> */}
                  
                  <TouchableOpacity style={[myStyles.buttonBackground, {justifyContent:'center',marginTop:50,backgroundColor:colorPrimary}]} 
                  onPress={() => {validate()}}>
                      <Text style={{fontFamily:fontFamily.medium, color:colorWhite, fontSize:fontSize.m, alignSelf:'center'}}>Continue</Text>
                  </TouchableOpacity>
                  
                </View>
                </KeyboardAwareScrollView>
         </View>
    );
}

export default ForgotPassword
