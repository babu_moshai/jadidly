import React, {Component, useState, useRef} from 'react'
import {View,TouchableOpacity,Text,TextInput} from 'react-native'
import StatusBarComponent from '../../components/StatusBarComponent';
import Spinner from '../../components/Spinner';
import { colorPrimary, colorWhite, colorBlack, cardBorderColor, lightFont} from '../../styles/Color';
import { DEVICE_HEIGHT,DEVICE_WIDTH} from '../../styles/Dimens';
import FastImage from 'react-native-fast-image'
import {useNavigation} from '@react-navigation/native'
import { fontFamily, fontSize } from '../../styles/Fonts';
import {myStyles} from '../../styles/ExternalStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import RequestModel from '../../components/modal/RequestModel'

function AddCar(){
    const navigation = useNavigation()
    const data1 = ["Brand 1","Brand 2","Brand 3","Brand 4","Brand 5","Brand 6"]
    const data2 = ["Car Name 1","Car Name 2","Car Name 3","Car Name 4","Car Name 5","Car Name 6"]
    const data3 = ["2014","2015","2016","2017","2018","2019"]
    const [carBrand, setCarBrand] = useState('')
    const [carName, setCarName] = useState('')
    const [modelYear, setModelYear] = useState('')
    
    const [visible1, setVisible1] = useState(false)
    const [visible2, setVisible2] = useState(false)
    const [visible3, setVisible3] = useState(false)
    const [loader, setLoader] = useState(false)
  

    return(
         <View style={{flex:1,backgroundColor:colorWhite}}>
             <StatusBarComponent textColor={true}/>
             <Spinner visible={loader}  />
             <View style={{backgroundColor:colorPrimary,paddingBottom:15,borderBottomEndRadius:10,borderBottomStartRadius:10}}>
                <View style={{flexDirection:'row',marginTop:35,marginStart:15, alignItems:'center'}}>
                  <TouchableOpacity onPress={() => {navigation.pop()}}>
                        <FastImage style={{width: 11, height: 18}} tintColor={colorWhite} resizeMode={"center"} source={require('../../assets/icons/back_icon.png')}/>
                  </TouchableOpacity>
                  <Text style={{fontFamily:fontFamily.medium, color:colorWhite,marginStart:15,fontSize:fontSize.xl}}>Add a Car</Text>
                </View>
             </View>
               
              <KeyboardAwareScrollView bounce={false} style={{flex:1,height:DEVICE_HEIGHT,backgroundColor:'white'}}  keyboardShouldPersistTaps='handled'>
                <View style={{flex:1,padding:15,backgroundColor:'white'}}>

                <RequestModel
                  visibility={visible1}
                  data={data1}
                  headingText={"Select a Car Brand"}
                  crossButton={() => setVisible1(!visible1)}
                  onSelection={(index) => {
                    setVisible1(!visible1)
                    setCarBrand(data1[index])}}
                />
                <RequestModel
                  visibility={visible2}
                  data={data2}
                  headingText={"Select a Car Name"}
                  crossButton={() => setVisible2(!visible2)}
                  onSelection={(index) => {
                    setVisible2(!visible2)
                    setCarName(data2[index])}}
                />
                <RequestModel
                  visibility={visible3}
                  data={data3}
                  headingText={"Select a Car Model Name"}
                  crossButton={() => setVisible3(!visible3)}
                  onSelection={(index) => {
                    setVisible3(!visible3)
                    setModelYear(data3[index])}}
                />
                
                <View style={myStyles.inputBox}>
                <TouchableOpacity style={{flex:1,flexDirection:'row',alignContent: 'center', alignItems: 'center',}} onPress={() => {setVisible1(!visible1)}}>
                  <Text style={{flex:1,margin:10,fontFamily:fontFamily.medium, color:colorBlack, fontSize:fontSize.m, alignSelf:'center'}}>{carBrand?carBrand:'Car Brand'}</Text>
                  <FastImage style={{width: 11, height: 18,rotation:270,marginEnd:20}}  tintColor={cardBorderColor} resizeMode={"center"} source={require('../../assets/icons/back_icon.png')}/>
                </TouchableOpacity>
                </View>

                <View style={myStyles.inputBox}>
                <TouchableOpacity style={{flex:1,flexDirection:'row',alignContent: 'center', alignItems: 'center',}} onPress={() => {setVisible2(!visible2)}}>
                  <Text style={{flex:1,margin:10,fontFamily:fontFamily.medium, color:colorBlack, fontSize:fontSize.m, alignSelf:'center'}}>{carName?carName:'Car Name'}</Text>
                  <FastImage style={{width: 11, height: 18,rotation:270,marginEnd:20}}  tintColor={cardBorderColor} resizeMode={"center"} source={require('../../assets/icons/back_icon.png')}/>
                </TouchableOpacity>
                </View>

                <View style={myStyles.inputBox}>
                <TouchableOpacity style={{flex:1,flexDirection:'row',alignContent: 'center', alignItems: 'center',}} onPress={() => {setVisible3(!visible3)}}>
                  <Text style={{flex:1,margin:10,fontFamily:fontFamily.medium, color:colorBlack, fontSize:fontSize.m, alignSelf:'center'}}>{modelYear?modelYear:'Model Year'}</Text>
                  <FastImage style={{width: 11, height: 18,rotation:270,marginEnd:20}}  tintColor={cardBorderColor} resizeMode={"center"} source={require('../../assets/icons/back_icon.png')}/>
                </TouchableOpacity>
                </View>
               
                <View style={{flexDirection:'row',flex:1,marginTop:20,marginStart:-5}}>
                    <View style={{alignContent:'center',justifyContent:'center'}}>
                      <FastImage style={{width: 100, height: 100}} source={require('../../assets/icons/ract_dash.png')}/>   
                      <FastImage style={{width: 30, height: 30,position:'absolute',alignSelf:'center'}} source={require('../../assets/icons/plus_icon.png')}/>   
                      {/* <FastImage style={{width: 85, height: 85,borderRadius:10,marginStart:8,top:5,position:'absolute'}} source={require('../../assets/images/placeholder.png')}/>    */}
                    </View> 
                </View>

                <TouchableOpacity style={[myStyles.buttonBackground, {justifyContent:'center',marginTop:40, backgroundColor:colorPrimary}]} onPress={() => {}}>
                    <Text style={{fontFamily:fontFamily.medium, color:colorWhite, fontSize:fontSize.m, alignSelf:'center'}}>Submit</Text>
                </TouchableOpacity>
                
              </View>
              </KeyboardAwareScrollView>
         </View>
    );
}

export default AddCar
