import React, {Component, useState, useRef, useEffect} from 'react'
import {View,TouchableOpacity,Text,TextInput,Keyboard,Alert,Linking} from 'react-native'
import StatusBarComponent from '../../components/StatusBarComponent';
import { colorPrimary, colorWhite, colorBlack, cardBorderColor, lightFont} from '../../styles/Color';
import { DEVICE_HEIGHT,DEVICE_WIDTH} from '../../styles/Dimens';
import FastImage from 'react-native-fast-image'
import {useNavigation,CommonActions} from '@react-navigation/native'
import { fontFamily, fontSize } from '../../styles/Fonts';
import {myStyles} from '../../styles/ExternalStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import RequestModel from '../../components/modal/RequestModel'
import ImageAndDocPickerModel from '../../components/modal/ImageAndDocPickerModel'
import { showSnackBarOne } from '../../helpers/showSnackBarOne'
import {callApiNoData,callApiWithData} from '../../api/NetworkRequest'
import Spinner from '../../components/Spinner'
import { getDataOfUser, saveDataOfUser, logoutUser } from '../../database/UserData'
function HelpAndSupport(){
    const navigation = useNavigation()
    const data = ["Subject 1","Subject 2","Subject 3","Subject 4","Subject 5","Subject 6"]
    const [subject, setSubject] = useState('Select a Subject')
    const [subjectId, setSubjectId] = useState('')
    const [detail, setDetail] = useState('')
    const [loader, setLoader] = useState(false)
    const [visible, setVisible] = useState(false)
    
    const subjectRef = useRef(null);
    const detailRef = useRef(null);

    const [uploadType, setUploadType] = useState('0')
    const [image1, setImage1] = useState('')
    const [image2, setImage2] = useState('')
    const [imageSent1, setImageSent1] = useState({})
    const [imageSent2, setImageSent2] = useState({})
    const [subjectList, setSubjectList] = useState({})
    const [imagePicker, setImagePicker] = useState(false)

    useEffect(()=>{
      getSubjectList()
    },[])
  
    const getSubjectList = async () => {
      let userDataLocale =  await getDataOfUser()
        
      setLoader(true)
      const apiData = await callApiNoData('subjectList',userDataLocale.token)
      console.log("API RESPONSE=> ",apiData)
      setLoader(false)

      if(apiData.status){
        //showSnackBarOne(apiData.message)
        setSubjectList(apiData.data)
      }else if(apiData.status_code == "401"){
        Alert.alert("Logout", apiData.error, 
        [
          {text: "ok", onPress: () => {
            logoutUser()
            navigation.dispatch(
              CommonActions.reset({
                index: 1,
                routes: [
                  { name: 'WelcomeScreen' }
                ],
              })
            );
          }},
        ]
        ,{ cancelable: false }
      )
      }else{
        showSnackBarOne(apiData.error)
      }
    }


    const showImage = (image)=>{
      if(uploadType==1){
        setImage1(image.path)
        setImageSent1({
          uri: image.path,
          name: 'MyImage.jpeg',
          type: image.mime
         })
      }else if(uploadType==2){
        setImage2(image.path)
        setImageSent2({
          uri: image.path,
          name: 'id_proof.jpeg',
          type: image.mime
         })
      }
      setImagePicker(false)
    }

    const validate = async () => {
      Keyboard.dismiss()
      if (subject == "" || subject == "Select a Subject") {
        showSnackBarOne("Select a subject")
        return
      }else if (detail == "") {
        showSnackBarOne("Write something in details")
        return
      } else if (detail.trim().length < 5) {
        showSnackBarOne("Write a descriptive details in between length of 5 to 300")
        return
      }
      helpAndSupport()
    }

    const helpAndSupport = async () => {
      let userDataLocale =  await getDataOfUser()

      const data = new FormData();
        data.append('subject', subjectId)
        data.append('details', detail)
        if(Object.keys(imageSent1).length != 0 && Object.keys(imageSent2).length != 0){
          data.append('images[0]', imageSent1)
          data.append('images[1]', imageSent2)
        }else if(Object.keys(imageSent1).length != 0){
          data.append('images[0]', imageSent1)
        }else if(Object.keys(imageSent2).length != 0){
          data.append('images[0]', imageSent2)
        }
        

      console.log("API data=> ",data)
      setLoader(true)
      const apiData = await callApiWithData('contactUs',data,userDataLocale.token)
      console.log("API RESPONSE=> ",apiData)
      setLoader(false)

      if(apiData.status){
        showSnackBarOne(apiData.message)
        navigation.pop()
      }else if(apiData.status_code == "401"){
        Alert.alert("Logout", apiData.error, 
        [
          {text: "ok", onPress: () => {
            logoutUser()
            navigation.dispatch(
              CommonActions.reset({
                index: 1,
                routes: [
                  { name: 'WelcomeScreen' }
                ],
              })
            );
          }},
        ]
        ,{ cancelable: false }
      )
      }else{
        showSnackBarOne(apiData.error)
      }
    }

    return(
         <View style={{flex:1,backgroundColor:colorWhite}}>
             <StatusBarComponent textColor={true}/>
             <Spinner visible={loader}  />
             <ImageAndDocPickerModel
                visibility={imagePicker ? imagePicker : false}
                imageObject={image => showImage(image)}
                onCancel={() => setImagePicker(false)}
              />
             <View style={{backgroundColor:colorPrimary,paddingBottom:15,borderBottomEndRadius:10,borderBottomStartRadius:10}}>
                <View style={{flexDirection:'row',marginTop:35,marginStart:15, alignItems:'center'}}>
                  <TouchableOpacity onPress={() => {navigation.pop()}}>
                        <FastImage style={{width: 11, height: 18}} tintColor={colorWhite} resizeMode={"center"} source={require('../../assets/icons/back_icon.png')}/>
                  </TouchableOpacity>
                  <Text style={{fontFamily:fontFamily.medium, color:colorWhite,marginStart:15,fontSize:fontSize.xl}}>Help and Supoort</Text>
                </View>
             </View>
               
              <KeyboardAwareScrollView bounce={false} style={{flex:1,height:DEVICE_HEIGHT,backgroundColor:'white'}}  keyboardShouldPersistTaps='handled'>
                <View style={{flex:1,padding:15,backgroundColor:'white'}}>

                <RequestModel
                  visibility={visible}
                  data={subjectList}
                  headingText={"Select a Subject"}
                  crossButton={() => setVisible(!visible)}
                  onSelection={(index) => {
                    setVisible(!visible)
                    setSubject(subjectList[index].subject)
                    setSubjectId(subjectList[index].id)}}
                />
                
                <View style={myStyles.inputBox}>
                <TouchableOpacity style={{flex:1,flexDirection:'row',alignContent: 'center', alignItems: 'center',}} onPress={() => {setVisible(!visible)}}>
                  <Text style={{flex:1,margin:10,fontFamily:fontFamily.medium, color:colorBlack, fontSize:fontSize.m, alignSelf:'center'}}>{subject}</Text>
                  <FastImage style={{width: 11, height: 18,rotation:270,marginEnd:20}}  tintColor={cardBorderColor} resizeMode={"center"} source={require('../../assets/icons/back_icon.png')}/>
                </TouchableOpacity>
                </View>
                
                <View style={[myStyles.inputBox,{height:100}]}>
                  <TextInput
                    autoCorrect={false}
                    style={[myStyles.inputStyle,{height:100,textAlignVertical:'top'}]}
                    placeholderStyle={myStyles.inputPlaceHolderStyle}
                    ref={detailRef}
                    placeholder={'Write here your details'}
                    onChangeText={detail => setDetail(detail)}
                    returnKeyType={'none'}
                    multiline={true}
                    value={detail}
                    maxLength={300}
                  />
                </View>

                <View style={[myStyles.cardBox]}>
                  <View style={{flexDirection:'row',flex:1}}>
                    <View style={{flex:1}}>
                      <Text style={{fontFamily:fontFamily.semibold, color:colorBlack, fontSize:fontSize.m}}>Contact 24*7 Help</Text>
                      <Text style={{fontFamily:fontFamily.semibold, color:lightFont, fontSize:fontSize.xs}}>Our executive will solve your problem</Text>
                    </View>
                    <TouchableOpacity onPress={()=> {Linking.openURL(`tel:${'123456789'}`)}}>
                      <FastImage style={{width: 40, height: 40}} source={require('../../assets/icons/call_icon.png')}/>
                    </TouchableOpacity>
                    
                  </View>
                </View>
                <View style={{flexDirection:'row',flex:1,marginTop:20}}>
                    
                    
                    <TouchableOpacity activeOpacity={0.9} onPress={()=> {setImagePicker(true),setUploadType(1)}} 
                      style={[myStyles.elevation,{backgroundColor:colorWhite,width: 80, height: 80,alignContent:'center',justifyContent:'center',borderStyle:'dashed', borderRadius:10,borderColor:colorPrimary,borderWidth:1.2}]}>
                      <FastImage style={{width: 30, height: 30,alignSelf:'center'}} source={require('../../assets/icons/plus_icon.png')}/>    
                      {image1==="undefined" || image1 =='' ?null:<FastImage style={{start:-2,width: 82, height: 82,position:'absolute',borderRadius:10}} resizeMode={'cover'} source={{uri:image1}}/> }
                      {image1==="undefined" || image1 =='' ?null:
                      <TouchableOpacity style={{height:20,width:20,borderRadius:10,justifyContent:'center',end:5,top:5,position:'absolute',backgroundColor:'rgba(0,0,0,0.6)'}} onPress={()=> {setImage1(''),setImageSent2({})}} >
                        <FastImage style={{width: 10, height: 10,alignSelf:'center'}} resizeMode={'contain'} tintColor={colorWhite} source={require('../../assets/icons/cross_icon.png')}/>
                      </TouchableOpacity>}
                    </TouchableOpacity>
                    
                    <TouchableOpacity activeOpacity={0.9} onPress={()=> {setImagePicker(true),setUploadType(2)}} 
                      style={[myStyles.elevation,{marginStart:15,backgroundColor:colorWhite,width: 80, height: 80,alignContent:'center',justifyContent:'center',borderStyle:'dashed', borderRadius:10,borderColor:colorPrimary,borderWidth:1.2}]}>
                      <FastImage style={{width: 30, height: 30,alignSelf:'center'}} source={require('../../assets/icons/plus_icon.png')}/>    
                      {image2==="undefined" || image2 =='' ?null:<FastImage style={{start:-2,width: 82, height: 82,position:'absolute',borderRadius:10}} resizeMode={'cover'} source={{uri:image2}}/> }
                      {image2==="undefined" || image2 =='' ?null:
                      <TouchableOpacity style={{height:20,width:20,borderRadius:10,justifyContent:'center',end:5,top:5,position:'absolute',backgroundColor:'rgba(0,0,0,0.6)'}} onPress={()=> {setImage2(''),setImageSent2({})}} >
                        <FastImage style={{width: 10, height: 10,alignSelf:'center'}} resizeMode={'contain'} tintColor={colorWhite} source={require('../../assets/icons/cross_icon.png')}/>
                      </TouchableOpacity>}
                    </TouchableOpacity>   
                </View>

                <TouchableOpacity style={[myStyles.buttonBackground, {justifyContent:'center',marginTop:40, backgroundColor:colorPrimary}]} onPress={() => {validate()}}>
                    <Text style={{fontFamily:fontFamily.medium, color:colorWhite, fontSize:fontSize.m, alignSelf:'center'}}>Submit</Text>
                </TouchableOpacity>
                
              </View>
              </KeyboardAwareScrollView>
         </View>
    );
}

export default HelpAndSupport
