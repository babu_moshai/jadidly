import React, {useEffect, useState, useRef} from 'react'
import {View,TouchableOpacity,Text,TextInput,Keyboard,Alert} from 'react-native'
import StatusBarComponent from '../../components/StatusBarComponent';
import { colorPrimary, colorWhite, colorBlack} from '../../styles/Color';
import { DEVICE_HEIGHT,DEVICE_WIDTH} from '../../styles/Dimens';
import FastImage from 'react-native-fast-image'
import {useNavigation,CommonActions} from '@react-navigation/native'
import { fontFamily, fontSize } from '../../styles/Fonts';
import {myStyles} from '../../styles/ExternalStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import ImageAndDocPickerModel from '../../components/modal/ImageAndDocPickerModel'
import { showSnackBarOne } from '../../helpers/showSnackBarOne'
import {callApiNoData,callApiWithData} from '../../api/NetworkRequest'
import Spinner from '../../components/Spinner'
import { getDataOfUser, saveDataOfUser, logoutUser } from '../../database/UserData'
import CountryPicker from 'react-native-country-picker-modal';
function Profile(){
    const navigation = useNavigation()
    const [loader, setLoader] = useState(false)

    const [editable, setEditable] = useState(false)
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [mobile, setMobile] = useState('')
    const [ccStr, setccStr] = useState('964')
    
    const [image, setImage] = useState('')
    const [imageSent, setImageSent] = useState({})
    const [imagePicker, setImagePicker] = useState(false)
    
    const emailRef = useRef(null);
    const mobileRef = useRef(null);
    const ccpRef = useRef(null);


    useEffect(() => {
      myProfile()
    },[])

    const showImage = (image)=>{
      setImage(image.path)
      setImageSent({
        uri: image.path,
        name: 'MyImage.jpeg',
        type: image.mime
       })
      setImagePicker(false)
    }


    const myProfile = async () => {
      let userDataLocale =  await getDataOfUser()
        
      setLoader(true)
      const apiData = await callApiNoData('profileDetail',userDataLocale.token)
      console.log("API RESPONSE=> ",apiData)
      setLoader(false)

      if(apiData.status){
        //showSnackBarOne(apiData.message)
        setImage(apiData.data.image?apiData.data.image:'')
        setName(apiData.data.name)
        setccStr(apiData.data.country_code)
        setEmail(apiData.data.email)
        setMobile(apiData.data.mobile_number)
        saveDataOfUser({})
        let userDataSet = {
          id : apiData.data.id,
          name : apiData.data.name,
          email : apiData.data.email,
          country_code : apiData.data.country_code,
          mobile_number : apiData.data.mobile_number,
          image:apiData.data.image,
          token:userDataLocale.token,
        }
        console.log(userDataSet)
        saveDataOfUser(userDataSet)
        
        
      }else if(apiData.status_code == "401"){
        Alert.alert("Logout", apiData.error, 
        [
          {text: "ok", onPress: () => {
            logoutUser()
            navigation.dispatch(
              CommonActions.reset({
                index: 1,
                routes: [
                  { name: 'WelcomeScreen' }
                ],
              })
            );
          }},
        ]
        ,{ cancelable: false }
      )
      }else{
        showSnackBarOne(apiData.error)
      }
    }

    const validate = async () => {
      console.log("API email=> ",email)
      Keyboard.dismiss()
      let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (image==="undefined" || image =='') {
        showSnackBarOne("Upload a image")
        return
      } else if (name == "") {
        showSnackBarOne("Enter name")
        return
      } else if (name.trim().length < 3) {
        showSnackBarOne("Enter a valid name. It should contain 2 character at least.")
        return
      } else if (mobile == "") {
        showSnackBarOne("Enter mobile number")
        return
      } else if (mobile.trim().length < 8) {
        showSnackBarOne("Mobile number contain 8 digits at least")
        return
      } else if (mobile.trim().startsWith('0',0)) {
        showSnackBarOne("Enter valid mobile number. It cannot start with zero")
        return
      } 
      // else if (email.trim().length == 0) {
      //   showSnackBarOne("Enter email address")
      //   return
      // } 
      else if (email.trim().length != 0 && reg.test(email.trim()) === false) {
        showSnackBarOne("Enter valid email address")
        return
      }
      editProfileApi()
    }

    const editProfileApi = async () => {
      let userDataLocale =  await getDataOfUser()

      const data = new FormData();
        data.append('name', name)  
        data.append('email', email)
        data.append('country_code', ccStr)
        data.append('mobile_number', mobile)
        if(Object.keys(imageSent).length != 0){
          data.append('image', imageSent)
        }
        

      console.log("API data=> ",data)
      setLoader(true)
      const apiData = await callApiWithData('updateProfile',data,userDataLocale.token)
      console.log("API RESPONSE=> ",apiData)
      setLoader(false)

      if(apiData.status){
        showSnackBarOne(apiData.message)
        setImage(apiData.data.image)
        setName(apiData.data.name)
        setccStr(apiData.data.country_code)
        setMobile(apiData.data.mobile_number)
        setEmail(apiData.data.email)
        saveDataOfUser({})
        let userData = {
          id : apiData.data.id,
          name : apiData.data.name,
          email : apiData.data.email,
          country_code : apiData.data.country_code,
          mobile_number : apiData.data.mobile_number,
          image:apiData.data.image,
          token:userDataLocale.token,
        }
        console.log(userData)
        saveDataOfUser(userData)
        setEditable(false)
        
      }else if(apiData.status_code == "401"){
        Alert.alert("Logout", apiData.error, 
        [
          {text: "ok", onPress: () => {
            logoutUser()
            navigation.dispatch(
              CommonActions.reset({
                index: 1,
                routes: [
                  { name: 'WelcomeScreen' }
                ],
              })
            );
          }},
        ]
        ,{ cancelable: false }
      )
      }else{
        showSnackBarOne(apiData.error)
      }
    }

    return(
         <View style={{flex:1,backgroundColor:colorWhite}}>
             <StatusBarComponent textColor={true}/>
             <Spinner visible={loader}  />
             <ImageAndDocPickerModel
                visibility={imagePicker ? imagePicker : false}
                imageObject={image => showImage(image)}
                onCancel={() => setImagePicker(false)}
              />
             <View style={{backgroundColor:colorPrimary,paddingBottom:25}}>
               
                <View style={{flexDirection:'row',marginTop:40,marginStart:15, alignItems:'center'}}>
                  <TouchableOpacity onPress={() => {editable?setEditable(false):navigation.pop()}}>
                        <FastImage style={{width: 11, height: 18}} tintColor={colorWhite} resizeMode={"center"} source={require('../../assets/icons/back_icon.png')}/>
                  </TouchableOpacity>
                  <Text style={{fontFamily:fontFamily.medium, color:colorWhite,marginStart:15,fontSize:fontSize.xl}}>{editable?'Edit Profile':'My Profile'}</Text>
                </View>
                <FastImage style={{width: 150, height:150,alignSelf:'center',marginTop:50,borderRadius:75}}  
                source={image==="undefined" || image =='' ? require('../../assets/images/placeholder.png'): {uri:image} }/>
                {editable?<TouchableOpacity style={{width: 40, height:40,alignSelf:'center',marginEnd:-95,marginTop:-35,borderRadius:20}} 
                onPress={() => {setImagePicker(true)}}>
                  <FastImage style={{width: 40, height:40}} source={require('../../assets/icons/camera_icon.png')}/>
                </TouchableOpacity>:<TouchableOpacity style={{width: 40, height:40,alignSelf:'center',marginEnd:-95,marginTop:-35,borderRadius:20}}></TouchableOpacity>}
             </View>
               
                <KeyboardAwareScrollView bounce={false} style={{flex:1,height:DEVICE_HEIGHT,backgroundColor:'white'}}  keyboardShouldPersistTaps='handled'>
                  <View style={{flex:1,padding:15,backgroundColor:'white'}}>
                  <View style={myStyles.inputBox}>
                    <TextInput
                      autoCorrect={false}
                      style={myStyles.inputStyle}
                      placeholderStyle={myStyles.inputPlaceHolderStyle}
                      placeholder={'Name'}
                      onChangeText={name => setName(name)}
                      // onSubmitEditing={()=>emailRef.current.focus()}
                      // blurOnSubmit={false}
                      returnKeyType='done'
                      keyboardType={'default'} 
                      value={name}
                      editable={editable}
                    />
                  </View>

                  <View style={myStyles.inputBox}>
                    <TextInput
                      autoCorrect={false}
                      style={myStyles.inputStyle}
                      placeholderStyle={myStyles.inputPlaceHolderStyle}
                      ref={emailRef}
                      placeholder={'Email'}
                      onChangeText={email => setEmail(email)}
                      onSubmitEditing={()=>mobileRef.current.focus()}
                      blurOnSubmit={false}
                      returnKeyType='next'
                      keyboardType={'email-address'} 
                      value={email}
                      editable={false}
                    />
                  </View>

                  <View style={myStyles.inputBox}>
                  <><Text style={[myStyles.ccpStyleText,{}]} maxLength={5} ref={ccpRef}>{"+"+ccStr} </Text>
                    {/* <CountryPicker
                    containerButtonStyle={{width:50,height:40,start:10}}
                      withFilter
                      placeholder={''}
                      withCallingCode={true}
                      onSelect={data => {
                        setccStr(data.callingCode[0])
                      }}
                    /> */}
                    <TextInput
                      autoCorrect={false}
                      style={[myStyles.inputStyle,{marginStart:50}]}
                      placeholderStyle={myStyles.inputPlaceHolderStyle}
                      ref={mobileRef}
                      placeholder={'Mobile Number'}
                      onChangeText={mobile => setMobile(mobile)}
                      returnKeyType='done'
                      keyboardType={'phone-pad'} 
                      value={mobile}
                      editable={false}
                    />
                    </>
                  </View>
                  <TouchableOpacity style={[myStyles.buttonBackground, {justifyContent:'center',marginTop:40, backgroundColor:colorPrimary}]}
                   onPress={() => {editable?validate():setEditable(true)}}>
                      <Text style={{fontFamily:fontFamily.medium, color:colorWhite, fontSize:fontSize.m, alignSelf:'center'}}>{editable?'Save Profile':'Edit Profile'}</Text>
                  </TouchableOpacity>
                  
                </View>
                </KeyboardAwareScrollView>
         </View>
    );
}

export default Profile
