import React, {Component, useState, useRef} from 'react'
import {View,TouchableOpacity,Text,FlatList} from 'react-native'
import StatusBarComponent from '../../components/StatusBarComponent';
import Spinner from '../../components/Spinner';
import { colorPrimary, colorWhite, colorBlack, cardBorderColor, lightFont} from '../../styles/Color';
import { DEVICE_HEIGHT,DEVICE_WIDTH} from '../../styles/Dimens';
import FastImage from 'react-native-fast-image'
import {useNavigation} from '@react-navigation/native'
import { fontFamily, fontSize } from '../../styles/Fonts';
import {myStyles} from '../../styles/ExternalStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import RequestModel from '../../components/modal/RequestModel'

function MyBookings(){
    const navigation = useNavigation()
    const [loader, setLoader] = useState(false)
    const [updated, setUpdated] = useState(false)
    const data = ["1","2","3","1","2","3"]

    return(
         <View style={{flex:1,backgroundColor:colorWhite}}>
             <StatusBarComponent textColor={true}/>
             <Spinner visible={loader}  />
             <View style={{backgroundColor:colorPrimary,paddingBottom:15,borderBottomEndRadius:10,borderBottomStartRadius:10}}>
                <View style={{flexDirection:'row',marginTop:35,marginStart:15, alignItems:'center'}}>
                  <TouchableOpacity onPress={() => {navigation.pop()}}>
                        <FastImage style={{width: 11, height: 18}} tintColor={colorWhite} resizeMode={"center"} source={require('../../assets/icons/back_icon.png')}/>
                  </TouchableOpacity>
                  <Text style={{fontFamily:fontFamily.medium, color:colorWhite,marginStart:15,fontSize:fontSize.xl}}>My Bookings</Text>
                </View>
             </View>
              
            <View style={{flex:1,backgroundColor:'white'}}>

                <FlatList
                    extraData={updated}
                    data={data}
                    showsVerticalScrollIndicator={false}
                    style={{flexGrow:1,height:DEVICE_HEIGHT}}
                    renderItem={({ item: rowData }) => {
                        return(
                            <View style={[myStyles.cardBox,{marginStart:15,marginEnd:15,marginBottom:15,marginTop:15}]}>
                                <TouchableOpacity style={{}} onPress={() => {}}>
                                    <View style={{flexDirection:'row',flex:1}}>
                                        <View style={{backgroundColor:colorPrimary,padding:10,width: 70, height: 70,borderRadius:10,alignItems:'center',justifyContent:'center'}}>
                                            <FastImage style={{height:40,width:40}} tintColor={colorWhite} source={require('../../assets/icons/booking_list_icon.png')}/>
                                        </View>
                                        <View style={{flex:1,marginStart:10}}>
                                            <Text style={{fontFamily:fontFamily.semibold, color:colorBlack, fontSize:fontSize.m}}>Booking</Text>
                                            <Text style={{fontFamily:fontFamily.semibold, color:lightFont, fontSize:fontSize.xs}}>Description</Text>
                                            <Text style={{fontFamily:fontFamily.semibold, color:lightFont, fontSize:fontSize.xss}}>24 January, 2020 1:55 PM</Text>
                                            <Text style={{fontFamily:fontFamily.semibold, color:lightFont, fontSize:fontSize.m}}>*123456</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                                <View style={{marginStart:-10,marginEnd:-10,height:1,backgroundColor:cardBorderColor}}/>
                                <View style={{flexDirection:'row',flex:1,padding:10,paddingBottom:0,alignItems:'center'}}>
                                    <View style={{height:10,width:10,borderRadius:5,backgroundColor:'green',marginBottom:4}}/>
                                    <Text style={{fontFamily:fontFamily.semibold, color:colorBlack, fontSize:fontSize.m,marginStart:10,flex:1}}>COMPLETED</Text>
                                    <Text style={{fontFamily:fontFamily.semibold, color:colorPrimary, fontSize:fontSize.m,marginEnd:-10}}>KWD 100</Text>
                                </View>
                            </View>                             
                        );
                    }}
                    horizontal={false}
                    keyExtractor={(item, index) => index}
                />
                
                            
              </View>
             
         </View>
    );
}

export default MyBookings
