import React from 'react'
import {View,TouchableOpacity,Text,TextInput} from 'react-native'
import StatusBarComponent from '../../components/StatusBarComponent';
import { colorPrimary, colorWhite} from '../../styles/Color';
import FastImage from 'react-native-fast-image'
import {useNavigation} from '@react-navigation/native'
import { fontFamily, fontSize } from '../../styles/Fonts';
import { WebView } from 'react-native-webview';

function AboutUs(){
    const navigation = useNavigation()
    return(
         <View style={{flex:1,backgroundColor:colorWhite}}>
             <StatusBarComponent textColor={true}/>
             <View style={{backgroundColor:colorPrimary,paddingBottom:15,borderBottomEndRadius:10,borderBottomStartRadius:10}}>
                <View style={{flexDirection:'row',marginTop:35,marginStart:15, alignItems:'center'}}>
                  <TouchableOpacity onPress={() => {navigation.pop()}}>
                        <FastImage style={{width: 11, height: 18}} tintColor={colorWhite} resizeMode={"center"} source={require('../../assets/icons/back_icon.png')}/>
                  </TouchableOpacity>
                  <Text style={{fontFamily:fontFamily.medium, color:colorWhite,marginStart:15,fontSize:fontSize.xl}}>About Us</Text>
                </View>
             </View>
               
              <View style={{backgroundColor:colorWhite, borderTopLeftRadius:25, borderTopRightRadius:25, flex:1, marginTop:20}}>
                <View style={{flex:1}}>
                  <WebView source={{ uri: 'http://188.166.65.5/jadidly/public/api/about-us'}} />
                </View>
              </View>
         </View>
    );
}

export default AboutUs
