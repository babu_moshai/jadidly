import React, {useEffect, useState, useRef} from 'react'
import {View,TouchableOpacity,Text,TextInput} from 'react-native'
import StatusBarComponent from '../../components/StatusBarComponent';
import Spinner from '../../components/Spinner';
import { colorPrimary, colorWhite, colorBlack, cardBorderColor, lightFont, darkFont} from '../../styles/Color';
import { DEVICE_HEIGHT,DEVICE_WIDTH} from '../../styles/Dimens';
import FastImage from 'react-native-fast-image'
import {useNavigation} from '@react-navigation/native'
import { fontFamily, fontSize } from '../../styles/Fonts';
import {myStyles} from '../../styles/ExternalStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import ToggleSwitch from '../../components/ToggleSwitch';
import { getFingerPrintEnable,setFingerPrintEnable } from '../../database/UserData';

function Settings(){
    const navigation = useNavigation()
    const [fingerToggle, setFingerToggle] = useState(false)
    const [loader, setLoader] = useState(false)
    useEffect(() => {
      checkPrefs()
    })
    const checkPrefs = async () => {
      console.log("getFingerPrintEnable => "+ await getFingerPrintEnable())
      var temp = await getFingerPrintEnable()
      if(temp == "1"){
        setFingerToggle(true)
      }else{
        setFingerToggle(false)
      }      
    }
  

    return(
         <View style={{flex:1,backgroundColor:colorWhite}}>
             <StatusBarComponent textColor={true}/>
             <Spinner visible={loader}  />
             <View style={{backgroundColor:colorPrimary,paddingBottom:15,borderBottomEndRadius:10,borderBottomStartRadius:10}}>
                <View style={{flexDirection:'row',marginTop:35,marginStart:15, alignItems:'center'}}>
                  <TouchableOpacity onPress={() => {navigation.pop()}}>
                        <FastImage style={{width: 11, height: 18}} tintColor={colorWhite} resizeMode={"center"} source={require('../../assets/icons/back_icon.png')}/>
                  </TouchableOpacity>
                  <Text style={{fontFamily:fontFamily.medium, color:colorWhite,marginStart:15,fontSize:fontSize.xl}}>Settings</Text>
                </View>
             </View>
               
              <KeyboardAwareScrollView bounce={false} style={{flex:1,height:DEVICE_HEIGHT,backgroundColor:'white'}}  keyboardShouldPersistTaps='handled'>
                <View style={{flex:1,padding:15,backgroundColor:'white'}}>
                
                <View style={myStyles.inputBox}>
                <TouchableOpacity style={{flex:1,flexDirection:'row',alignContent: 'center', alignItems: 'center',paddingEnd:15}} onPress={() => {}}>
                  <Text style={{flex:1,margin:10,fontFamily:fontFamily.medium, color:colorBlack, fontSize:fontSize.m, alignSelf:'center'}}>Enable Fingerprint</Text>
                  <ToggleSwitch
                    isOn={fingerToggle}
                    onColor={colorPrimary}
                    offColor={lightFont}
                    size='large'
                    onToggle={isOn => {
                      setFingerToggle(!fingerToggle)
                      setFingerPrintEnable(fingerToggle?'0':'1')
                    }}
                  />
                </TouchableOpacity>
                </View>

                <View style={myStyles.inputBox}>
                <TouchableOpacity style={{flex:1,flexDirection:'row',alignContent: 'center', alignItems: 'center',}} onPress={() => {navigation.navigate('Profile')}}>
                  <Text style={{flex:1,margin:10,fontFamily:fontFamily.medium, color:colorBlack, fontSize:fontSize.m, alignSelf:'center'}}>Profile</Text>
                  <FastImage style={{width: 11, height: 18,rotation:180,marginEnd:20}}  tintColor={lightFont} resizeMode={"center"} source={require('../../assets/icons/back_icon.png')}/>
                </TouchableOpacity>
                </View>

                <View style={myStyles.inputBox}>
                <TouchableOpacity style={{flex:1,flexDirection:'row',alignContent: 'center', alignItems: 'center',}} onPress={() => {}}>
                  <Text style={{flex:1,margin:10,fontFamily:fontFamily.medium, color:colorBlack, fontSize:fontSize.m, alignSelf:'center'}}>Language</Text>
                  <FastImage style={{width: 11, height: 18,rotation:180,marginEnd:20}}  tintColor={lightFont} resizeMode={"center"} source={require('../../assets/icons/back_icon.png')}/>
                </TouchableOpacity>
                </View>
                
              </View>
              </KeyboardAwareScrollView>
         </View>
    );
}

export default Settings
