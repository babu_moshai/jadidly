import React, {useEffect, useState} from 'react'
import {View,TouchableOpacity,Text,StyleSheet,Alert} from 'react-native'
import StatusBarComponent from '../../components/StatusBarComponent';
import { colorPrimary, colorWhite, colorPrimaryLight, lightFont, cardBorderColor} from '../../styles/Color';
import { DEVICE_WIDTH } from '../../styles/Dimens';
import FastImage from 'react-native-fast-image'
import {useNavigation,CommonActions} from '@react-navigation/native'
import { fontFamily, fontSize } from '../../styles/Fonts';
import { isLoggedInSet,isLoggedInGet } from '../../database/UserData'
import FragHome from '../fragments/FragHome'
import FragMyBooking from '../fragments/FragMyBooking'
import FragMyCars from '../fragments/FragMyCars'
import FragProfile from '../fragments/FragProfile'
import { showSnackBarOne } from '../../helpers/showSnackBarOne'
import {callApiNoData,callApiWithData} from '../../api/NetworkRequest'
import Spinner from '../../components/Spinner'
import { getDataOfUser, saveDataOfUser, logoutUser } from '../../database/UserData'
function Home(){
    const navigation = useNavigation()
    const [selectedTab, setSelectedTab] = useState(1)
    const [userDataLocale, setUserDataLocale] = useState({})
    const [loader, setLoader] = useState(false)
    const [dataRes, setDataRes] = useState({})
    const [length, setLength] = useState(0)

    useEffect(() => {
      checkPrefs()
    },[])
    const checkPrefs = async () => {
      console.log("checkPrefs => "+ await isLoggedInGet())
      await isLoggedInSet("1")
      getHomeAPI()
    }
    const getHomeAPI = async () => {
      setUserDataLocale(await getDataOfUser())
      //alert("userDataLocale.id"+"/"+userDataLocale.id+"/")
      if(userDataLocale.id === "undefined"){
        if(userDataLocale.id != "0" && userDataLocale.id != ""){
          setLoader(true)
          const apiData = await callApiNoData('dashboard',userDataLocale.token)
          console.log("API RESPONSE=> ",apiData)
          setLoader(false)
    
          if(apiData.status){
            //showSnackBarOne(apiData.message)
            saveDataOfUser({})
            let userDataSet = {
              id : apiData.data.id,
              name : apiData.data.name,
              email : apiData.data.email,
              country_code : apiData.data.country_code,
              mobile_number : apiData.data.mobile_number,
              image:apiData.data.image,
              token:userDataLocale.token,
            }
            console.log(userDataSet)
            saveDataOfUser(userDataSet)
            setDataRes(apiData.data)
          }else if(apiData.status_code == "401"){
            Alert.alert("Logout", apiData.error, 
            [
              {text: "ok", onPress: () => {
                logoutUser()
                navigation.dispatch(
                  CommonActions.reset({
                    index: 1,
                    routes: [
                      { name: 'WelcomeScreen' }
                    ],
                  })
                );
              }},
            ]
            ,{ cancelable: false }
          )
          }else{
            showSnackBarOne(apiData.error)
          }
        }
      }      
    }
    const showGuestDialog = async () => {
      Alert.alert("Login", "You need to register/login to get access.", 
        [
          {text: "Not Now", onPress: () => {}},
          {text: "Register/Login", onPress: () => { 
            logoutUser()
            handleScreen("WelcomeScreen")
          }},
        ]
        ,{ cancelable: true }
      )
    }
    return(
         <View style={{flex:1}}>
                <StatusBarComponent textColor={true}/>
                <Spinner visible={loader}  />
                {selectedTab==1?<FragHome dataRes={dataRes}/>:selectedTab==2?<FragMyBooking/>:selectedTab==3?<FragMyCars/>:<FragProfile/>}
                {/* <FastImage style={{width: DEVICE_WIDTH+120,marginStart:-60, height: DEVICE_HEIGHT/2.6,position:'absolute',borderBottomLeftRadius:DEVICE_WIDTH,borderBottomRightRadius:DEVICE_WIDTH}}
                source={require('../../assets/images/splash_bg.png')}/>
                <View style={{width:DEVICE_WIDTH/1.5,height:DEVICE_WIDTH/1.5,top:-80,start:-50,position:'absolute', backgroundColor:whiteTranslucent,borderRadius:DEVICE_WIDTH/1.5}}></View>
                <View style={{width:DEVICE_WIDTH/2.5,height:DEVICE_WIDTH/2.5,top:-60,start:150,position:'absolute', backgroundColor:whiteTranslucent,borderRadius:DEVICE_WIDTH/2.5}}></View>
                <View style={{flexDirection:'row',marginTop:30,marginStart:15,marginEnd:15, alignItems:'center'}}>
                    <TouchableOpacity onPress={() => {navigation.openDrawer()}}>
                      <FastImage style={{width: 25, height: 15}} source={require('../../assets/icons/menu.png')}/>
                    </TouchableOpacity>
                    <View style={{flex:1}}/>
                    <TouchableOpacity onPress={() => {}}>
                      <FastImage style={{width: 18, height: 23}} source={require('../../assets/icons/Notification.png')}/>
                    </TouchableOpacity>
                </View>
                <FastImage
                style={{width: 122, height: 110,alignSelf:'center'}}
                source={require('../../assets/icons/splash_image.png')}/>
                <View style={styles.carouselContainer}>
                  <Carousel  
                    style={styles.carousel}
                    data={data}
                    renderItem={renderItem}
                    itemWidth={125}
                    itemContainerStyle={{marginTop:30}}
                    containerWidth={DEVICE_WIDTH} 
                    separatorWidth={0}
                    ref={carouselRef}
                    inActiveOpacity={0.8}
                    //pagingEnable={false}
                    //minScrollDistance={20}
                    />        
                </View>
                <View style={{flexDirection:'row',marginStart:15,marginEnd:15, alignItems:'center'}}>
                    <Text style={{fontSize:fontSize.l, fontFamily:fontFamily.semibold,color:colorBlack,textAlignVertical:'center',maxHeight:'90%'}} numberOfLines={2}>Offers</Text>    
                    <View style={{flex:1}}/>
                    <TouchableOpacity onPress={() => {}}>
                    <Text style={{fontSize:fontSize.xs,textAlign:'center',textAlignVertical:'center',fontFamily:fontFamily.medium, color:lightFont}}>View All</Text>
                    </TouchableOpacity>
                </View>
                <View style={{width:40,height:3,backgroundColor:colorPrimary,margin:15,marginTop:-4,borderRadius:10}}/>
                <FlatList
                    extraData={updated}
                    data={data}
                    showsVerticalScrollIndicator={false}
                    style={{flexGrow:0}}
                    renderItem={({ item: rowData }) => {
                        return(
                            <View style={styles.cardBackground}>
                                <FastImage style={{position:'absolute',width:'100%',height:150}} source={require('../../assets/images/offer_list_dummy.png')}/>
                                <View style={{flexDirection:'row'}}>
                                  <View style={{height:60,width:'50%',backgroundColor:colorPrimaryLight,justifyContent:'center'}}>
                                    <Text style={{fontSize:15, fontFamily:fontFamily.bold, marginStart:10 , marginEnd:15,color:colorWhite,textAlignVertical:'center',maxHeight:'90%'}} numberOfLines={2}>Registration & Renewal asdds sdsdsdd sdsd sdssds </Text>    
                                  </View> 
                                  <View style={{height:40,width:40,backgroundColor:colorWhite,alignSelf:'center',marginStart:-20,borderRadius:20}}>
                                    <Text style={{height:40,width:40,fontSize:fontSize.xs,textAlign:'center',textAlignVertical:'center',fontFamily:fontFamily.medium, color:colorPrimary}}>25% OFF</Text>    
                                  </View> 
                                </View>
                            </View>                               
                        );
                    }}
                    horizontal={false}
                    keyExtractor={(item, index) => index}
                /> */}

                <View style={{flexDirection:'row',backgroundColor:colorWhite,borderTopColor:cardBorderColor,borderTopWidth:1}}>
                  <View style={{height:60,padding:15,flexDirection:'row',alignItems:'center',flex:1}}>
                      <TouchableOpacity style={{flexDirection:'row',height:40,flexWrap:'wrap',minWidth:DEVICE_WIDTH/4.9,borderRadius:8,justifyContent:'center',alignContent:'center',backgroundColor:selectedTab==1?colorPrimaryLight:colorWhite}} onPress={() => {setSelectedTab(1)}}>
                        <FastImage style={{width: 16, height: 17,margin:5}} tintColor={selectedTab==1?colorPrimary:lightFont} resizeMode={'contain'} source={require('../../assets/icons/home_icon.png')}/>
                        <Text style={{fontSize:fontSize.xss,textAlignVertical:'center',marginEnd:5, alignSelf:'center',fontFamily:fontFamily.regular,color:selectedTab==1?colorPrimary:colorWhite,textAlign:'center'}}>{selectedTab==1?'Home':''}</Text>
                      </TouchableOpacity>
                      <View style={{flex:1}}/>
                      <TouchableOpacity style={{flexDirection:'row',height:40,flexWrap:'wrap',minWidth:DEVICE_WIDTH/4.9,borderRadius:8,justifyContent:'center',alignContent:'center',backgroundColor:selectedTab==2?colorPrimaryLight:colorWhite}} onPress={() => {userDataLocale.id != "0"?setSelectedTab(2):showGuestDialog()}}>
                        <FastImage style={{width: 18, height: 18,margin:5}} tintColor={selectedTab==2?colorPrimary:lightFont} source={require('../../assets/icons/my_booking_black.png')}/>
                        <Text style={{fontSize:fontSize.xss,textAlignVertical:'center',marginEnd:5,alignSelf:'center',fontFamily:fontFamily.regular,color:selectedTab==2?colorPrimary:colorWhite,textAlign:'center'}}>{selectedTab==2?'My\nBookings':''}</Text>
                      </TouchableOpacity>
                      <View style={{flex:1}}/>
                      <TouchableOpacity style={{flexDirection:'row',height:40,flexWrap:'wrap',minWidth:DEVICE_WIDTH/4.9,borderRadius:8,justifyContent:'center',alignContent:'center',backgroundColor:selectedTab==3?colorPrimaryLight:colorWhite}} onPress={() => {userDataLocale.id != "0"?setSelectedTab(3):showGuestDialog()}}>
                        <FastImage style={{width: 17, height: 15,margin:5}} tintColor={selectedTab==3?colorPrimary:lightFont} source={require('../../assets/icons/my_car_black.png')}/>
                        <Text style={{fontSize:fontSize.xss,textAlignVertical:'center',marginEnd:5,alignSelf:'center',fontFamily:fontFamily.regular,color:selectedTab==3?colorPrimary:colorWhite,textAlign:'center'}}>{selectedTab==3?'My Cars':''}</Text>
                      </TouchableOpacity>
                      <View style={{flex:1}}/>
                      <TouchableOpacity style={{flexDirection:'row',height:40,flexWrap:'wrap',minWidth:DEVICE_WIDTH/4.9,borderRadius:8,justifyContent:'center',alignContent:'center',backgroundColor:selectedTab==4?colorPrimaryLight:colorWhite}} onPress={() => {userDataLocale.id != "0"?setSelectedTab(4):showGuestDialog()}}>
                        <FastImage style={{width: 14, height: 17,margin:5}} tintColor={selectedTab==4?colorPrimary:lightFont} source={require('../../assets/icons/profile_icon.png')}/>
                        <Text style={{fontSize:fontSize.xss,textAlignVertical:'center',marginEnd:5,alignSelf:'center',fontFamily:fontFamily.regular,color:selectedTab==4?colorPrimary:colorWhite,textAlign:'center'}}>{selectedTab==4?'Profile':''}</Text>
                      </TouchableOpacity>
                  </View>
                </View>
                <View style={{flexDirection:'row',marginBottom:-10,marginTop:5}}>
                  <View style={{height:5,paddingStart:15,paddingEnd:15,flexDirection:'row',alignItems:'center',flex:1}}>
                      <View style={{width:40,height:3,backgroundColor:selectedTab==1?colorPrimary:colorWhite,margin:15,marginTop:-4,borderRadius:10}}/>
                      <View style={{flex:1}}/>
                      <View style={{width:40,height:3,backgroundColor:selectedTab==2?colorPrimary:colorWhite,margin:15,marginTop:-4,borderRadius:10}}/>
                      <View style={{flex:1}}/>
                      <View style={{width:40,height:3,backgroundColor:selectedTab==3?colorPrimary:colorWhite,margin:15,marginTop:-4,borderRadius:10}}/>
                      <View style={{flex:1}}/>
                      <View style={{width:40,height:3,backgroundColor:selectedTab==4?colorPrimary:colorWhite,margin:15,marginTop:-4,borderRadius:10}}/>
                  </View>
                </View>

         </View>
    );
}
const styles = StyleSheet.create({ 
  carouselContainer: {
      height:150,
  },
    carousel: {
          flex:1,
  },
  cardBackground:{
    marginBottom:10,height:150, marginEnd:15, marginStart:15,justifyContent:'center',
    ...Platform.select({
        android: {
            elevation: 10,
        },
        ios: {
            shadowColor: '#000',
            shadowOffset: {width: 0,height: 1,},
            shadowOpacity: 0.2,
            shadowRadius: 1.41,
        },
    })
}, 
})

export default Home
