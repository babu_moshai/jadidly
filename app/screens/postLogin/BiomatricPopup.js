import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Alert,Text,TouchableOpacity,View,ViewPropTypes,Platform} from 'react-native';
 
import FingerprintScanner from 'react-native-fingerprint-scanner';
import StatusBarComponent from '../../components/StatusBarComponent';
//const {FingerprintScanner} = require("react-native-fingerprint-scanner").default;

export default class BiometricPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errorMessageLegacy: undefined,
      biometricLegacy: undefined
    };
 
    this.description = null;
  }
 
  componentDidMount() {
    if (this.requiresLegacyAuthentication()) {
      this.authLegacy();
    } else {
      this.authCurrent();
    }
  }
 
  componentWillUnmount = () => {
    FingerprintScanner.release();
  }
 
  requiresLegacyAuthentication() {
    return Platform.Version < 23;
  }
 
  authCurrent() {
    FingerprintScanner.authenticate({ title: 'Log in with Biometrics' })
      .then(() => {
        this.props.onAuthenticate();
      });
  }
 
  authLegacy() {
    FingerprintScanner
      .authenticate({ onAttempt: this.handleAuthenticationAttemptedLegacy })
      .then(() => {
        this.props.handlePopupDismissedLegacy();
        Alert.alert('Fingerprint Authentication', 'Authenticated successfully');
      })
      .catch((error) => {
        this.setState({ errorMessageLegacy: error.message, biometricLegacy: error.biometric });
        this.description.shake();
      });
  }
 
  handleAuthenticationAttemptedLegacy = (error) => {
    this.setState({ errorMessageLegacy: error.message });
    this.description.shake();
  };
 
  renderLegacy() {
    const { errorMessageLegacy, biometricLegacy } = this.state;
    const { style, handlePopupDismissedLegacy } = this.props;
 
    return (
      <View style={{flex:1,backgroundColor:'blue'}}>
        <StatusBarComponent textColor={false}/>
        <View style={{backgroundColor:'green'}}>
 
          <Text style={{color:'black'}}>Biometric{'\n'}Authentication</Text>
          <Text>{errorMessageLegacy || `Scan your ${biometricLegacy} on the\ndevice scanner to continue`}</Text>
 
          <TouchableOpacity onPress={handlePopupDismissedLegacy}>
            <Text>BACK TO MAIN</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
 
 
  render = () => {
    if (this.requiresLegacyAuthentication()) {
      return this.renderLegacy();
    }
 
    // current API UI provided by native BiometricPrompt
    return null;
  }
}
 
BiometricPopup.propTypes = {
  onAuthenticate: PropTypes.func.isRequired,
  handlePopupDismissedLegacy: PropTypes.func,
  style: ViewPropTypes.style,
};